-- for new
SELECT tk.ict_id, t.tx_id, tk_adr_from.adr_title as token_from, tk_adr_to.adr_title as token_to, tk.ict_value as token_value, con.con_code as token,
  t.tx_value as trans_value, tk.ict_hash as token_hash,
  t_adr_from.adr_title as trans_from, t_adr_to.adr_title as trans_to, t.tx_hash as trans_hash
FROM coins.icos_tokentransfers tk
  LEFT JOIN coins.icos ico ON tk.ico_id = ico.ico_id
  LEFT JOIN coins.coins con ON con.con_id = tk.con_id
  LEFT JOIN coins.transactions t ON t.adr_from = tk.adr_to
  JOIN coins.addresses tk_adr_from ON tk.adr_from = tk_adr_from.adr_id
  JOIN coins.addresses tk_adr_to ON tk.adr_to = tk_adr_to.adr_id
  JOIN coins.addresses t_adr_from ON t.adr_from = t_adr_from.adr_id
  JOIN coins.addresses t_adr_to ON t.adr_to = t_adr_to.adr_id
  LEFT JOIN coins.tokentransfers_transactions ti ON tk.ict_id = ti.ict_id AND t.tx_id = ti.tx_id
WHERE ico_title <> 'test' AND ico.ico_id = tk.ico_id
      AND t.adr_to = tk.adr_from
      AND t.tx_value > 0
      AND tk.adr_to NOT IN (SELECT adr_id FROM coins.transaction_ignore)
      AND tk.adr_from NOT IN (SELECT adr_id FROM coins.transaction_ignore)
      AND t.adr_to NOT IN (SELECT adr_id FROM coins.transaction_ignore)
      AND t.adr_from NOT IN (SELECT adr_id FROM coins.transaction_ignore)
      AND ti.tx_id ISNULL AND ti.ict_id ISNULL

-- for all
SELECT tk.ict_id, tk.ico_id, t.tx_id, tk_adr_from.adr_title as token_from, tk_adr_to.adr_title as token_to, tk.ict_value as token_value, con.con_code as token,
  t.tx_value as trans_value, tk.ict_hash as token_hash,
  t_adr_from.adr_title as trans_from, t_adr_to.adr_title as trans_to, t.tx_hash as trans_hash
FROM coins.icos_tokentransfers tk
  LEFT JOIN coins.icos ico ON tk.ico_id = ico.ico_id
  LEFT JOIN coins.coins con ON con.con_id = tk.con_id
  LEFT JOIN coins.transactions t ON t.adr_from = tk.adr_to
  JOIN coins.addresses tk_adr_from ON tk.adr_from = tk_adr_from.adr_id
  JOIN coins.addresses tk_adr_to ON tk.adr_to = tk_adr_to.adr_id
  JOIN coins.addresses t_adr_from ON t.adr_from = t_adr_from.adr_id
  JOIN coins.addresses t_adr_to ON t.adr_to = t_adr_to.adr_id
WHERE ico_title <> 'test' AND ico.ico_id = tk.ico_id
      AND t.adr_to = tk.adr_from
      AND t.tx_value > 0
      AND tk.adr_to NOT IN (SELECT adr_id FROM coins.transaction_ignore)
      AND tk.adr_from NOT IN (SELECT adr_id FROM coins.transaction_ignore)
      AND t.adr_to NOT IN (SELECT adr_id FROM coins.transaction_ignore)
      AND t.adr_from NOT IN (SELECT adr_id FROM coins.transaction_ignore)
ORDER BY tk.ict_id


-- for adresses which exist in two or more icos
SELECT tk.ict_id, tk.adr_to, sum(t.tx_value), tk.ico_id, joined.ico_id
FROM coins.tokentransfers_transactions tt
  JOIN coins.icos_tokentransfers tk ON tk.ict_id = tt.ict_id
  JOIN coins.transactions t ON t.tx_id = tt.tx_id
  JOIN
      (SELECT tk.ict_id, tk.adr_to, sum(t.tx_value), tk.ico_id FROM coins.tokentransfers_transactions tt
        JOIN coins.icos_tokentransfers tk ON tk.ict_id = tt.ict_id
        JOIN coins.transactions t ON t.tx_id = tt.tx_id GROUP BY tk.ict_id) joined
    ON tk.adr_to = joined.adr_to AND tk.ico_id <> joined.ico_id
GROUP BY tk.ict_id, joined.ico_id

-- participants who have transfers on 2 or more icos and value of transfers >= 100
SELECT DISTINCT uniq_participant.adr_to as icos_prt, t.tx_value
FROM (SELECT tk.adr_to, sum(t.tx_value)
      FROM coins.tokentransfers_transactions tt
        JOIN coins.icos_tokentransfers tk ON tk.ict_id = tt.ict_id
        JOIN coins.transactions t ON t.tx_id = tt.tx_id
        JOIN
            (SELECT tk.ict_id, tk.adr_to, sum(t.tx_value), tk.ico_id FROM coins.tokentransfers_transactions tt
              JOIN coins.icos_tokentransfers tk ON tk.ict_id = tt.ict_id
              JOIN coins.transactions t ON t.tx_id = tt.tx_id GROUP BY tk.ict_id) similar_prt
          ON tk.adr_to = similar_prt.adr_to AND tk.ico_id <> similar_prt.ico_id
      GROUP BY tk.ict_id, similar_prt.ico_id) uniq_participant
JOIN coins.icos_tokentransfers tk ON uniq_participant.adr_to = tk.adr_to
JOIN coins.tokentransfers_transactions tt ON tk.ict_id = tt.ict_id
JOIN coins.transactions t ON tt.tx_id = t.tx_id
WHERE t.tx_value >= 100
ORDER BY t.tx_value DESC



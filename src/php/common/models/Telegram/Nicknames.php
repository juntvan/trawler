<?php
namespace App\Models\Telegram;

class Nicknames extends \App\Models\Telegram\Base
{
    public static function upsert($nic_title)
    {
        $nic_title = trim($nic_title);
        if (empty($nic_title)) {
            throw new \InvalidArgumentException('Empty nickname');
        }

        // Check if dialog is already exists
        $nic = self::findFirst([
                'conditions' => 'nic_title = :nic_title:',
                'bind' => ['nic_title' => $nic_title]
            ]
        );

        if (!empty($nic->nic_id)) {
            return $nic->nic_id;
        }

        $nic = new self();

        if (!$nic->save(['nic_title' => $nic_title])) {
            foreach ($nic->getMessages() as $message) {
                trigger_error($message);
            }
            throw new \RuntimeException('Failed to insert new nick');
        }

        return $nic->nic_id;
    }
}
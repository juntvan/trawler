<?php

namespace App\Models\Telegram\Messages;

class Urls extends \App\Models\Telegram\Base
{

    public function initialize()
    {
        parent::initialize();
        $this->setSource('messages_urls');
        $this->belongsTo('url_id', 'App\Models\Urls', 'url_id', ['alias' => 'url']);
        $this->belongsTo('msg_id', 'App\Models\Telegram\Messages', 'msg_id', ['alias' => 'msg']);
    }

    public static function process($msg_id, $url_id)
    {
        if (empty($msg_id)) {
            throw new \InvalidArgumentException('Empty msg_id');
        }

        if (empty($url_id)) {
            throw new \InvalidArgumentException('Invalid url_id');
        }

        $data = [
            'msg_id' => $msg_id,
            'url_id' => $url_id
        ];

        $rec = self::findFirst([
            'conditions' => 'msg_id = :msg_id: AND url_id = :url_id:',
            'bind' => ['msg_id' => $msg_id, 'url_id' => $url_id]
        ]);

        if (empty($rec->msg_id)) {
            $rec = new self;
            $rec->save($data);
        }

        if ($rec->getMessages()) {
            foreach ($rec->getMessages() as $message) {
                trigger_error($message);
            }
            throw new \RuntimeException('Failed to insert new node for messages_urls');
        }

        if (empty($rec->msg_id)) {
            throw new \RuntimeException('Failed to insert new node for messages_urls');
        }

        return true;
    }
}
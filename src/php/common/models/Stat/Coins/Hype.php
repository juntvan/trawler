<?php

namespace App\Models\Stat\Coins;

use App\Models\Telegram\Messages\Coins;

class Hype extends \App\Models\Stat\Base
{
    public function initialize()
    {
        parent::initialize();
        $this->setSource('coins_hype');
        $this->belongsTo('con_id', 'App\Models\Coins\Coins', 'con_id', ['alias' => 'con']);
    }

    public function upsert()
    {
        $day = 0;

        while ($day < 10) {
            $interval = $day . ' day';

            $sql = "SELECT mc.con_id, count(mc.con_id) as usagef, current_date - INTERVAL '{$interval}' AS date FROM telegram.messages_coins mc
                    JOIN telegram.messages m ON m.msg_id = mc.msg_id
                WHERE m.msg_date::date = current_date - INTERVAL '{$interval}'
                GROUP BY mc.con_id";

            $day++;

            $messages_coins_model = new Coins();

            $res = new \Phalcon\Mvc\Model\Resultset\Simple(
                null,
                $messages_coins_model,
                $messages_coins_model->getReadConnection()->query($sql));

            if (count($res) < 1) {
                continue;
            }

            foreach ($res as $r) {
                $rec = self::findFirst([
                    'conditions' => 'coh_date = :current_date: AND con_id = :con_id:',
                    'bind' => ['con_id' => $r->con_id, 'current_date' => $r->date]
                ]);

                $data = [
                    'con_id' => $r->con_id,
                    'coh_date' => $r->date,
                    'coh_usagef' => $r->usagef

                ];

                if (empty($rec->coh_id)) {
                    $rec = new self;
                    $rec->save($data);
                    if ($rec->getMessages()) {
                        foreach ($rec->getMessages() as $message) {
                            trigger_error($message);
                        }
                        throw new \RuntimeException("Failed to insert new coins_hype row");
                    }
                    $this->ldebug('New coins_hype row', ['coh_id' => $rec->coh_id, 'con_id' => $rec->con_id, 'coh_usagef' => $rec->coh_usagef, 'coh_date' => $rec->coh_date]);
                } else {
                    $diff = [];
                    foreach ($data as $field => $value) {
                        if (empty($rec->$field) or $rec->$field != $value) {
                            $diff[$field] = $value;
                        }
                    }
                    if (!empty($diff)) {
                        $rec->update($diff);
                        $this->ldebug('Updating existing coins_hype row', ['coh_id' => $rec->coh_id, 'coh_date' => $rec->coh_date] + $diff);
                    }
                }
            }
        }

//        $sql = 'SELECT mc.con_id, count(mc.con_id) as usagef, current_date FROM telegram.messages_coins mc
//                    JOIN telegram.messages m ON m.msg_id = mc.msg_id
//                WHERE m.msg_date::date = current_date
//                GROUP BY mc.con_id';


    }
}
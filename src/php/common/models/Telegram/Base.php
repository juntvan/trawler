<?php

namespace App\Models\Telegram;

use App\Models\ModelBase;
use attics\Lib\Llog\Logger;

class Base extends ModelBase
{
    use Logger;

    public function initialize()
    {
        $this->setSchema('telegram');
    }

    public function init($logger)
    {
        $this->setLogger($logger);
    }
}
<?php

namespace App\Models\Icos;

use App\Lib\Tools;
use App\Models\Coins\Addresses;
use App\Models\Coins\Coins;
use App\Models\Hosts;
use App\Models\Urls;
use Psr\Log\InvalidArgumentException;

class Icos extends Base
{
    private $url_handler;

    public function initialize()
    {
        parent::initialize();
        $this->url_handler = new Urls();
        $this->url_handler->setLogger(\Phalcon\Di::getDefault()->getLogger());
        $this->hasMany('ico_id', 'App\Models\Icos\Icos\Sources', 'ico_id', ['alias' => 'ics']);
        $this->belongsTo('hst_id', 'App\Models\Hosts', 'hst_id', ['alias' => 'hst']);
        $this->belongsTo('con_id', 'App\Models\Coins\Coins', 'con_id', ['alias' => 'con']);
    }

    public function upsert($ico_name_orig, $link, $con_symbol = null, $adr_contract = null)
    {
        if (empty($ico_name_orig)) {
            throw new \InvalidArgumentException('Empty ico_name');
        }

        $ico_name = mb_strtolower(trim($ico_name_orig));

        if (empty($link)) {
            throw new \InvalidArgumentException('Empty link');
        }

        try {
            $url_id = $this->url_handler->process($link, false);
        } catch (\InvalidArgumentException $e) {
            throw new \InvalidArgumentException('Invalid url ' . $link);
        }
        $hst_id = $this->url_handler->hstidByUrlid($url_id);

        $adr_id = null;
        if (!empty($adr_contract)) {
            $addresses = Tools::getEthFromUrl($adr_contract);
            if (!empty($addresses[0])) {
                $adr_model = new Addresses();
                $adr_model->init($this->getLogger());
                $adr_id = $adr_model->process($addresses[0]);
            }
        }

        $con_id = null;
        if (!empty($con_symbol)) {
            try {
                $con_id = Coins::process($ico_name, $ico_name_orig, $con_symbol, Hosts::prepareHst($link), $adr_id);
            } catch (\InvalidArgumentException $e) {
                $con_id = null;
            }
        }

        $data = [
            'hst_id' => $hst_id,
            'ico_name' => $ico_name,
            'con_id' => $con_id,
            'ico_last_check' => 'now()'
        ];

        $rec = self::findFirst([
                'conditions' => 'hst_id = :hst_id:',
                'bind' => ['hst_id' => $hst_id]
            ]
        );

        if (empty($rec->ico_id)) {
            $rec = new self;
            $rec->save($data);
            if ($rec->getMessages()) {
                foreach ($rec->getMessages() as $err) {
                    trigger_error($err, E_USER_ERROR);
                }
                throw new \RuntimeException("Failed to insert new ico {$data}");
            }
            $this->ldebug('New ico', ['ico_id' => $rec->ico_id, 'ico_name' => $rec->ico_name]);
        }
        else {
            $update['ico_last_check'] = 'now()';
            if (empty($rec->con_id) && !empty($con_id)) {
                $update['con_id'] = $con_id;
            }
            $rec->update($update);
        }

        return $rec->ico_id;
    }

    public function updCalId($ico_calendar_id, $field)
    {
        if (empty($ico_calendar_id)) {
            throw new \InvalidArgumentException('Empty ico_calendar_id');
        }

        if (empty($field)) {
            throw new \InvalidArgumentException('Empty field to fill');
        }

        $this->update([$field => $ico_calendar_id]);
    }
}
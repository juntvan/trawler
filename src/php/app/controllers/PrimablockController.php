<?php

namespace App\Controllers;

use App\Models\Telegram\Dialogs;
use App\Models\Telegram\Messages;

class PrimablockController extends ControllerBase
{
    public function indexAction()
    {
        $this->assets->addCss('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', false);
        $this->assets->addCss('http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css', false);
        $this->assets->addCss('css/primablock.css');

        $headerCollection = $this->assets->collection("header");
        $headerCollection->addJs('js/vue.js');
        $headerCollection->addJs('https://cdn.jsdelivr.net/vue.resource/1.0.3/vue-resource.min.js', false);
        $headerCollection->addJs('js/jquery-3.3.1.min.js');
        $headerCollection->addJs('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', false);
        $headerCollection->addJs('js/lodash.js');

        $footerCollection = $this->assets->collection("footer");
        $footerCollection->addJs('js/primablock.js');
        $footerCollection->addJs('https://unpkg.com/ionicons@4.1.0/dist/ionicons.js', false);

        \Phalcon\Tag::setTitle("Primablock");
        return $this->view;
    }
}
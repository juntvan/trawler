<?php

use App\Lib\Tools;
use App\Models\Coins\Primablock;
use App\Models\Coins\Addresses;
use Sunra\PhpSimple\HtmlDomParser;

class TransactionTask extends TaskBase
{
    use \attics\Lib\Llog\Logger;
    private $urls_addresses_handler;
    private $transaction_handler;
    private $tokentransfer_transaction_handler;
    private $APIKEY = 'HCJ3SQ3WPIJ23V72S2QQSX5FD2AUC2BHHR';
    private $ico_tokentransfer_handler;
    private $guzzle_client;
    private $con_model;
    private $tx_list_types = [
        'txlistinternal',
        'txlist'
    ];

    public function websocEtherscanAction()
    {
        $this->logger = $this->di->get('logger');
        $this->setLogger($this->logger);
        $loop = React\EventLoop\Factory::create();

        $reactConnector = new React\Socket\Connector($loop, [
            'dns' => '8.8.8.8',
            'timeout' => 60
        ]);

        $connector = new Ratchet\Client\Connector($loop, $reactConnector);
        $pbt_model = new Primablock\Transactions();
        $this->ldebug('start listening Etherscan websock');

        while (1) {
            $this->websocEtherscanCycle($connector, $pbt_model, $loop);
        }
    }

    public function testwebsocAction()
    {
        $this->logger = $this->di->get('logger');
        $loop = React\EventLoop\Factory::create();

        $reactConnector = new React\Socket\Connector($loop, [
            'dns' => '8.8.8.8'
        ]);

        $connector = new Ratchet\Client\Connector($loop, $reactConnector);
        $pbt_model = new Primablock\Transactions();

        foreach (range(0, 0) as $iter) {
            $offset = $iter * 30;
            $sql = 'SELECT * FROM coins.primablock prm JOIN ' .
                '(SELECT DISTINCT ON (pbt.prm_id) * FROM coins.primablock_transactions pbt ' .
                'JOIN coins.addresses adr ON adr.adr_id=pbt.prm_id ' .
                'ORDER BY pbt.prm_id, pbt.pbt_timestamp DESC) as uniq ' .
                'ON uniq.prm_id=prm.prm_id ' .
                'WHERE prm.prm_invalid IS NOT TRUE ' .
                'ORDER BY pbt_timestamp DESC ' .
                "LIMIT 30";

            $res = new Phalcon\Mvc\Model\Resultset\Simple(
                null,
                $pbt_model,
                $pbt_model->getReadConnection()->query($sql));

            $connector('wss://socket.etherscan.io/wshandler')
                ->then(function (Ratchet\Client\WebSocket $conn) use ($loop, $res)
                {
                    $conn->on('message', function (\Ratchet\RFC6455\Messaging\MessageInterface $response) use ($conn)
                    {
                        $msg = json_decode($response, true);
                        print_r($msg);

                        if ($msg['event'] === 'txlist') {
                            foreach ($msg['result'] as $entry) {
                                $this->handle_transaction($entry, $msg['address']);
                            }
                        }
                    });

                    $conn->on('close', function ($code = null, $reason = null)
                    {
                        echo "Connection closed ({$code} - {$reason})\n";
                    });

                    foreach ($res as $key => $prm) {
                        if (($key % 30) === 0 && $key !== 0) {
                            break;
                        }
                        echo 'prm_id ' . $prm->prm_id . ' pbt_timestamp ' . $prm->pbt_timestamp . PHP_EOL;
                        $m = '{"event": "txlist", "address": "' . $prm->adr_title . '"}';
                        $conn->send($m);
                    }

                    $loop->addPeriodicTimer(15, function () use ($conn)
                    {
                        $conn->send('{"event": "ping"}');
                    });
                }, function (\Exception $e) use ($loop)
                {
                    echo "Could not connect: {$e->getMessage()}\n";
                    $loop->stop();
                });


            $offset = $iter * 30;
            $sql = 'SELECT * FROM coins.primablock prm JOIN ' .
                '(SELECT DISTINCT ON (pbt.prm_id) * FROM coins.primablock_transactions pbt ' .
                'JOIN coins.addresses adr ON adr.adr_id=pbt.prm_id ' .
                'ORDER BY pbt.prm_id, pbt.pbt_timestamp DESC) as uniq ' .
                'ON uniq.prm_id=prm.prm_id ' .
                'WHERE prm.prm_invalid IS NOT TRUE ' .
                'ORDER BY pbt_timestamp DESC ' .
                "LIMIT 30 OFFSET 30";

            $res = new Phalcon\Mvc\Model\Resultset\Simple(
                null,
                $pbt_model,
                $pbt_model->getReadConnection()->query($sql));

            $connector('wss://socket.etherscan.io/wshandler')
                ->then(function (Ratchet\Client\WebSocket $conn) use ($loop, $res)
                {
                    $conn->on('message', function (\Ratchet\RFC6455\Messaging\MessageInterface $response) use ($conn)
                    {
                        $msg = json_decode($response, true);
                        print_r($msg);

                        if ($msg['event'] === 'txlist') {
                            foreach ($msg['result'] as $entry) {
                                $this->handle_transaction($entry, $msg['address']);
                            }
                        }
                    });

                    $conn->on('close', function ($code = null, $reason = null)
                    {
                        echo "Connection closed ({$code} - {$reason})\n";
                    });

                    foreach ($res as $key => $prm) {
                        if (($key % 30) === 0 && $key !== 0) {
                            break;
                        }
                        echo 'prm_id ' . $prm->prm_id . ' pbt_timestamp ' . $prm->pbt_timestamp . PHP_EOL;
                        $m = '{"event": "txlist", "address": "' . $prm->adr_title . '"}';
                        $conn->send($m);
                    }

                    $loop->addPeriodicTimer(15, function () use ($conn)
                    {
                        $conn->send('{"event": "ping"}');
                    });
                }, function (\Exception $e) use ($loop)
                {
                    echo "Could not connect: {$e->getMessage()}\n";
                    $loop->stop();
                });
        }
        $loop->run();

    }

    //find out addr from etherscan, where tx > 10000
    public function fillIgnoreAdrAction()
    {
        $this->logger = $this->di->get('logger');
        //https://etherscan.io/accounts/1?ps=100
        $this->guzzle_client = new \GuzzleHttp\Client();

        $ignore_model = new \App\Models\Coins\Transaction\Ignore();
        $ignore_model->init($this->logger);

        $dom = new DOMDocument();
        $page = 1;
        foreach (range(0, 10) as $v) {

            $url = "https://etherscan.io/accounts/{$page}?ps=100";

            try {
                $response = $this->guzzle_client->request('GET', $url);
            } catch (\GuzzleHttp\Exception\RequestException $e) {
                $this->logger->error('request error',
                    ['error' => $e->getRequest(), 'response' => $e->getResponse()]);
                $err = 1;
                exit;
            }

            $response = $response->getBody();

            $html = $response->getContents();

            $dom = HtmlDomParser::str_get_html($html);

            $table = $dom->find('table', 0);

            $tbody = '';
            foreach ($table->children as $elem) {
                if ($elem->tag === 'tbody') {
                    $tbody = $elem;
                    break;
                }
            }

            if (empty($tbody)) {
                throw new \RuntimeException('tbody not found');
            }

            $trs = $tbody->find('tr');

            if (empty($trs)) {
                throw new \InvalidArgumentException('Tr element not found in table tag');
            }
            foreach ($trs as $tr) {
                if (!isset($tr->children[4]->nodes[0]->_)) {
                    continue;
                }
                $txcount = intval(reset($tr->children[4]->nodes[0]->_));
                if ($txcount < 10000) {
                    continue;
                }
                $title = null;

                $adr_row = $tr->children(1);

                if (empty($adr_row)) {
                    throw new \InvalidArgumentException('Adr cell not found');
                }

                $a = $adr_row->find('a', 0);

                if (!$a || !$a->text()) {
                    throw new \InvalidArgumentException('Cant find address');
                }
                $adr = $a->text();

                $name = $adr_row->text();
                $pipe = strpos($name, '|');
                if ($pipe !== false) {
                    $title = substr($name, $pipe + 2);
                }

                $ignore_model->process($adr, $title);
            }
            $page++;
        }
    }

    public function testAction()
    {
        $this->logger = $this->di->get('logger');

//        $this->tokentransfer_transaction_handler = new \App\Models\Coins\Tokentransfers\Transactions();
//        $this->tokentransfer_transaction_handler->init($this->logger);
//
//        $this->tokentransfer_transaction_handler->process();
    }

    //get all tickets from coinmarketcap
    public function getTicketsCoinmarketAction()
    {
        $this->logger = $this->di->get('logger');

        $coins_model = new \App\Models\Coins\Coins;
        $coins_model->init($this->logger);
        $coins_model->getTicketsCoinmarket();
    }

    //adding ico from terminal
    public function addIcoAction()
    {
        $this->logger = $this->di->get('logger');

        if (empty($GLOBALS['argv'][3]) || $GLOBALS['argc'] < 4) {
            throw new InvalidArgumentException('pass address to arg');
        }

        $coins_model = new \App\Models\Coins\Coins();
        $coins_model->init($this->logger);

        $con_id = $coins_model->setCoinContract($GLOBALS['argv'][3]);
        $con = $coins_model->findFirst([
            'conditions' => 'con_id = :con_id:',
            'bind' => ['con_id' => $con_id]
        ]);

        if (empty($con->con_id)) {
            throw new \InvalidArgumentException('Fail to find coin ' . $con_id);
        }

        $ico_id = \App\Models\Coins\Icos::process($con->con_code, $con_id);
        $this->logger->debug('Process ico', ['ico_id' => $ico_id]);
    }

    public function icosAction()
    {
        $this->logger = $this->di->get('logger');

        $this->con_model = new \App\Models\Coins\Coins();
        $this->con_model->init($this->logger);
        $this->con_model->getTicketsCoinmarket();

        $this->logger->debug('Handling icos');

        $this->ico_tokentransfer_handler = new \App\Models\Coins\Icos\Tokentransfers();
        $this->ico_tokentransfer_handler->init($this->logger);

        $this->transaction_handler = new \App\Models\Coins\Transactions();
        $this->transaction_handler->init($this->logger);

        $this->tokentransfer_transaction_handler = new \App\Models\Coins\Tokentransfers\Transactions();
        $this->tokentransfer_transaction_handler->init($this->logger);

        $this->guzzle_client = new GuzzleHttp\Client([
            'timeout' => 70
        ]);

        while (1) {

            $icos = \App\Models\Coins\Icos::find([
                'conditions' => 'ico_title <> ?0',
                'bind' => ['test'],
                'order' => 'ico_created DESC'
            ]);

            $ignore_addresses_from_model = \App\Models\Coins\Transaction\Ignore::query()
                ->columns('adr_id')
                ->execute();

            $ignore_addresses_from_model = $ignore_addresses_from_model->toArray();

            $ignore_addresses = [];
            foreach ($ignore_addresses_from_model as $value) {
                $ignore_addresses[] = $value['adr_id'];
            }

            foreach ($icos as $ico) {

                $this->tokenTransfers($ico->con->adr->adr_title, $ico->ico_id);
                $ico_tokens = $this->ico_tokentransfer_handler->query()
                    ->conditions('last_check IS NULL AND ico_id = :ico_id:')
                    ->bind(['ico_id' => $ico->ico_id])
                    ->notInWhere('adr_to', $ignore_addresses)
                    ->execute();

                if (empty($ico_tokens[0]->ict_id)) {
                    $ico_tokens = $this->ico_tokentransfer_handler->query()
                        ->conditions('ico_id = :ico_id:')
                        ->bind(['ico_id' => $ico->ico_id])
                        ->notInWhere('adr_to', $ignore_addresses)
                        ->orderBy('last_check ASC')
                        ->execute();
                }

                foreach ($ico_tokens as $ico_token) {
                    echo 'hash ' . $ico_token->ict_hash . ' adr_to ' . $ico_token->adrto->adr_title . PHP_EOL;

                    $err = 0;
                    foreach ($this->tx_list_types as $tx_list_type) {
                        $page = 1;
                        $qtimeout = 0;
                        while (1) {
                            $url = "http://api.etherscan.io/api?module=account&action={$tx_list_type}&address={$ico_token->adrto->adr_title}&page={$page}&offset=4000&sort=asc&apikey={$this->APIKEY}";

                            try {
                                $response = $this->guzzle_client->request('GET', $url);
                            } catch (\GuzzleHttp\Exception\RequestException $e) {
                                $this->logger->error('request error',
                                    ['error' => $e->getRequest(), 'response' => $e->getResponse()]);
                                $err = 1;
                                break;
                            }

                            $response = json_decode($response->getBody(), true);

                            if (empty($response)) {
                                throw new \RuntimeException('Empty response from url ' . $url);
                            }

                            if (intval($response['status']) !== 1) {
                                $this->logger->debug("Response error from", ['url' => $url]);
                                var_dump($response);
                                if ($response['message'] !== 'No transactions found') {
                                    if (++$qtimeout > 5) {
                                        $err = 1;
                                        break;
                                    }
                                    sleep(10);
                                    continue;
                                }
                                break;
                            }

                            if (empty($response['result'])) {
                                $this->logger->debug("Empty result arr", ['url' => $url]);
                                break;
                            }

                            foreach ($response['result'] as $value) {
                                $this->transaction_handler->process($value);
                            }
                            $page++;
                            $qtimeout = 0;
                        }
                    }
                    if ($err === 0) {
                        $ico_token->update(['last_check' => 'now()']);
                        if ($ico_token->getMessages()) {
                            foreach ($ico_token->getMessages() as $message) {
                                trigger_error($message);
                            }
                            throw new \RuntimeException("Fail to insert update ico_token " . $ico_token->ict_id);
                        }
                    }
                    $this->tokentransfer_transaction_handler->process();
                }

            }
            $this->logger->debug('Finish handling. Waiting 60 sec and restart');
            sleep(60);
        }
    }

    //find primablock addresses from msg_url and upsert
    public function getPrimablockContractsAction()
    {
        $this->logger = $this->di->get('logger');

        $this->urls_addresses_handler = new \App\Models\Telegram\Urls\Addresses();
        $this->urls_addresses_handler->setLogger($this->logger);

        $this->logger->debug('Start updating primablock balances');

        $to_find = "primablock.com";

        $hst = \App\Models\Hosts::findFirst([
            'conditions' => 'hst_title = :hst_title:',
            'bind' => ['hst_title' => $to_find]
        ]);

        if (empty($hst->hst_id)) {
            throw new \InvalidArgumentException("Host {$to_find} doesn`t exit");
        }

        $hst_id = $hst->hst_id;
        $primablock_model = new Primablock();
        $primablock_model->init($this->logger);

        while (1) {
            $adrs = \App\Models\Telegram\Urls\Addresses::query()
                ->innerJoin('App\Models\Urls')
                ->where('hst_id = :hst_id:')
                ->bind(['hst_id' => $hst_id])
                ->execute();


            foreach ($adrs as $adr) {
                $balance = null;
                try {
                    $balance = $this->getContractBalance($adr->adr->adr_title);
                } catch (Exception $e) {
                    $this->logger->debug('Fail to get contract balance', [$e]);
                    $balance = null;
                }

                $prm_id = $primablock_model->upsert($adr->adr->adr_id, $balance);
                //nightmare continue this process
            }
            $this->logger->debug('Wait 30 sec');
            sleep(30);
        }
    }

    public function getAllPrimablockTransactionsAction()
    {
        $this->logger = $this->di->get('logger');

        $this->transaction_handler = new \App\Models\Coins\Primablock\Transactions();
        $this->transaction_handler->setLogger($this->logger);

        $this->logger->debug('Start handling All transactions');

        while (1) {
            $curl = curl_init();

            $prms = \App\Models\Coins\Primablock::find([
                "order" => "prm_created DESC"
            ]);

            foreach ($prms as $prm) {
                if (empty($prm->adr->adr_title)) {
                    throw new \InvalidArgumentException('Empty adr_title');
                }

                $this->logger->debug('Handle adr', ['adr_title' => $prm->adr->adr_title]);


                try {
                    //get con_id from contract address
                    $con_id_token_arr = $this->pasteToken($prm->adr->adr_title, $prm->adr->adr_id, $prm->prm_id);
                } catch (Exception $e) {
                    $this->logger->warning('Fail to paste token', [$e]);
                }

                try {
                    $this->findSimilarContract($prm->adr->adr_title);
                } catch (Exception $e) {
                    $this->logger->warning('Fail to find similar contract');
                }

                foreach ($this->tx_list_types as $tx_list_type) {
                    //$adr = '0x938f07ee16ccedd16714d6ae3614c34961e5e790';
                    //norm
                    //api?module=account&action=txlist&address=0xddbd2b932c763ba5b1b7ae3b362eac3e8d40121a
                    //internal
                    //api?module=account&action=txlistinternal&address=0x2c1ba59d6f58433fb1eaee7d20b26ed83bda51a3

                    $url = "http://api.etherscan.io/api?module=account&action={$tx_list_type}&address={$prm->adr->adr_title}&apikey={$this->APIKEY}";
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => $url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_TIMEOUT => 80,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "GET",
                        CURLOPT_HTTPHEADER => array(
                            "cache-control: no-cache"
                        ),
                    ));

                    $response = curl_exec($curl);
                    $curl_err = curl_error($curl);

                    if (!empty($curl_err)) {
                        //throw new \RuntimeException("Get curl error from url {$url} {$curl_err}");
                        //throw new \RuntimeException("Get curl error from url {$url} {$curl_err}");
                        $this->logger->debug('Get curl error from url', ['url' => $url, 'curl_err' => $curl_err]);
                        continue;
                    }

                    $response = json_decode($response, true);

                    if (empty($response) || intval($response['status']) !== 1) {
                        $this->logger->debug("Response error from", ['url' => $url]);
                        var_dump($response);
                        continue;
                    }

                    if (empty($response['result'])) {
                        throw new \RuntimeException("No transaction from {$url}");
                    }

                    $con_id = $prm->adr->con_id;
                    foreach ($response['result'] as $transaction) {
                        $pbt_id = $this->transaction_handler->process($transaction, $prm->adr->adr_id, $prm->prm_id,
                            $con_id);
                    }
                }

            }
            $this->logger->debug('Finish handling All transactions. Wait 30s and restart');
            curl_close($curl);
            sleep(30);
        }
    }

    public function transactionPrimablockLoopAction()
    {
        $this->logger = $this->di->get('logger');

        $this->transaction_handler = new \App\Models\Coins\Primablock\Transactions();
        $this->transaction_handler->setLogger($this->logger);

        $last = \App\Models\Coins\Primablock::findFirst([
            'order' => 'prm_created DESC'
        ]);

        if (empty($last->prm_created)) {
            throw new \InvalidArgumentException('Empty prm_created');
        }

        $last = $last->prm_created;

        while (1) {
            $prms = Primablock::find([
                'order' => 'prm_created DESC',
                'conditions' => 'prm_created > :last:',
                'bind' => ['last' => $last]
            ]);

            if (empty($prms[0]->prm_created)) {
                $this->logger->debug('New contracts not found. Waiting 20 sec');
                sleep(20);
                continue;
            }

            $last = $prms[0]->prm_created;

            foreach ($prms as $prm) {
                $this->getTransactions($prm->adr->adr_title);
            }

            $this->logger->debug('pause 20 sec');
            sleep(20);
        }
    }

    public function treeAction()
    {
        $this->logger = $this->di->get('logger');

        $tokentransfer_model = new \App\Models\Coins\Icos\Tokentransfers();
        $tokentransfers = $tokentransfer_model->find([
            'conditions' => 'ico_id = 2',
            'order' => 'ict_timestamp ASC'
        ]);

        $nodes = [];

        $nodes[50]['parents'][20] = [
            'id' => 20,
            'childrens' => [50]
        ];

        $nodes[50]['parents'][20] = [
            'id' => 20,
            'childrens' => [70]
        ];

        foreach ($tokentransfers as $tokentransfer) {
            echo $tokentransfer->ict_timestamp .PHP_EOL;
        }
    }

    //part of etherscan listener
    private function websocEtherscanCycle($connector, $pbt_model, $loop)
    {
        foreach (range(0, 2) as $iter) {
            $offset = $iter * 30;
            $sql = 'SELECT * FROM coins.primablock prm JOIN ' .
                '(SELECT DISTINCT ON (pbt.prm_id) * FROM coins.primablock_transactions pbt ' .
                'JOIN coins.addresses adr ON adr.adr_id=pbt.prm_id ' .
                'ORDER BY pbt.prm_id, pbt.pbt_timestamp DESC) as uniq ' .
                'ON uniq.prm_id=prm.prm_id ' .
                'WHERE prm.prm_invalid IS NOT TRUE ' .
                'ORDER BY pbt_timestamp DESC ' .
                "LIMIT 30 OFFSET {$offset}";

            $res = new Phalcon\Mvc\Model\Resultset\Simple(
                null,
                $pbt_model,
                $pbt_model->getReadConnection()->query($sql));

            $connector('wss://socket.etherscan.io/wshandler')
                ->then(function (Ratchet\Client\WebSocket $conn) use ($loop, $res)
                {
                    $conn->on('message', function (\Ratchet\RFC6455\Messaging\MessageInterface $response) use ($conn)
                    {
                        $msg = json_decode($response, true);
                        print_r($msg);

                        if ($msg['event'] === 'txlist') {
                            foreach ($msg['result'] as $entry) {
                                $this->handle_transaction($entry, $msg['address']);
                            }
                        }
                    });

                    $conn->on('close', function ($code = null, $reason = null)
                    {
                        echo "Connection closed ({$code} - {$reason})\n";
                        return;
                    });

                    foreach ($res as $key => $prm) {
                        if (($key % 30) === 0 && $key !== 0) {
                            break;
                        }
                        echo 'prm_id ' . $prm->prm_id . ' pbt_timestamp ' . $prm->pbt_timestamp . PHP_EOL;
                        $m = '{"event": "txlist", "address": "' . $prm->adr_title . '"}';
                        $conn->send($m);
                    }

                    $loop->addPeriodicTimer(18, function () use ($conn)
                    {
                        $conn->send('{"event": "ping"}');
                    });
                }, function (\Exception $e) use ($loop)
                {
                    echo "Could not connect: {$e->getMessage()}\n";
                    $loop->stop();
                    echo "Wait 5 min" . PHP_EOL;
                    sleep(5 * 60);
                });
        }
        $loop->run();
    }

    //handle single transaction
    private function getTransactions($prm_adr)
    {
        //$counter = 0;
        $tx_list_types = [
            'txlistinternal',
            'txlist'
        ];

        if (empty($prm_adr)) {
            throw new \InvalidArgumentException('Empty prm_adr');
        }

        $logger = Phalcon\DI::getDefault()->getLogger();

        $this->transaction_handler = new \App\Models\Coins\Primablock\Transactions();
        $this->transaction_handler->setLogger($logger);

        $curl = curl_init();

        $logger->debug('Handle adr', ['adr_title' => $prm_adr]);

        $adr_handler = new Addresses();
        $adr_handler->init($logger);

        $adr_id = $adr_handler->process($prm_adr);

        try {
            //get con_id from contract address
            $con_id_token_arr = $this->pasteToken($prm_adr, $adr_id, $adr_id);
        } catch (Exception $e) {
            $logger->warning('Fail to paste token', [$e]);
        }

        try {
            $this->findSimilarContract($prm_adr);
        } catch (Exception $e) {
            $logger->warning('Fail to find similar contract');
        }

        foreach ($tx_list_types as $tx_list_type) {

            $url = "http://api.etherscan.io/api?module=account&action={$tx_list_type}&address={$prm_adr}&apikey={$this->APIKEY}";
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache"
                ),
            ));

            $response = curl_exec($curl);
            $curl_err = curl_error($curl);

            if (!empty($curl_err)) {
                throw new \RuntimeException("Get curl error from url {$url} {$curl_err}");
            }

            $response = json_decode($response, true);

            if (empty($response) || intval($response['status']) !== 1) {
                $logger->debug("Response error from", ['url' => $url]);
                var_dump($response);
                continue;
            }

            if (empty($response['result'])) {
                throw new \RuntimeException("No transaction from {$url}");
            }

            $con_id = \App\Models\Coins\Coins::process('ethereum', 'Ethereum', 'ETH');
            foreach ($response['result'] as $transaction) {
                $pbt_id = $this->transaction_handler->process($transaction, $adr_id, $adr_id, $con_id);
            }
        }

        curl_close($curl);
    }

    private function pasteToken($adr_title, $adr_id, $prm_id)
    {

        $this->setLogger($this->di->get('logger'));

        $curl = curl_init();

        //get token
        $url = "http://api.etherscan.io/api?module=account&action=tokentx&address={$adr_title}&apikey={$this->APIKEY}";
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));
        curl_setopt($curl, CURLOPT_TIMEOUT, 60);
        $response = curl_exec($curl);
        $curl_err = curl_error($curl);

        if (!empty($curl_err)) {
            curl_close($curl);
            throw new \RuntimeException("Fail to get token from {$url} {$curl_err}");
        }

        $response = json_decode($response, true);

        try {
            if (empty($response) || intval($response['status']) !== 1) {
                throw new \RuntimeException("Empty token from {$url}");
            }

            if (empty($response['result'])) {
                throw new \RuntimeException("Empty result array from {$url}");
            }

            $response = $response['result'];

            $adr_model = new \App\Models\Coins\Addresses();
            $this->transaction_handler = new \App\Models\Coins\Primablock\Transactions();
            $this->transaction_handler->setLogger($this->getLogger());

            $con_id_arr = [];
            foreach ($response as $token) {
                $adr_id_contract_con = null; //Can be null??
                if (empty($token['contractAddress']) || empty($token['tokenName'])) {
                    continue;
                }
                //var_dump($token);
                $adr_id_contract_con = $adr_model->process($token['contractAddress']); //coin(token) address
                $this->logger->debug('Insert token', ['adr_id_contract' => $adr_id_contract_con]);
                $con_id = \App\Models\Coins\Coins::process($token['tokenName'], $token['tokenName'],
                    $token['tokenSymbol'], 'api.etherscan.io', $adr_id_contract_con);
                $con_id_arr[] = $con_id;

                $pbt_id = $this->transaction_handler->process($token, $adr_id, $prm_id, $con_id, true);

            }

        } catch (Exception $e) {
            throw new \RuntimeException($e);
        } finally {
            curl_close($curl);
        }
        return $con_id_arr;
    }

    private function findSimilarContract($adr_title)
    {

        if (empty($adr_title)) {
            throw new \InvalidArgumentException('Empty adr_title');
        }

        $path = "/find-similiar-contracts?a={$adr_title}&lvl=5";
        $url = "https://etherscan.io{$path}";
        $userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36';
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array(
                'authority: etherscan.io',
                'method: GET',
                "path: {$path}",
                'scheme: https',
                'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'accept-encoding: utf-8',
                'accept-language: en-US,en;q=0.9',
                'cache-control: no-cache',
                'pragma: no-cache',
                'upgrade-insecure-requests: 1',
                "user-agent: {$userAgent}"
            )
        );

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        $curl_err = curl_error($curl);

        if (!empty($curl_err)) {
            curl_close($curl);
            throw new \RuntimeException("Fail to get page from etherscan for {$adr_title}");
        }

        $response = array_values(array_unique(Tools::getEthFromEtherscan($response)));

        $adr_model = new \App\Models\Coins\Addresses();
        $adr_model->init($this->di->get('logger'));
        $primablock_model = new Primablock();
        $primablock_model->init($this->di->get('logger'));

        foreach ($response as $adr) {
            $this->logger->debug('Parse adr from similar contracts', ['adr_title' => $adr]);
            try {
                $adr_id = $adr_model->process($adr);
            } catch (Exception $e) {
                $this->logger->debug($e);
                continue;
            }

            $balance = null;
            try {
                $balance = $this->getContractBalance($adr);
            } catch (Exception $e) {
                $this->logger->debug($e);
                $balance = null;
            }

            try {
                $prm_id = $primablock_model->upsert($adr_id, $balance);
            } catch (Exception $e) {
                $this->logger->debug($e);
                continue;
            }
        }

        $this->logger->debug('Finish parse similar contract');
    }

    private function tokenTransfers($adr, $ico_id)
    {
        $page = 1;
        $qtimeout = 0;

        //get tokens transfers
        while (1) {
            $url = 'https://api.etherscan.io/api?module=account&action=tokentx&contractaddress=' . $adr . '&page=' . $page . '&offset=100000&sort=asc&apikey=' . $this->APIKEY;

            $this->logger->debug("Handle", ['page' => $page, 'url' => $url]);

            try {
                $response = $this->guzzle_client->request('GET', $url);
            } catch (\GuzzleHttp\Exception\RequestException $e) {
                $this->logger->debug('request error',
                    ['error' => $e->getRequest(), 'response' => $e->getResponse()]);
                throw new \RuntimeException('Request error');
            }

            $response = json_decode($response->getBody(), true);

            if (empty($response)) {
                throw new \RuntimeException('Empty response from url ' . $url);
            }

            if (intval($response['status']) !== 1) {
                $this->logger->debug("Response error from", ['url' => $url]);
                var_dump($response);
                if ($response['message'] !== 'No transactions found') {
                    if (++$qtimeout > 5) { //if get timeout over 5 times
                        break;
                    }
                    sleep(5);
                    continue;
                }
                break;
            }

            if (empty($response['result'])) {
                $this->logger->debug("Empty result arr", ['url' => $url]);
                break;
            }

            foreach ($response['result'] as $value) {
                $this->ico_tokentransfer_handler->process($value, $ico_id);
            }
            $page++;
            $qtimeout = 0;
        }
    }

    private function getContractBalance($adr)
    {
        $curl = curl_init();

        $url = "https://api.etherscan.io/api?module=account&action=balance&address={$adr}&tag=latest&apikey={$this->APIKEY}";

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 60,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $curl_err = curl_error($curl);

        if (!empty($curl_err)) {
            throw new \RuntimeException("Get curl error from url {$url} {$curl_err}");

        }

        $response = json_decode($response, true);

        if (empty($response) || intval($response['status']) !== 1) {
            throw new \RuntimeException("Response error from {$url}");
        }

        if (!isset($response['result'])) {
            var_dump($response);
            throw new \RuntimeException("No transaction from {$url}");
        }

        return $response['result'];
    }

    private function handle_transaction($result, $adr)
    {
        //Phalcon\DI::getDefault()->getLogger()
        $this->logger = $this->di->get('logger');
        $this->setLogger($this->logger);

        $transaction_handle = new Primablock\Transactions();
        $transaction_handle->init($this->getLogger());

        $prm_handle = new Primablock();
        $prm_handle->init($this->getLogger());

        $prm_id = $prm_handle->getSetId($adr);

        $transaction_handle->process($result, $prm_id, $prm_id);
    }
}
<?php

namespace App\Models\Stat;

class Base extends \App\Models\ModelBase
{
    use \attics\Lib\Llog\Logger;

    public function initialize()
    {
        $this->setSchema('stat');
    }

    public function init($logger) {
        $this->setLogger($logger);
    }
}
<?php

namespace App\Models\Icos;

class Base extends \App\Models\ModelBase
{
    use \attics\Lib\Llog\Logger;

    public function initialize()
    {
        $this->setSchema('icos');
    }

    public function init($logger) {
        $this->setLogger($logger);
    }
}
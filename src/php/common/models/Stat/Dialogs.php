<?php

namespace App\Models\Stat;

use Phalcon\Mvc\Model\Resultset\Simple;

class Dialogs  extends Base
{
    public function upsert()
    {
        $sql = "SELECT m.dlg_id, m.msg_date::date as date, count(m.msg_id) as nmsg, count(distinct m.usr_id_from) as nusr from telegram.messages m
                GROUP BY m.dlg_id, date
                ORDER BY m.dlg_id, date DESC";

        $res = new Simple(
            null,
            $this,
            $this->getReadConnection()->query($sql)
        );

        if (count($res) < 1) {
            throw new \RuntimeException('No data to insert');
        }

        foreach ($res as $r) {
            $data = [
                'dlg_id' => $r->dlg_id,
                'std_date' => $r->date,
                'std_nmsg' => $r->nmsg,
                'std_nusr' => $r->nusr
            ];

            $record = self::findFirst([
                'conditions' => 'dlg_id = :dlg_id: AND std_date = :std_date:',
                'bind' => ['dlg_id' => $data['dlg_id'], 'std_date' => $data['std_date']]
            ]);

            if (empty($record->dlg_id)) {
                $record = new self;
                $record->save($data);
                if ($record->getMessages()) {
                    foreach ($record->getMessages() as $message) {
                        trigger_error($message);
                    }
                    throw new \RuntimeException("Failed to insert new row in stat.dialogs");
                }
                $this->logger->debug('New row inserted in stat.dialogs', $data);
            } else {
                $diff = [];
                foreach ($data as $field => $value) {
                    if (empty($record->$field) or $record->$field != $value) {
                        $diff[$field] = $value;
                    }
                }
                if (!empty($diff)) {
                    $record->update($data);
                    $this->logger->debug('Row updated in stat.dialogs', $data);
                }

            }
        }
    }
}
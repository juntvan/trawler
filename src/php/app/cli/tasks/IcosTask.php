<?php

class IcosTask extends TaskBase
{
    public function icosAction()
    {
        $this->logger = $this->di->get('logger');

        $icos_sources_handler = new \App\Models\Icos\Icos\Sources();
        $icos_sources_handler->init($this->logger);
        $this->logger->debug('handlingSources');
        $icos_sources_handler->handlingSources();

        $this->logger->debug('addIcosSepareted');
        $calendarTask = new CalendarTask();
        $calendarTask->addIcosSeparatedAction();
    }

    public function testAction()
    {
        $this->logger = $this->di->get('logger');

        $url_model = new \App\Models\Urls();
        $url_model->init($this->logger);


        $icos_sources_handler = new \App\Models\Icos\Icos\Sources();
        $icos_sources_handler->init($this->logger);

        $ics = $icos_sources_handler->find([
            'conditions' => 'url_id_telegram IS NOT NULL'
        ]);


        foreach ($ics as $ic) {
            echo $ic->telegram->url_title . PHP_EOL;
        }

//        $url_model->process("https://web.telegram.org/#/im?p=@EthealOfficial");
        echo '1';
    }
}

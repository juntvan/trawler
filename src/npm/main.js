const Nightmare = require('nightmare');
nightmare = new Nightmare({show: false});
const { Client } = require('pg');

const client = new Client({
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME
});
Nightmare.action('textExtract', function(selector, done) {
    this.evaluate_now((selector) => {
        return Array.from(document.querySelectorAll(selector)).map((element) => element.innerText.trim());
}, done, selector)
});

client.connect()
    .then(() => console.log('db connected'))
.catch((e) => console.error('db connection error', e.stack));

// setInterval(function () {
//     query = {
//         text: 'UPDATE coins.primablock SET prm_scraped = null'
//     };
//     client.query(query)
//         .then((res) => {
//         console.log('prm_scraped null filled');
//     })
//     .catch(e => {
//         console.error('Update fail');
//     console.error(e.stack);
// })
// }, 18000000);


nightmare
    .useragent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36")
    .goto('https://primablock.com/pool/' + '0xf966bd962b170299383b902b84cbd387944a3abb' + '/contributor')
    .wait('.input-search')
    .insert('form input ', '0x464508AC323224eDfA10f8Bc70D235C81Ba3eB16')
    .click('body > app-root > div > div > ng-component > main > form > app-button-set > app-styled-button > button')
    .wait('.primary')
    .click('.primary')
    .wait(500);

function main() {
    console.log('working');
    client.query("SELECT * FROM coins.primablock AS prm INNER JOIN coins.addresses AS adr " +
        "ON prm.prm_id=adr.adr_id WHERE prm.prm_scraped IS NULL OR prm.prm_scraped < (now() - interval '12 hours')")
        .then((res) => {
            if (res && res.rowCount) {
                parseTrans(0, res);
            }
            else {
                console.log('No addresses found waiting 10s ...');
                setTimeout(main, 10000);
            }
        })
        .catch((e) => console.error(e));
}

main();

function updateDb(item, row) {
    query = {
        text: `UPDATE coins.primablock SET
       prm_status = $1,
       prm_fee = $2, 
       prm_allocation = $3, 
       prm_pool_value = $4, 
       prm_contribution_min = $5, 
       prm_contribution_max = $6, 
       prm_auto = $7, 
       prm_scraped = now()
       WHERE prm_id = $8`,
        values: [
            item[0],
            item[1].replace(/%/,''),
            item[2].replace(/,/g,''),
            item[5].replace(/,/g,''),
            item[4].replace(/,/g,''),
            item[3].replace(/,/g,''),
            item[7] === 'Off' ? false : true,
            row.adr_id
        ]
    };
    console.log('row updated');
    // console.log(row);
    // console.log(item);
    //console.log(query);
    client.query(query)
        .then((res) => {
        if (res) {
            parseTrans(0, res);
        }
    })
.catch(e => {
        console.error('Update fail');
    console.error(e.stack);
});
}

function parseTrans(i, res) {
    if (!res.rows[i]) {
        return;
    }
    console.log(res.rows[i].adr_title);
    nightmare
        .wait(500)
        .goto('https://primablock.com/pool/' + res.rows[i].adr_title + '/contributor')
        .wait(700)
        .wait('body > app-root > div > div > app-contributor > main > div.ng-star-inserted > app-view-switcher > app-view-switcher-panel.front > div > div > div > section.pool-value > grouped-content > div > p.fees > b')
        .textExtract([
            'body > app-root > div > div > app-contributor > main > div.ng-star-inserted > app-view-switcher > app-view-switcher-panel.front > div > div > header > h1 > b > app-status-text',
            'body > app-root > div > div > app-contributor > main > div.ng-star-inserted > app-view-switcher > app-view-switcher-panel.front > div > div > div > section.pool-value > grouped-content > div > p > app-tooltip > div > ul > li:nth-child(1) > b',
            'body > app-root > div > div > app-contributor > main > div.ng-star-inserted > app-view-switcher > app-view-switcher-panel.front > div > div > div > div > grouped-content > div > section.allocations > table > tbody > tr:nth-child(1) > td > app-ether > app-currency > span > span',
            'body > app-root > div > div > app-contributor > main > div.ng-star-inserted > app-view-switcher > app-view-switcher-panel.front > div > div > div > div > grouped-content > div > section:nth-child(3) > ul > li:nth-child(1) > b > app-ether > app-currency > span > span',
            'body > app-root > div > div > app-contributor > main > div.ng-star-inserted > app-view-switcher > app-view-switcher-panel.front > div > div > div > div > grouped-content > div > section.allocations > table > tbody > tr:nth-child(3) > td > app-ether > app-currency > span > span',
            'body > app-root > div > div > app-contributor > main > div.ng-star-inserted > app-view-switcher > app-view-switcher-panel.front > div > div > div > div > grouped-content > div > section.allocations > table > tbody > tr:nth-child(2) > td > app-ether > app-currency > span > span',
            'body > app-root > div > div > app-contributor > main > div.ng-star-inserted > app-view-switcher > app-view-switcher-panel.front > div > div > div > div > grouped-content > div > section:nth-child(3) > ul > li.ng-star-inserted > p > b',
            'body > app-root > div > div > app-contributor > main > div.ng-star-inserted > app-view-switcher > app-view-switcher-panel.front > div > div > div > div > grouped-content > div > section:nth-child(5) > div:nth-child(2) > ul > li > b'
        ])
        .then((parsedData) => {
            if (!parsedData) {
                console.error('Empty parsed data');
            }
            else
                updateDb(parsedData, res.rows[i]);
            if (res.rows[++i]) {
                parseTrans(i, res);
            }
            else {
                console.log('waiting 5s...');
                setTimeout(main, 5000);
            }
        })
        .catch((e) => {
            console.log('waiting fail');
            console.error(e);
            console.log('Set prm_invalid for ' + res.rows[i].adr_title);
            client.query('UPDATE coins.primablock SET prm_scraped = $1, prm_invalid = $2 WHERE prm_id = $3', ['now()', 'TRUE', res.rows[i].adr_id])
                .then((res) => {
                console.log('Row updated');
                main();
            })
            .catch((err) => {
            console.error('Unable to set prm_invalid');
            console.error(err);
            });
        });
}

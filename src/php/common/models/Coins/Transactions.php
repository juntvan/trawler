<?php

namespace App\Models\Coins;

use App\Lib\Tools;
use App\Models\Coins\Addresses;
use App\Models\Coins\Coins;

class Transactions extends \App\Models\Coins\Base
{
    public function initialize()
    {
        parent::initialize();
        $this->belongsTo('adr_from','App\Models\Coins\Addresses','adr_id',['alias'=>'adrfrom']);
        $this->belongsTo('adr_id','App\Models\Coins\Addresses','adr_id',['alias'=>'adr']);
        $this->belongsTo('adr_to','App\Models\Coins\Addresses','adr_id',['alias'=>'adrto']);
    }

    public function process($transaction)
    {
        $data = [];

        if (empty($transaction)) {
            throw new \InvalidArgumentException('Empty transaction arg');
        }

        if (empty($transaction['hash'])) {
            throw new \InvalidArgumentException("Empty hash for transaction " . $transaction);
        }
        $data['tx_hash'] = trim($transaction['hash']);

        if (empty($transaction['timeStamp'])) {
            throw new \InvalidArgumentException("Empty timeStamp for transaction {$data['tx_hash']}");
        }
        $data['tx_timestamp'] = intval($transaction['timeStamp']);
        if (empty($data['tx_timestamp'])) {
            throw new \InvalidArgumentException("Invalid timeStamp for transaction {$data['tx_hash']}");
        }
        $data['tx_timestamp'] = Tools::date_convert($data['tx_timestamp']);
        if (empty($data['tx_timestamp'])) {
            throw new \InvalidArgumentException("Time convertion error for ico_id {$data['tx_hash']}");
        }

        if (!isset($transaction['blockNumber'])) {
            throw new \InvalidArgumentException("blockNumber doesnt exist for ico_id {$data['tx_hash']}");
        }
        $data['tx_block'] = intval($transaction['blockNumber']);

        if (isset($transaction['value'])) {
            $right_op = '1000000000000000000'; //for eth
            $scale = 18;
            if (!empty($transaction['tokenDecimal'])) {
                $right_op = bcpow('10', intval($transaction['tokenDecimal']));
                $scale = intval($transaction['tokenDecimal']);
            }
            $data['tx_value'] = bcdiv($transaction['value'], $right_op, $scale);
        }

        $data['tx_success'] = 1;
        if (isset($transaction['isError'])) {
            if (intval($transaction['isError']) > 0) {
                $data['tx_success'] = 0;
            }
        }

        $adr_handler = new Addresses();
        $adr_handler->setLogger($this->getLogger());


        $data['adr_to'] = null;
        $data['adr_from'] = null;
        if (!empty($transaction['to'])) {
            $data['adr_to'] = $adr_handler->process($transaction['to']);
        }

        if (!empty($transaction['from'])) {
            $data['adr_from'] = $adr_handler->process($transaction['from']);
        }

        $rec = self::findFirst([
                'conditions' => 'tx_hash = :tx_hash: AND adr_to = :adr_to: AND adr_from = :adr_from:',
                'bind' => [
                    'tx_hash' => $data['tx_hash'],
                    'adr_to' => $data['adr_to'],
                    'adr_from' => $data['adr_from']
                ]
            ]
        );

        if (empty($rec->tx_created)) {
            $rec = new self();
            $rec->save($data);
            if ($rec->getMessages()) {
                foreach ($rec->getMessages() as $err) {
                    trigger_error($err, E_USER_ERROR);
                }
                throw new \RuntimeException('Failed to insert new transaction', $data);
            }
            $this->ldebug('New transaction', ['id' => $rec->tx_id]);
        }
        else {
            $this->ldebug('Transaction already exist', ['id' => $rec->tx_id]);
        }

        return $rec->tx_id;
    }


}
<?php

namespace App\Models\Coins;

class Icos extends Base
{
    public function initialize()
    {
        parent::initialize();
        $this->belongsTo('con_id', 'App\Models\Coins\Coins', 'con_id', ['alias'=> 'con']);
    }

    public static function process($ico_title, $con_id = null)
    {
        if (empty($ico_title)) {
            throw new \InvalidArgumentException('Empty ico_title');
        }

        $data = [
            'ico_title' => $ico_title,
            'con_id' => $con_id
        ];

        $rec = self::findFirst([
            'conditions' => 'ico_title = :ico_title:',
            'bind' => ['ico_title' => $ico_title]
        ]);

        if (empty($rec->ico_id)) {
            $rec = new self();
            $rec->save($data);
            if ($rec->getMessages()) {
                foreach ($rec->getMessages() as $message) {
                    trigger_error($message);
                }
                throw new \RuntimeException("Fail to insert ico {$ico_title}");
            }
        }

        return $rec->ico_id;
    }
}
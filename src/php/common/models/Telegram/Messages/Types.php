<?php
namespace App\Models\Telegram\Messages;

class Types extends \App\Models\Telegram\Base
{

    public function initialize()
    {
        parent::initialize();
        $this->setSource('messages_types');
    }


    /**
     * Create new message type if not exists
     * @param string $mtp_title
     * @return integer
     */
    public static function process($mtp_title)
    {
        $mtp_title = trim($mtp_title);
        if(empty($mtp_title)){
            throw new \InvalidArgumentException('Invalid message type');
        }

        static $cache;
        if(!empty($cache[$mtp_title])){
            return $cache[$mtp_title];
        }

        $mtp = self::findFirst([
                'conditions' => 'mtp_title = :mtp_title:',
                'bind' => ['mtp_title' => $mtp_title]
            ]
        );

        if(empty($mtp->mtp_id)){
            // Insert new
            $mtp = new self();
            $mtp->save(['mtp_title' => $mtp_title]);
        }

		if($mtp->getMessages())
		{
			foreach($mtp->getMessages() as $message){
				trigger_error($message);
			}
			throw new \RuntimeException('Failed to insert new message type');
		}

        if(empty($mtp->mtp_id)){
            throw new \RuntimeException('Failed to insert new message type');
        }

        $cache[$mtp_title] = $mtp->mtp_id;

        return $cache[$mtp_title];
    }
}
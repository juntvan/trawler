#!/bin/bash

pg_dump -F p -s -E UTF8 -C -O -x --file=/backups/trawler_structure_schema.sql --dbname=trawler -U postgres
pg_dump -F p -E UTF8 -C -O -x --file=/backups/trawler_structure_data.sql --dbname=trawler -U postgres -n icos -n public -n telegram -n stat
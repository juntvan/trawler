<?php

namespace App\Models\Telegram;

use App\Lib\Tools;
use App\Models\Telegram\Users\History;
use App\Models\Telegram\Users\Statuses;

class Users extends \App\Models\Telegram\Base
{
    public function initialize()
    {
        parent::initialize();
        $this->hasMany('usr_id', 'App\Models\Telegram\Participants', 'usr_id', ['alias' => 'prt']);

    }

    public function upsert($user)
    {
        $data = [];

        if (empty($user['id'])) {
            throw new \InvalidArgumentException('Empty user id');
        }
        $data['usr_id'] = intval($user['id']);
        if (empty($data['usr_id'])) {
            throw new \InvalidArgumentException("Invalid user id for -> $user");
        }

        if (!empty($user['_'])) {
            $data['tps_id'] = Types::upsert($user['_']);
            unset($user['_']);
        } elseif (!empty($user['type'])) {
            $data['tps_id'] = Types::upsert($user['type']);
            unset($user['type']);
        }

        if (!empty($user['first_name'])) {
            $data['usr_first_name'] = trim(mb_convert_encoding($user['first_name'], 'UTF-8'));
            unset($user['first_name']);
        }

        if (!empty($user['username'])) {
            $data['usr_name'] = $user['username'];
            unset($user['username']);
        }

        if (!empty($user['last_name'])) {
            $data['usr_last_name'] = trim(mb_convert_encoding($user['last_name'], 'UTF-8'));
            unset($user['last_name']);
        }

        if (!empty($user['verified'])) {
            $data['usr_verified'] = 1;
            unset($user['verified']);
        }

        if (!empty($user['phone'])) {
            $data['usr_phone'] = $user['phone'];
            unset($user['phone']);
        }

        if (!empty($user['restricted'])) {
            $data['usr_restricted'] = 1;
            unset($user['restricted']);
        }

        if (!empty($user['access_hash'])) {
            $data['usr_access_hash'] = intval($user['access_hash']);
            unset($user['access_hash']);
        }

        //add to statuses tab
        if (isset($user['status']['_'])) {
            $data['ust_id'] = Statuses::upsert($user['status']['_']);
            unset($user['status']['_']);
        }
        //??
        if (isset($user['status']['expires'])) {
            $data['usr_status_expires'] = Tools::date_convert($user['status']['expires']);
            unset($user['status']['expires']);
        }
        //??
        if (isset($user['status']['was_online'])) {
            // is it real timestamp?
            $data['usr_last_online'] = Tools::date_convert($user['status']['was_online']);
            unset($user['status']['was_online']);
        }

        // Check if user is already exists
        $record = Users::findFirst([
                'conditions' => 'usr_id = :usr_id:',
                'bind' => ['usr_id' => $data['usr_id']]
            ]
        );

        // New record
        if (empty($record->usr_created)) {
            $record = new Users();
            $record->save($data);
            if ($record->getMessages()) {
                foreach ($record->getMessages() as $message) {
                    trigger_error($message, E_USER_ERROR);
                }
                throw new \RuntimeException('Failed to insert new user', $user);
            }
            $this->ldebug('New user', ['id' => $record->usr_id]);
        } else {
            // Compare new and old records
            $diff = ['usr_last_update' => Tools::date_convert(time())];
            foreach ($data as $field => $value) {
                if (empty($record->$field) or $record->$field != $value) {
                    $diff[$field] = $value;
                }
            }
            if (!empty($diff)) {
                //TODO need to change
                if (!empty($diff['username']) || !empty($diff['first_name']) || !empty($diff['last_name'])) {
                    $history = new History();
                    $history->upsert($record);
                }
                $record->update($diff);
                $this->ldebug('Updating existing user', ['id' => $data['usr_id']] + $diff);
            }
        }

        return $record->usr_id;
    }

    public function getsetUserId($id)
    {
        if (empty($id)) {
            throw new \InvalidArgumentException('Empty id');
        }

        $id = intval($id);

        if (empty($id)) {
            throw new \InvalidArgumentException('Invalid id');
        }

        $rec = self::findFirst([
            'conditions' => 'usr_id = :usr_id:',
            'bind' => ['usr_id' => $id]
        ]);

        if (empty($rec->usr_id)) {
            $rec = new self;
            $data = [
                'usr_id' => $id,
            ];

            $rec->save($data);
            if ($rec->getMessages()) {
                foreach ($rec->getMessages() as $message) {
                    trigger_error($message, E_USER_ERROR);
                }
                throw new \RuntimeException('Failed to insert new user', $id);
            }
            $this->ldebug('New user in getsetUsrId', ['id' => $rec->usr_id]);

        }
        return $rec->usr_id;


    }
}
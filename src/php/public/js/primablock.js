
Vue.component('msg-view', {
     data: function () {
        return {
            count: 0,
            msgs: [],
            title: ''
        }
    },
    beforeDestroy: function () {
        document.removeEventListener('keydown', this.close)
    },
    mounted: function () {
        document.addEventListener("keydown", this.close);
    },
    methods: {
        close: function (e) {
            if (e.keyCode === 27) {
                this.$emit('close');
            }
        },
    },
    template: '#modal-template'
})

new Vue({
    el: '#app',

    data: {
        prms: {},
        showModalMsg: false,
        showModalToken: false,
        slotProps: '',
        currentPage: 1,
        perPage: 1000,
        totalRows: 0,
        filter: {},
        filterIcon: {
            'asc': 'ion-android-arrow-dropup',
            'desc': 'ion-android-arrow-dropdown'
        },
        loading: false,
        selectPool: 'All',
        selectStatus: 'All'
    },

    created: function () {
        this.fetchDataParams();
        window.onscroll = this.onScroll;
    },
    methods: {
        fetchData: function () {
            this.$http.get('http://trawler:8080/api/primablock')
                .then(response => {
                    this.prms = response.data
                })
        },
        fetchDataParams: function () {
            var options = {
                params: {
                    page: this.currentPage,
                    per_page: this.perPage
                }
            };
            this.loading = true
            this.$http.get('http://trawler:8080/api/primablock', options)
                .then(response => {
                    this.loading = false;
                    temp = response.body;
                    this.currentPage++;
                    this.perPage = temp.per_page;
                    delete temp.page;
                    delete temp.per_page;

                    for (k in temp) {
                        if (temp[k].allocation === 0) {
                            temp[k].pool_value_percent = null;
                        }
                        else {
                            temp[k].pool_value_percent = Math.round((temp[k].pool_value / temp[k].allocation * 100) * 100) / 100;
                        }
                    }

                    if (Object.keys(this.prms).length === 0) {
                        this.prms = temp;
                    }
                    else {
                        console.log(Object.keys(this.prms).length);
                        for (k in temp) {
                            this.$set(this.prms, Object.keys(this.prms).length, temp[k]);
                        }
                    }

                })
        },
        setFilter: function (newfilter) {
            if (this.filter.hasOwnProperty(newfilter) && this.filter[newfilter] === 'desc') {
                Vue.delete(this.filter, newfilter);
            }
            else if (this.filter.hasOwnProperty(newfilter)) {
                this.filter[newfilter] = 'desc';
            }
            else {
                this.$set(this.filter, newfilter, 'asc');
            }
        },
        filterClassArrow: function (prop) {
            if (this.filter.hasOwnProperty(prop)) {
                return this.filterIcon[this.filter[prop]];
            }
            return '';
        },
        onScroll: function (event) {
            var wrapper = event.target


            var curPos = window.pageYOffset + document.documentElement.clientHeight;
            var height = wrapper.body.offsetHeight;
            var diffHeight = height - curPos;
            if (diffHeight < 1 && !this.loading) {
                this.loading = true;
                this.fetchDataParams();
            }
        },
        colorPoolValue: function (allocation, pool_value_percent) {
            if (allocation === 0) {
                return '';
            }
            return 'linear-gradient(to left, white ' + (100 - pool_value_percent) + '%, green ' + pool_value_percent + '%)';
        },
        setModalData: function (msgs, title)  {
            console.log(title);
            this.msgs = msgs;
            this.title = title;
        }

    },
    computed: {
        orderedPrms: function () {
            let res = _.orderBy(this.prms, Object.keys(this.filter), Object.values(this.filter));

            if (this.selectPool === 'Filled') {
                _.remove(res, function(value) {
                  if (value.pool_value_percent === 100) {
                      return false;
                  }
                  return true;
                });
            }
            else if (this.selectPool === 'Unfilled') {
                _.remove(res, function(value) {
                    if (value.pool_value_percent === 100) {
                        return true;
                    }
                    return false;
                });
            }

            if (this.selectStatus !== 'All') {
                _.remove(res, (value) =>  {
                    console.log(this.selectStatus);

                    if (value.status === this.selectStatus) {
                        return false;
                    }
                    return true;
                });
            }

            return res;
        }

    }
})
<?php

namespace App\Models\Telegram\Messages;

class Coins extends \App\Models\Telegram\Base
{
    private $reg = null;
    private $con_model;

    public function initialize()
    {
        parent::initialize();
        $this->setSource('messages_coins');
        $this->belongsTo('con_id', 'App\Models\Coins\Coins', 'con_id', ['alias' => 'con']);
        $this->belongsTo('msg_id', 'App\Models\Telegram\Messages', 'msg_id', ['alias' => 'msg']);

    }

    public function process($msg_id, $msg_title)
    {
        if (empty($msg_id)) {
            throw new \InvalidArgumentException('Empty msg_id');
        }
        $msg_id = intval($msg_id);
        if (empty($msg_id)) {
            throw new \InvalidArgumentException('Invalid msg_id');
        }

        if (empty($msg_title)) {
            $this->ldebug('Empty msg_title for ', ['msg_id' => $msg_id]);
            return;
        }

        if (empty($this->reg)) {
            $this->reg = \App\Models\Coins\Coins::getCoinRegex();
        }

        $matches = [];

        preg_match_all($this->reg, $msg_title,$matches);
        if (!empty($matches)) {
            $matches = reset($matches);
        }
        if (empty($matches)) {
            $this->ldebug('Coins in msg not found', ['msg_id' => $msg_id]);
            return;
        }


        foreach ($matches as $match) {
            if (empty($match)) {
                continue;
            }

            if (!$this->con_model) {
                $this->con_model = new \App\Models\Coins\Coins();
                $this->con_model->init(\Phalcon\DI::getDefault()->getLogger());
            }

            $con_id = $this->con_model->getCoinId($match);
            if (empty($con_id)) {
                throw new \InvalidArgumentException('Coin symbol ' . $match . ' not found');
            }

            $data = [
                'msg_id' => $msg_id,
                'con_id' => $con_id
            ];
            $rec = self::findFirst([
                'conditions' => 'msg_id = :msg_id: AND con_id = :con_id:',
                'bind' => ['msg_id' => $msg_id, 'con_id' => $con_id]
            ]);

            if (empty($rec->msg_id)) {
                $rec = new self;
                $rec->save($data);
                if ($rec->getMessages()) {
                    foreach ($rec->getMessages() as $message) {
                        trigger_error($message);
                    }
                    throw new \RuntimeException('Failed to insert new message_coins');
                }
                $this->ldebug('Insert new message_coins', ['msg_id' => $msg_id, 'con_id' => $con_id]);
            }


        }

    }
}
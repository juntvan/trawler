CREATE OR REPLACE FUNCTION update_prm_number ()
  RETURNS BOOLEAN LANGUAGE plpgsql SECURITY DEFINER AS $$
DECLARE
  ret BOOLEAN := TRUE;
  res CURSOR FOR
    SELECT adr_in_dialogs.dlg_id, COUNT(adr_in_dialogs.dlg_id) AS prm_number FROM
        (SELECT dlg.dlg_id, url.url_title, COUNT(dlg.dlg_id) AS prm_num FROM telegram.dialogs dlg
        JOIN telegram.messages msg ON dlg.dlg_id = msg.dlg_id
        JOIN telegram.messages_urls msgs_urls ON msg.msg_id = msgs_urls.msg_id
        JOIN public.urls url ON url.url_id = msgs_urls.url_id
        JOIN public.hosts hst ON hst.hst_id = url.hst_id
        WHERE hst.hst_title = 'primablock.com'
      GROUP BY (dlg.dlg_id, url.url_title)) AS adr_in_dialogs
    GROUP BY (adr_in_dialogs.dlg_id);

BEGIN
 FOR x IN res
   LOOP
   RAISE NOTICE 'update row %', x;
    UPDATE telegram.dialogs
       SET dlg_prm_number = x.prm_number
     WHERE telegram.dialogs.dlg_id = x.dlg_id;
       IF ret = TRUE AND FOUND = FALSE THEN
          ret := FOUND;
       END IF;
  END LOOP;

RETURN ret;
END;
$$;

SELECT update_prm_number();
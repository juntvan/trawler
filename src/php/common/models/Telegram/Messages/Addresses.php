<?php

namespace App\Models\Telegram\Messages;

use App\Models\Telegram\Base;

class Addresses extends Base
{
    public function initialize()
    {
        parent::initialize();
        $this->setSource('messages_addresses');
    }

    public function process($adr_id, $msg_id)
    {
        if (empty($adr_id)) {
            throw new \InvalidArgumentException('Empty adr_id');
        }

        if (empty($msg_id)) {
            throw new \InvalidArgumentException('Empty msg_id');
        }

        $data = [
            'adr_id' => $adr_id,
            'msg_id' => $msg_id
        ];

        $rec = self::findFirst([
            'conditions' => 'msg_id = :msg_id: AND adr_id = :adr_id:',
            'bind' => ['msg_id' => $msg_id, 'adr_id' => $adr_id]
        ]);

        if (empty($rec->adr_id)) {
            $rec = new self;
            $rec->save($data);
            if ($rec->getMessages()) {
                foreach ($rec->getMessages() as $message) {
                    trigger_error($message);
                }
                throw new \RuntimeException('Fail to insert into messages_addresses');
            }
            $this->ldebug('Added new row in messages_addresses', ['adr_id' => $rec->adr_id]);
        }
        return true;
    }
}
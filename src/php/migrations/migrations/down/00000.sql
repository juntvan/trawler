ALTER TABLE coins.primablock DROP prm_scraped;

ALTER TABLE coins.primablock RENAME COLUMN prm_allocation TO prm_capacity;
ALTER TABLE coins.primablock RENAME COLUMN prm_pool_value TO prm_collected;
ALTER TABLE coins.primablock DROP prm_status;
ALTER TABLE coins.primablock_transactions ALTER COLUMN pbt_hash TYPE CHAR(64) USING pbt_hash :: CHAR(64);
ALTER TABLE coins.primablock_transactions DROP pbt_from;
ALTER TABLE coins.primablock_transactions DROP pbt_to;
ALTER TABLE coins.coins DROP adr_id_contract;

ALTER TABLE coins.coins
  ADD CONSTRAINT coins_fk_con_id
FOREIGN KEY (adr_id_contract) REFERENCES addresses (adr_id) ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE coins.coins
  ALTER COLUMN con_id DROP DEFAULT;
DROP SEQUENCE coins_con_id_seq;

ALTER TABLE coins.primablock_transactions ALTER COLUMN pbt_from TYPE CHAR(64) USING pbt_from :: CHAR(64);
ALTER TABLE coins.primablock_transactions ALTER COLUMN pbt_to TYPE CHAR(64) USING pbt_to :: CHAR(64);

DROP INDEX coins.coins_idx_adr_id_contract;

DROP INDEX coins.addresses_idx_con_id ;

ALTER TABLE coins.addresses
  RENAME CONSTRAINT addresses_fk_con_id TO addresses_fk_adr_id;

ALTER TABLE coins.coins
  RENAME CONSTRAINT coins_fk_adr_id TO coins_fk;

ALTER TABLE coins.primablock
  RENAME CONSTRAINT primablock_fk_adr_id TO primablock_fk;

DROP INDEX coins.primablock_idx_ico_id;

ALTER TABLE coins.primablock_transactions
  RENAME CONSTRAINT primablock_transactions_fk_prm_id TO primablock_transactions_fk;

ALTER TABLE coins.primablock_transactions
  RENAME CONSTRAINT primablock_transactions_fk_adr_id TO primablock_transactions_fk1;

ALTER TABLE coins.primablock_transactions
  DROP COLUMN con_id;

ALTER TABLE coins.primablock_transactions
  RENAME COLUMN adr_from TO pbt_from;

ALTER TABLE coins.primablock_transactions
  RENAME COLUMN adr_to TO pbt_to;

ALTER INDEX coins.primablock_transactions_idx_adr_id
RENAME TO primablock_transactions_idx;

ALTER TABLE coins.primablock_transactions
  DROP CONSTRAINT primablock_transactions_fk_adr_id_from RESTRICT;

ALTER TABLE coins.primablock_transactions
  DROP CONSTRAINT primablock_transactions_fk_adr_id_to RESTRICT;

ALTER TABLE coins.primablock_transactions
  DROP CONSTRAINT primablock_transactions_fk_con_id RESTRICT;

DROP INDEX coins.primablock_transactions_idx_prm_id;

DROP INDEX coins.primablock_transactions_idx_adr_id_from;

DROP INDEX coins.primablock_transactions_idx_adr_id_to;

DROP INDEX coins.primablock_transactions_idx_con_id;

ALTER TABLE public.urls
  RENAME CONSTRAINT urls_fk_hst_id TO urls_fk;

DROP INDEX public.urls_idx_hst_id;

ALTER TABLE telegram.dialogs
  RENAME CONSTRAINT dialogs_fk_tps_id TO dialogs_fk;

ALTER TABLE telegram.dialogs
  RENAME CONSTRAINT dialogs_fk_nic_id TO dialogs_fk1;

DROP INDEX telegram.dialogs_idx_tps_id;

DROP INDEX telegram.dialogs_idx_nic_id;

ALTER TABLE telegram.messages_urls
  RENAME CONSTRAINT messages_urls_fk_url_id TO messages_urls_fk;

ALTER TABLE coins.primablock DROP prm_invalid;

ALTER TABLE coins.primablock ALTER COLUMN prm_allocation TYPE NUMERIC(6,2) USING prm_allocation :: NUMERIC(6,2);
ALTER TABLE coins.primablock ALTER COLUMN prm_pool_value TYPE NUMERIC(6,2) USING prm_pool_value :: NUMERIC(6,2);
ALTER TABLE coins.primablock ALTER COLUMN prm_contribution_max TYPE NUMERIC(6,2) USING prm_contribution_max :: NUMERIC(6,2);
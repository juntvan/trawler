<?php

namespace App\Models\Telegram\Messages;

use App\Models\Telegram\Dialogs;
use App\Models\Telegram\Participants;
use App\Models\Telegram\Users;

class Actions extends \App\Models\Telegram\Base
{
    private $user_handler;
    private $participants_handler;
    private $dialog_handler;

    public function initialize()
    {
        parent::initialize();
        $this->setSource('messages_actions');
    }

    public function init($logger) {
        $this->setLogger($logger);
        $this->user_handler = new Users();
        $this->user_handler->setLogger($logger);
        $this->participants_handler = new Participants();
        $this->participants_handler->setLogger($logger);
        $this->dialog_handler = new Dialogs();
        $this->dialog_handler->setLogger($logger);
    }

    /**
     * Create new message action if not exists
     * @param string $mac_title
     * @return integer
     */
    public function process($message)
    {
        if (empty($message)) {
            throw new \InvalidArgumentException('Empty message');
        }


        if (empty($message['action'])) {
            throw new \InvalidArgumentException('Empty action');
        }

        $action = $message['action'];

        if (empty($action['_'])) {
            throw new \InvalidArgumentException('Empty type action');
        }

        $mac_title = $action['_'];

        $mac_title = trim($mac_title);
        if(empty($mac_title)){
            throw new \InvalidArgumentException('Invalid message action');
        }

        if ($mac_title === 'messageActionChatAddUser' || $mac_title === 'messageActionChatDeleteUser') {
            if (isset($action['user_id'])) {
                $action['users'][] = $action['user_id'];
            }
            if (empty($action['users'])) {
                throw new \InvalidArgumentException('Empty action[users]');
            }

            foreach ($action['users'] as $user) {
                $usr_id = $this->user_handler->getsetUserId($user);

                if (!empty($message['to_id']['channel_id'])) {
                    $dlg_id = $message['to_id']['channel_id'];
                }
                elseif (!empty($message['to_id']['chat_id'])) {
                    $dlg_id = $message['to_id']['chat_id'];
                }
                elseif (!empty($message['to_id']['user_id'])) {
                    $dlg_id = $message['to_id']['user_id'];
                }

                $dlg_id = $this->dialog_handler->getsetDlgId($dlg_id);

                $is_deleted = false;
                if ($mac_title === 'messageActionChatDeleteUser') {
                    $is_deleted = true;
                }

                $date = null;
                if (!empty($message['date'])) {
                    $date = $message['date'];
                }

                $pts_id = $this->participants_handler->upsert($dlg_id, $usr_id, $date, $is_deleted);
            }
        }

        static $cache;
        if (!empty($cache[$mac_title])){
            return $cache[$mac_title];
        }

        $mac = self::findFirst([
                'conditions' => 'mac_title = :mac_title:',
                'bind' => ['mac_title' => $mac_title]
            ]
        );

        if (empty($mac->mac_id)){
            $mac = new self();
            $mac->save(['mac_title' => $mac_title]);
        }

        if ($mac->getMessages()){
            foreach($mac->getMessages() as $message){
                trigger_error($message);
            }
            throw new \RuntimeException('Failed to insert new message action');
        }

        if (empty($mac->mac_id)){
            throw new \RuntimeException('Failed to insert new message action');
        }

        $cache[$mac_title] = $mac->mac_id;

        return $cache[$mac_title];
    }
}
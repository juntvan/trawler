<?php

namespace App\Models\Telegram;

use App\Lib\Tools;
use App\Models\ModelBase;

class Participants extends Base
{
    public function initialize()
    {
        parent::initialize();
        $this->belongsTo('dlg_id', 'App\Models\Telegram\Dialogs', 'dlg_id', ['alias' => 'dlg']);
        $this->belongsTo('usr_id', 'App\Models\Telegram\Users', 'usr_id', ['alias' => 'usr']);
        $this->belongsTo('prt_id', 'App\Models\Stat\Users', 'prt_id', ['alias' => 'statuser']);
    }

    public function upsert($dlg_id, $usr_id, $prt_joined = null, $is_deleted = false)
    {
        if (empty($dlg_id)) {
            throw new \InvalidArgumentException('Empty dialog ID');
        }
        $dlg_id = intval($dlg_id);
        if (empty($dlg_id)) {
            throw new \InvalidArgumentException('Invalid dialog ID');
        }

        if (empty($usr_id)) {
            throw new \InvalidArgumentException('Empty user ID');
        }
        $usr_id = intval($usr_id);
        if (empty($usr_id)) {
            throw new \InvalidArgumentException('Invalid user ID');
        }

        if (!is_bool($is_deleted)) {
            throw new \InvalidArgumentException('is_deleted isn`t bool type');
        }

        $data = [
            'usr_id' => $usr_id,
            'dlg_id' => $dlg_id,
            'prt_is_deleted' => $is_deleted
        ];

        $prt_joined = Tools::date_convert($prt_joined);
        if (!empty($prt_joined)) {
            $data['prt_joined'] = $prt_joined;
        }

        // Check if dialog is already exists
        $rec = Participants::findFirst([
                'conditions' => 'usr_id = :usr_id: AND dlg_id = :dlg_id:',
                'bind' => ['usr_id' => $usr_id, 'dlg_id' => $dlg_id]
            ]
        );

        if (empty($rec->prt_id)) {
            $rec = new self;
            $rec->save($data);
            if ($rec->getMessages()) {
                foreach ($rec->getMessages() as $message) {
                    trigger_error($message, E_USER_ERROR);
                }
                throw new \RuntimeException('Failed to insert new particapant record', $data);
            }
            $this->ldebug('New participant', ['user' => $rec->usr_id, 'dialog' => $dlg_id]);
        } else {
            // Compare new and old records
            $diff = [];
            if ($rec->prt_is_deleted !== $data['prt_is_deleted']) {
                $diff['prt_is_deleted'] = $data['prt_is_deleted'];
            }
            if (!empty($diff)) {
                $rec->update($diff);
                $this->ldebug('Updating existing participant', ['prt_id' => $rec->prt_id, $diff]);
            }
        }

        return $rec->prt_id;
    }
}

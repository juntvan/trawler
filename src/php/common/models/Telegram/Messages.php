<?php

namespace App\Models\Telegram;

use App\Lib\Tools;
use App\Models\Coins\Addresses;
use App\Models\Telegram\Messages\Actions;
use App\Models\Telegram\Messages\Coins;
use App\Models\Urls;
use Phalcon\Di;

class Messages extends \App\Models\Telegram\Base
{
    private $user_handler;
    private $urls_handler;
    private $addr_handler;
    private $message_addr_handler;
    private $at_telegram_handler;
    private $dialog_handler;
    private $msgs_coins;
    private $participant_handler;
    private $action_handler;

    public function init($logger)
    {
        $this->setLogger($logger);

        // Create user handler
        $this->user_handler = new Users();
        $this->user_handler->setLogger($logger);

        // Create user handler
        $this->urls_handler = new Urls();
        $this->urls_handler->init($logger);

        //coin addresses handler
        $this->addr_handler = new Addresses();
        $this->addr_handler->setLogger($logger);

        //coin addresses handler
        $this->message_addr_handler = new \App\Models\Telegram\Messages\Addresses();
        $this->message_addr_handler->setLogger($logger);

        $this->at_telegram_handler = new Telegramat();
        $this->message_addr_handler->setLogger($logger);

        $this->dialog_handler = new Dialogs();
        $this->dialog_handler->setLogger($logger);

        $this->msgs_coins = new Coins();
        $this->msgs_coins->init($logger);

        $this->participant_handler = new Participants();
        $this->participant_handler->init($logger);

        $this->action_handler = new Actions();
        $this->action_handler->init($this->getLogger());
    }

    public function onConstruct()
    {
        $this->belongsTo('dlg_id', 'App\Models\Telegram\Dialogs', 'dlg_id', ['alias' => 'dlg']);
        $this->belongsTo('usr_id_from', 'App\Models\Telegram\Users', 'usr_id', ['alias' => 'usr']);
    }

    /**
     * Create new message if not exist, update if exist
     * @param array $messages
     * @return integer
     */
    public function process($message) //rename to upsert
    {
        $data = [];

        //validate message id
        if (empty($message['id'])) {
            throw new \InvalidArgumentException('Empty message id');
        }
        $msg_tid = intval($message['id']);
        if (empty($msg_tid)) {
            throw new \InvalidArgumentException('Invalid message id');
        }
        $data['msg_tid'] = $msg_tid;

        //add msg type
        if (empty($message['_'])) {
            throw new \InvalidArgumentException('Invalid message type');
        }
        $data['mtp_id'] = \App\Models\Telegram\Messages\Types::process($message['_']);

        if (!empty($message['action']['_'])) {
            $data['mac_id'] = $this->action_handler->process($message);
        }

        //validate dialog id
        if (empty($message['to_id']['channel_id']) && empty($message['to_id']['chat_id']) && empty($message['to_id']['user_id'])) {
            throw new \InvalidArgumentException("Empty to_id for {$msg_tid}");
        } else {
            if (!empty($message['to_id']['channel_id'])) {
                $dlg_id_to = intval($message['to_id']['channel_id']);
            } elseif (!empty($message['to_id']['chat_id'])) {
                $dlg_id_to = intval($message['to_id']['chat_id']);
            } elseif (!empty($message['to_id']['user_id'])) {
                $dlg_id_to = intval($message['to_id']['user_id']);
            }
            if (empty($dlg_id_to)) {
                throw new \InvalidArgumentException("Invalid recepient id {$msg_tid}");
            }

            $this->dialog_handler = new Dialogs();
            $data['dlg_id'] = $this->dialog_handler->getsetDlgId($dlg_id_to);
        }

        if (empty($data['dlg_id'])) {
            throw new \InvalidArgumentException('Empty dlg_id');
        }

        $this->ldebug("Handling ", ['msg_tid' =>$data['msg_tid'], 'dlg_id' => $data['dlg_id']]);

        //via bot
        if (!empty(($message['via_bot_id']))) {
            $data['msg_via_bot_id'] = intval($message['via_bot_id']);
        }

        //validate sender id
        if (!empty($message['from_id'])) {
            $usr_id_from = intval($message['from_id']);
            if (empty($usr_id_from)) {
                throw new \InvalidArgumentException("Invalid sender id {$msg_tid}");
            }
            $data['usr_id_from'] = $this->user_handler->getsetUserId($usr_id_from);
        }

        //validate timestamp
        if (empty($message['date'])) {
            throw new \InvalidArgumentException("Empty date for {$msg_tid}");
        }

        $msg_date = Tools::date_convert($message['date']);
        if (empty($msg_date)) {
            throw new \InvalidArgumentException("Invalid date for {$msg_tid}");
        }
        $data['msg_date'] = $msg_date;

        //message text
        $msg_title = null;
        if (!empty($message['message'])) {
            $msg_title = trim(mb_convert_encoding($message['message'], 'UTF-8'));
        }
        $data['msg_title'] = $msg_title;

        if (!empty($message['reply_to_msg_id'])) {

            $reply_to_msg_id = intval($message['reply_to_msg_id']);
            if (!empty($reply_to_msg_id)) {
                $data['msg_id_reply'] = $this->getSetMsgId($reply_to_msg_id, $data['dlg_id']);
            }
        }

        // Check if dialog is already exists
        $rec = self::findFirst([
                'conditions' => 'msg_tid = :msg_tid: AND dlg_id = :dlg_id:',
                'bind' => ['msg_tid' => $msg_tid, 'dlg_id' => $data['dlg_id']]
            ]
        );


        if (empty($rec->msg_created)) {
            $rec = new self;
            $rec->save($data);
            if ($rec->getMessages()) {
                foreach ($rec->getMessages() as $err) {
                    trigger_error($err, E_USER_ERROR);
                }
                throw new \RuntimeException("Failed to insert new message {$data}");
            }

            $this->ldebug('New message',
                ['msg_id' => $rec->msg_id, 'dlg_id' => $rec->dlg_id, 'title' => $msg_title]);
        } else {
            // Compare new and old records
            $diff = [];
            foreach ($data as $field => $value) {
                if (empty($rec->$field) or $rec->$field != $value) {
                    $diff[$field] = $value;
                }
            }
            if (!empty($diff)) {
                $rec->update($diff);
                $this->ldebug('Updating existing message',
                    ['msg_id' => $rec->msg_id, 'dlg_id' => $rec->dlg_id, 'title' => $msg_title] + $diff);
            }
        }


        if (isset($rec->usr_id_from)) {
            $prt_id = $this->participant_handler->upsert($rec->dlg_id, $rec->usr_id_from, null, false);
        }

        if (!empty($rec->msg_title)) {
            try {
                //insert url from msg if exist
                $this->urls_handler->handle_telega_msg($rec->msg_title, $rec->msg_id);
            } catch (\Exception $e) {
                $this->ldebug($e->getMessage());
            }

            try {
                //insert @ from msg if exist
                $this->at_telegram_handler->process($rec->msg_id, $rec->msg_title);
            } catch (\Exception $e) {
                $this->ldebug($e->getMessage());
            }

            $addrs = Tools::getEthFromUrl($rec->msg_title);
            if (!empty($addrs)) {
                foreach ($addrs as $addr) {
                    $adr_id = $this->addr_handler->process($addr);

                    $this->message_addr_handler->process($adr_id, $rec->msg_id);
                }
            }

            $this->msgs_coins->process($rec->msg_id, $rec->msg_title);
        }

        return $rec->msg_id;
    }

    public function getSetMsgId($msg_tid, $dlg_id)
    {
        if (empty($msg_tid)) {
            throw new \InvalidArgumentException('Empty msg_tid');
        }
        if (empty($dlg_id)) {
            throw new \InvalidArgumentException('Empty dlg_id');
        }

        $msg_tid = intval($msg_tid);
        $dlg_id = intval($dlg_id);

        if (empty($msg_tid)) {
            throw new \InvalidArgumentException('Invalid msg_tid');
        }
        if (empty($dlg_id)) {
            throw new \InvalidArgumentException('Invalid dlg_id');
        }

        $rec = self::findFirst([
            'conditions' => 'msg_tid = :msg_tid: AND dlg_id = :dlg_id:',
            'bind' => ['msg_tid' => $msg_tid, 'dlg_id' => $dlg_id]
        ]);

        if (empty($rec->msg_id)) {
            $msg_model = new self;
            $msg_model->init($this->getLogger());
            $msg = [
                'id' => $msg_tid,
                '_' => 'will_be_filled_in_future',
                'date' => time(),
                'to_id' => [
                    'channel_id' => $dlg_id
                ]
            ];
            $msg_id = $msg_model->process($msg);
            return $msg_id;
        }

        return $rec->msg_id;
    }

    public function setPinnedMsg($dlg_id, $msg_tid)
    {
        if (empty($dlg_id)) {
            throw new \InvalidArgumentException('Empty dlg_id');
        }

        $dlg_id = intval($dlg_id);

        if (empty($dlg_id)) {
            throw new \InvalidArgumentException('Invalid dlg_id');
        }

        if (!isset($msg_tid)) {
            throw new \InvalidArgumentException('Empty msg_tid');
        }

        $msg_tid = intval($msg_tid);

        if ($msg_tid === 0) {
            $this->logger->debug('Unsetting pinned msg. This isn`t processed', ['msg_tid' => $msg_tid, 'dlg_id' => $dlg_id]);
            return;
        }

        if (empty($msg_tid)) {
            throw new \InvalidArgumentException('Invalid msg_id');
        }

        $exist = self::findFirst([
            'conditions' => 'dlg_id = :dlg_id: AND msg_tid = :msg_tid:',
            'bind' => ['dlg_id' => $dlg_id, 'msg_tid' => $msg_tid]
        ]);

        if (empty($exist->msg_id)) {

            $msg_model = new self;
            $msg = [
                'id' => $msg_tid,
                '_' => 'will_be_filled_in_future',
                'date' => time(),
                'to_id' => [
                    'channel_id' => $dlg_id
                ],
                'msg_is_pinned' => true,
                'msg_pin_created' => 'now()'
            ];
            $msg_model->process($msg);
            $this->logger->debug("Insert empty msg {$msg_tid} for dlg_id {$dlg_id} and set msg_is_pinned");
        }
        else {
            $exist->update([
                'msg_is_pinned' => true,
                'msg_pin_created' => 'now()'
            ]);
            if ($exist->getMessages()) {
                foreach ($exist->getMessages() as $err) {
                    trigger_error($err, E_USER_ERROR);
                }
                throw new \RuntimeException("Failed to pin msg {$msg_tid} for dlg_id {$dlg_id}");
            }

            $this->logger->debug("Set pinned msg {$msg_tid} for dlg_id {$dlg_id}");
        }


    }

}


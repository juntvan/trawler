<?php

namespace App\Models\Icos\Icos;

use App\Lib\Tools;
use App\Models\Hosts;
use App\Models\Icos\Base;
use App\Models\Icos\Icos;
use App\Models\Telegram\Telegramat;
use App\Models\Urls;
use GuzzleHttp\Exception\BadResponseException;
use Sunra\PhpSimple\HtmlDomParser;

class Sources extends Base
{
    private $guzzle;
    private $urls_handler;
    private $source_handler;
    private $ico_handler;
    private $telegram_at;

    public function initialize()
    {
        parent::initialize();
        $this->setLogger(\Phalcon\Di::getDefault()->getLogger());
        $this->setSource('icos_sources');
        $this->guzzle = new \GuzzleHttp\Client();

        $this->urls_handler = new Urls();
        $this->urls_handler->init($this->getLogger());
        $this->ico_handler = new Icos();
        $this->ico_handler->init($this->getLogger());
        $this->source_handler = new \App\Models\Icos\Sources();
        $this->source_handler->init($this->getLogger());
        $this->telegram_at = new Telegramat();
        $this->telegram_at->init($this->getLogger());

        $this->belongsTo('url_id_link_to', 'App\Models\Urls', 'url_id', ['alias' => 'linkto']);
        $this->belongsTo('url_id_whitepaper', 'App\Models\Urls', 'url_id', ['alias' => 'whitepaper']);
        $this->belongsTo('url_id_telegram', 'App\Models\Urls', 'url_id', ['alias' => 'telegram']);
        $this->belongsTo('tat_id', 'App\Models\Telegram\Telegramat', 'tat_id', ['alias' => 'at']);
        $this->belongsTo('src_id', 'App\Models\Icos\Sources', 'src_id', ['alias' => 'src']);
        //        $this->belongsTo('adr_id', 'App\Models\Coins\Addresses', 'adr_id', ['alias' => 'adr']);
    }

    public function handlingSources()
    {
        $this->icoalert();
        $this->icohotlist();
        $this->topicolist();
        $this->cryptocompare();
//        $this->icomarks();//not finished
        $this->cointelligence();
    }

    private function icoalert()
    {
        $perpage = 10000;
        $time = time();

        $site = 'https://www.icoalert.com';

        $this->logger->debug('Handling source', ['src' => $site]);

        $src_id = $this->source_handler->process($site);
        $src = $this->source_handler->findFirst("src_id = $src_id");

        $request_types = [
            'icoalert-production', //upcoming icos
            'icoalert-production-active', //active icos
            'icoalert-production-presale', //upcoming presale
            'icoalert-production-active-presale' //active presale
        ];

        $filters = [
            'icoalert-production' => "startDate > {$time} OR startDate = -1 OR endDate = -1",
            'icoalert-production-active' => "startDate <= {$time} AND endDate > {$time} AND startDate > 0",
            'icoalert-production-presale' => "preSaleStartDate > {$time} OR preSaleStartDate = -1 OR preSaleEndDate = -1",
            'icoalert-production-active-presale' => "preSaleStartDate <= {$time} AND preSaleEndDate > {$time} AND preSaleStartDate > 0 AND preSaleEndDate > 0",
        ];

        $url = 'https://jbmtbl811x-dsn.algolia.net/1/indexes/*/queries?x-algolia-agent=Algolia%20for%20vanilla%20JavaScript%20(lite)%203.21.1%3Binstantsearch.js%201.11.7%3BJS%20Helper%202.19.0&x-algolia-application-id=JBMTBL811X&x-algolia-api-key=bab8508db4e902d54d1603e9d897a285';

        foreach ($request_types as $request_type) {
            $page = 0;

            $filter = rawurlencode($filters[$request_type]);

            while (1) {

                if ($request_type === 'icoalert-production-presale' || $request_type === 'icoalert-production-active-presale') {
                    $param = "query=&hitsPerPage={$perpage}&page={$page}&restrictSearchableAttributes=%5B%22title%22%2C%22description%22%2C%22tags%22%5D&attributesToRetrieve=%5B%22tags%22%2C%22tbd%22%2C%22preSale%22%2C%22website%22%2C%22reportAvailable%22%2C%22reportLink%22%2C%22featuredListing%22%2C%22preFeaturedListing%22%2C%22verified%22%2C%22startDate%22%2C%22startDay%22%2C%22startMonth%22%2C%22startQuarter%22%2C%22startYear%22%2C%22endDate%22%2C%22preSaleTbd%22%2C%22preSaleTimeframeType%22%2C%22preSaleStartDate%22%2C%22preSaleStartDay%22%2C%22preSaleStartMonth%22%2C%22preSaleStartQuarter%22%2C%22preSaleStartYear%22%2C%22preSaleEndDate%22%2C%22kyc%22%2C%22usExcludedOption%22%2C%22dateCreated%22%2C%22craftEntryId%22%2C%22title%22%2C%22description%22%2C%22startDay%22%2C%22startMonth%22%2C%22startQuarter%22%2C%22startYear%22%2C%22preSaleStartDay%22%2C%22preSaleStartMonth%22%2C%22preSaleStartQuarter%22%2C%22preSaleStartYear%22%5D&filters={$filter}&facets=%5B%22preSale%22%5D&tagFilters=&facetFilters=%5B%22preSale%3Atrue%22%5D";
                } else {
                    $param = "query=&hitsPerPage={$perpage}&page={$page}&restrictSearchableAttributes=%5B%22title%22%2C%22description%22%2C%22tags%22%5D&attributesToRetrieve=%5B%22tags%22%2C%22tbd%22%2C%22preSale%22%2C%22website%22%2C%22reportAvailable%22%2C%22reportLink%22%2C%22featuredListing%22%2C%22preFeaturedListing%22%2C%22verified%22%2C%22startDate%22%2C%22startDay%22%2C%22startMonth%22%2C%22startQuarter%22%2C%22startYear%22%2C%22endDate%22%2C%22preSaleTbd%22%2C%22preSaleTimeframeType%22%2C%22preSaleStartDate%22%2C%22preSaleStartDay%22%2C%22preSaleStartMonth%22%2C%22preSaleStartQuarter%22%2C%22preSaleStartYear%22%2C%22preSaleEndDate%22%2C%22kyc%22%2C%22usExcludedOption%22%2C%22dateCreated%22%2C%22craftEntryId%22%2C%22title%22%2C%22description%22%2C%22startDay%22%2C%22startMonth%22%2C%22startQuarter%22%2C%22startYear%22%2C%22preSaleStartDay%22%2C%22preSaleStartMonth%22%2C%22preSaleStartQuarter%22%2C%22preSaleStartYear%22%5D&filters={$filter}&facets=%5B%5D&tagFilters=";
                }
                $params = [
                    'requests' => [
                        [
                            'indexName' => $request_type,
                            'params' => $param
                        ]
                    ]
                ];

                $params = json_encode($params);

                if ($params === false || $params === null) {
                    $src->incrFail();
                    throw new \InvalidArgumentException('json_encode error');
                }

                try {
                    $response = $this->guzzle->request('POST', $url, ['body' => $params]);
                } catch (\GuzzleHttp\Exception\RequestException $e) {
                    $this->logger->error('request error',
                        ['error' => $e->getRequest()->getBody(), 'response' => $e->getResponse()->getBody(), "code"
                        => $e->getCode()]);
                    $src->incrFail();
                    $this->logger->error('exit', ["code_line" => __LINE__]);
                    exit;
                }

                $response = json_decode($response->getBody(), true);

                if ($response === false || $response === null) {
                    $src->incrFail();
                    throw new \InvalidArgumentException('json_decode error');
                }

                if (empty($response['results'])) {
                    $src->incrFail();
                    throw new \InvalidArgumentException('Empty response result');
                }

                if (empty($response['results'][0])) {
                    $src->incrFail();
                    throw new \InvalidArgumentException('Empty results[0]');
                }

                if (empty($response['results'][0]['hits'])) {
                    $this->logger->debug('Empty hits',
                        ['reqest_type' => $request_type, 'page' => $page, 'perpage' => $perpage]);
                    break;
                }

                foreach ($response['results'][0]['hits'] as $key => $ico) {
                    if (empty($ico['website'])) {
                        continue;
                    }
                    try {
                        $ico_id = $this->ico_handler->upsert($ico['title'], $ico['website'], null);
                    } catch (\InvalidArgumentException $e) {
                        $this->logger->error('', [$e->getMessage()]);
                        continue;
                    }
                    $this->upsert($ico_id, $src_id, Tools::convert_any_date($ico['startDate']),
                        Tools::convert_any_date($ico['endDate']),
                        Tools::convert_any_date($ico['preSaleStartDate']),
                        Tools::convert_any_date($ico['preSaleEndDate']), null, null,
                        $ico['verified'], null, null, null, null, $ico['kyc'],
                        null, null, null, null, null, null);
                }

                $page++;
            }
        }

        $src->incrSuc();
    }

    private function icohotlist()
    {
        $site = 'https://www.icohotlist.com/';

        $src_id = $this->source_handler->process($site);
        $src = $this->source_handler->findFirst("src_id = $src_id");

        $this->logger->debug('Get list of all pools from page', ['src' => $site]);
        $links_to_icos = null;

        try {
            $links_to_icos = $this->getListofIcosFromSite($site, '/https:\/\/www.icohotlist.com\/ico\/[\w-]+/', $src, 0);
        } catch (\Exception $e) {
            $this->logger->debug('', ['e' => $e->getMessage()]);
        }

        if (empty($links_to_icos)) {
            $this->logger->debug('Empty links to icos');
        }

        foreach ($links_to_icos as $ico_page) {
            list($ico_id, $title, $status, $edate, $sdate, $website, $whitepaper, $url_id_whitepaper, $teleg,
                $url_id_telegram, $hardcap, $softcap, $psdate, $pedate, $symbol, $hardcap_currency,
                $softcap_currency, $tat_id)
                = [
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
            ];
            $kyc = false;

            $this->logger->debug('Handle link', [$ico_page]);
            sleep(1);
            $attempt = 0;
            while (1) {
                try {
                    $response = $this->guzzle->request('GET', $ico_page);
                } catch (\GuzzleHttp\Exception\RequestException $e) {
                    $this->logger->error('request error',
                        ['error' => $e->getRequest()->getBody(), 'response' => $e->getResponse()->getBody(), "code"
                        => $e->getCode()]);
                    if ($e->getCode() === 504 && $attempt < 5) {
                        sleep(5);
                        $attempt++;
                        continue;
                    }
                    $src->incrFail();
                    $this->logger->error('exit', ["code_line" => __LINE__]);
                    exit;
                }
                break;
            }

            $html = $response->getBody()->getContents();

            if (empty($html)) {
                $this->logger->debug('Fail to get. Continue');
                $src->incrFail();
                continue;
            }
            $dom = HtmlDomParser::str_get_html($html);

            if (empty($dom)) {
                $this->logger->debug('Fail to parse html. Continue');
                $src->incrFail();
                continue;
            }

            $frst_blk = $dom->find('div.ico-single-overview');
            if (empty($frst_blk)) {
                $this->logger->debug('div.ico-single-overview not found. Continue');
                $src->incrFail();
                continue;
            }
            $frst_blk = reset($frst_blk);

            $h1 = $frst_blk->find('h1', 0);
            if (!empty($h1)) {
                $title = $h1->children[0]->text();
            }

            if (empty($title)) {
                $this->logger->debug('Fail to get title of ico. Continue');
                $src->incrFail();
                continue;
            }

            $ps = $frst_blk->find('p');
            if (empty($ps)) {
                $this->logger->debug('Fail to get first block of info from site. Continue');
                $src->incrFail();
                continue;
            }

            $prev = '';
            foreach ($ps as $p) {
                if ($prev === 'Status') {
                    $status = trim($p->text());
                    if ($status === 'Past') {
                        break;
                    }
                } elseif ($prev === 'Starting date') {
                    $sdate = trim($p->text());
                    if (!empty($sdate)) {
                        $sdate = Tools::convert_any_date(strtotime($sdate));
                    }
                } elseif ($prev === 'Ending date') {
                    $edate = trim($p->text());
                    if (!empty($edate)) {
                        $edate = Tools::convert_any_date(strtotime($edate));
                    }
                } elseif ($prev === 'Website') {
                    $website = $p->find('a', 0);
                    if (!empty($website)) {
                        $website = $website->href;
                    }
                } elseif ($prev === 'Whitepaper') {
                    $whitepaper = $p->find('a', 0);
                    if (!empty($whitepaper)) {
                        $whitepaper = $whitepaper->href;
                    }
                    break;
                }
                $prev = trim($p->text());
            }

            if (strpos($status, 'Past') !== false) {
                $this->logger->debug('Past ico. Continue');
                continue;
            }

            if (empty($website)) {
                $this->logger->debug('Empty website of ico. Continue');
                continue;
            }

            $links = $frst_blk->find('div.ico-fa-links');
            if (!empty($links)) {
                $links = reset($links);
                foreach ($links->children as $child) {
                    if (strpos($child->title, 'Telegram') !== false) {
                        $teleg = $child->href;
                        break;
                    }
                }
            }

            $scnd_blck = $dom->find('div[id=ico-details]');
            if (empty($scnd_blck)) {
                $this->logger->debug('div[id=ico-details] not found. Continue');
                $src->incrFail();
                continue;
            }

            $scnd_blck = reset($scnd_blck);
            $divs = $scnd_blck->find('div');
            if (empty($divs)) {
                $this->logger->debug('Fail to get divs from second block. Continue');
                $src->incrFail();
                continue;
            }

            $prev = '';
            foreach ($divs as $div) {
                if ($prev === 'Token Sale Hard Cap') {
                    $converted = $this->convertMoney($div->text());
                    if ($converted) {
                        $hardcap = $converted[0];
                        $hardcap_currency = $converted[1];
                    }
                } elseif ($prev === 'Token Sale Soft Cap') {
                    $converted = $this->convertMoney($div->text());
                    if ($converted) {
                        $softcap = $converted[0];
                        $softcap_currency = $converted[1];
                    }
                } elseif ($prev === 'Presale Start Date') {
                    $psdate = trim($div->text());
                    if (!empty($psdate)) {
                        $psdate = Tools::convert_any_date(strtotime($psdate));
                    }
                } elseif ($prev === 'Presale End Date') {
                    $pedate = trim($div->text());
                    if (!empty($pedate)) {
                        $pedate = Tools::convert_any_date(strtotime($pedate));
                    }
                } elseif ($prev === 'Token Symbol') {
                    $symbol = strtoupper(trim($div->text()));
                    if (strlen($symbol) > 10) {
                        $symbol = null;
                    }
                } elseif ($prev === 'KYC') {
                    if (strpos($div->text(), 'Yes') !== false) {
                        $kyc = true;
                    }
                    break;
                }
                $prev = trim($div->text());
            }


            $ico_id = $this->ico_handler->upsert($title, $website, $symbol);

            //whitepaper & telegram urls convert to id
            if (!empty($whitepaper)) {
                $url_id_whitepaper = $this->urls_handler->process($whitepaper, false);
            }
            if (!empty($teleg)) {
                if (strpos($teleg, '@') !== false) {
                    try {
                        $tat_id = $this->telegram_at->process(null, $teleg);
                        $tat_id = empty($tat_id) ? null : reset($tat_id);
                    } catch (\RuntimeException $e) {
                        $this->logger->debug('', $e->getMessage());
                    }
                } else {
                    $url_id_telegram = $this->urls_handler->process($teleg, false);
                }
            }

            $url_id_link_to = $this->urls_handler->process($ico_page, false);

            $this->upsert($ico_id, $src_id, $sdate, $edate, $psdate, $pedate, $hardcap, $softcap,
                null, null, $url_id_whitepaper, $url_id_telegram, $url_id_link_to, $kyc, null,
                null, null, $hardcap_currency, $softcap_currency, $tat_id);

            $dom->clear();
        }

        $src->incrSuc();
    }

    private function topicolist()
    {
        $host = 'https://topicolist.com';
        $sites = [
            'https://topicolist.com/upcoming-icos',
            'https://topicolist.com/ongoing-icos',
            'https://topicolist.com/pre-icos'
        ];

        $src_id = $this->source_handler->process($sites[0]);
        $src = $this->source_handler->findFirst("src_id = $src_id");

        foreach ($sites as $site) {
            $this->logger->debug('Get list of all pools from page', ['src' => $site]);
            $links_to_icos = null;

            try {
                $links_to_icos = $this->getListofIcosFromSite($site, '/(href=")(\/ico\/[\w-]+)/', $src, 2);
            } catch (\Exception $e) {
                $this->logger->debug('', ['e' => $e->getMessage()]);
            }

            if (empty($links_to_icos)) {
                $this->logger->debug('Empty links to icos');
                $src->incrFail();
                continue;
            }

            foreach ($links_to_icos as $ico_page) {
                list($ico_id, $title, $edate, $sdate, $website, $whitepaper, $url_id_whitepaper, $teleg,
                    $url_id_telegram, $psdate, $pedate, $symbol, $rate, $tat_id) = [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                ];
                $kyc = false;
                $ico_page = $host . $ico_page;

                $this->logger->debug('Handle link', [$ico_page]);
                try {
                    $response = $this->guzzle->request('GET', $ico_page);
                } catch (\GuzzleHttp\Exception\RequestException $e) {
                    $this->logger->error('request error',
                        ['error' => $e->getRequest(), 'response' => $e->getResponse()]);
                    $src->incrFail();
                    continue;
                }

                $html = $response->getBody()->getContents();

                if (empty($html)) {
                    $this->logger->debug('Fail to get html. Continue');
                    $src->incrFail();
                    continue;
                }

                $dom = HtmlDomParser::str_get_html($html);

                if (empty($dom)) {
                    $this->logger->debug('Fail to parse html. Continue');
                    $src->incrFail();
                    continue;
                }

                $aside = $dom->find('aside', 0);
                if (empty($aside)) {
                    $this->logger->debug('Aside tag not found. Continue');
                    $src->incrFail();
                    continue;
                }
                $div = $aside->find('div', 0);
                if (empty($div)) {
                    $this->logger->debug('Div in aside not found. Continue');
                    $src->incrFail();
                    continue;
                }

                while ($div) {
                    $text = $div->text();
                    if (strpos($text, 'Rating') !== false) {
                        $lastspan = trim($div->find('span', -1)->nodes[0]->text());
                        if (!empty($lastspan)) {
                            if (is_numeric($lastspan)) {
                                $rate = $lastspan;
                            } elseif (strpos($lastspan, 'BBB') !== false) {
                                $rate = 2.6;
                            } elseif (strpos($lastspan, 'BB') !== false) {
                                $rate = 1.8;
                            } elseif (strpos($lastspan, 'B') !== false) {
                                $rate = 1;
                            } elseif (strpos($lastspan, 'AAA') !== false) {
                                $rate = 5;
                            } elseif (strpos($lastspan, 'AA') !== false) {
                                $rate = 4.2;
                            } elseif (strpos($lastspan, 'A') !== false) {
                                $rate = 3.4;
                            }
                        }
                    } elseif (strpos($text, 'Pre ICO') !== false) {
                        $dates = $div->find('div[class=tags] div[class=ico-bold]');
                        if (!empty($dates)) {
                            if (isset($dates[0])) {
                                $psdate = Tools::convert_any_date(strtotime(trim($dates[0]->text())));
                            }
                            if (isset($dates[1])) {
                                $pedate = Tools::convert_any_date(strtotime(trim($dates[1]->text())));
                            }
                        }
                    } elseif (strpos($text, 'ICO Main Sale') !== false) {
                        $dates = $div->find('div[class=tags] div[class=ico-bold]');
                        if (!empty($dates)) {
                            if (isset($dates[0])) {
                                $sdate = Tools::convert_any_date(strtotime(trim($dates[0]->text())));
                            }
                            if (isset($dates[1])) {
                                $edate = Tools::convert_any_date(strtotime(trim($dates[1]->text())));
                            }
                        }
                    } elseif (strpos($text, 'Information') !== false) {
                        $token = $div->find('div[class=tags] div[class=ico-bold]', 0);
                        if (!empty($token) && strpos($token->prev_sibling()->text(), 'TOKEN') !== false) {
                            $symbol = trim($token->text());
                            $symbol = explode(' ', $symbol);
                            $symbol = $symbol[0];
                        }
                    } elseif (strpos($text, 'Restrictions') !== false) {
                        $divparts = $div->find('div[class="w-condition-invisible"]');
                        foreach ($divparts as $value) {
                            if (strpos($value->text(), 'KYC Required') === false) {
                                $kyc = true;
                                break;
                            }
                        }
                        break;
                    }

                    $div = $div->next_sibling();
                }

                $website = $dom->find('header a[title="ICO official website"]');
                if (empty($website)) {
                    $this->logger->debug('Empty website. Continue');
                    $src->incrFail();
                    continue;
                }
                $website = $website[0]->href;
                if (empty($website)) {
                    $this->logger->debug('Empty website. Continue');
                    $src->incrFail();
                    continue;
                }

                $whitepaper = $dom->find('header a[title="ICO whitepaper"]');
                if (!empty($whitepaper)) {
                    $whitepaper = trim($whitepaper[0]->href);
                    if (!empty($whitepaper)) {
                        $url_id_whitepaper = $this->urls_handler->process($whitepaper, false);
                    }
                }

                $teleg = $dom->find('header a[title="ICO on telegram"]');
                if (!empty($teleg)) {
                    $teleg = $teleg[0]->href;
                    if (!empty($teleg)) {

                        if (strpos($teleg, '@') !== false) {
                            try {
                                $tat_id = $this->telegram_at->process(null, $teleg);
                                $tat_id = empty($tat_id) ? null : reset($tat_id);
                            } catch (\RuntimeException $e) {
                                $this->logger->debug('', $e->getMessage());
                            }
                        } else {
                            $url_id_telegram = $this->urls_handler->process($teleg, false);
                        }
                    }
                }

                $title = $dom->find('header h1 text');
                if (empty($title)) {
                    $this->logger->debug('Empty title. Continue');
                    $src->incrFail();
                    continue;
                }
                $title = $title[0]->text();

                $ico_id = $this->ico_handler->upsert($title, $website, $symbol);

                $url_id_link_to = $this->urls_handler->process($ico_page, false);

                $this->upsert($ico_id, $src_id, $sdate, $edate, $psdate, $pedate, null, null,
                    null, $rate, $url_id_whitepaper, $url_id_telegram, $url_id_link_to, $kyc, null, null, null,
                    null, null, $tat_id);

                $dom->clear();
            }
        }
        $src->incrSuc();
    }

    private function cryptocompare()
    {
        $host = 'https://www.cryptocompare.com';

        $sites = [
            'https://www.cryptocompare.com/ico/#/upcoming',
            'https://www.cryptocompare.com/ico/#/ongoing'
        ];

        $src_id = $this->source_handler->process($sites[0]);
        $src = $this->source_handler->findFirst("src_id = $src_id");

        foreach ($sites as $site) {
            $this->logger->debug('Get list of all pools from page', ['src' => $site]);
            $links_to_icos = null;

            try {
                $links_to_icos = $this->getListofIcosFromSite($site, '/\{"I.+?Url":"(\/coins\/[\w-]+?\/overview).+?Status":"(Ongoing|Upcoming|Finished)".+?\}/', $src, -1);
            } catch (\Exception $e) {
                $this->logger->debug('', ['e' => $e->getMessage()]);
                $src->incrFail();
                continue;
            }

            if (empty($links_to_icos)) {
                $this->logger->debug('Empty links to icos');
                $src->incrFail();
                continue;
            }

            foreach ($links_to_icos[2] as $key => $v) {
                if ($v === 'Finished') {
                    unset($links_to_icos[1][$key]);
                }
            }

            $links_to_icos = $links_to_icos[1];

            if (empty($links_to_icos)) {
                $this->logger->debug('Empty links to icos');
                $src->incrFail();
                continue;
            }

            foreach ($links_to_icos as $ico_page) {
                list($ico_id, $title, $edate, $sdate, $website, $whitepaper, $url_id_whitepaper, $hardcap, $softcap, $fundingRaisedListUSD,
                    $pedate, $symbol, $fundingRaisedList, $fundingRaisedListCurrency, $hardcap_currency,
                    $softcap_currency) = [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                ];
                $kyc = false;
                $ico_page = $host . $ico_page;

                $this->logger->debug('Handle link', [$ico_page]);
                try {
                    $response = $this->guzzle->request('GET', $ico_page);
                } catch (\GuzzleHttp\Exception\RequestException $e) {
                    $this->logger->error('request error',
                        ['error' => $e->getRequest()->getBody(), 'response' => $e->getResponse()->getBody(), "code"
                        => $e->getCode()]);
                    $src->incrFail();
                    $this->logger->error('exit', ["code_line" => __LINE__]);
                    exit;
                }

                $html = $response->getBody()->getContents();

                if (empty($html)) {
                    $this->logger->debug('Fail to get. Continue');
                    $src->incrFail();
                    continue;
                }

                $script = null;
                preg_match_all('/pageInfo\.setCoinPageInfo\((.+?)\);\s+pageInfo/', $html, $script);

                if (empty($script[1])) {
                    $this->logger->debug('Data not found. Continue');
                    continue;
                }

                $script = json_decode($script[1][0], true);

                if (empty($script)) {
                    $this->logger->debug('Empty data after json_decode. Continue');
                    continue;
                }

                if ($script['Response'] !== 'Success' || empty($script['Data'])) {
                    $this->logger->debug('Invalid data after json_decode. Continue');
                    continue;
                }

                $script = $script['Data'];
                if (isset($script['ICO']['WebsiteLink']) && strpos($script['ICO']['WebsiteLink'], 'N/A') === false) {
                    $website = trim($script['ICO']['WebsiteLink']);
                    if (!empty($website)) {
                        $website = explode(' ', $website);
                        $website = $website[0];
                    }
                }
                if (empty($website)) {
                    $this->logger->debug('Empty website. Continue');
                    continue;
                }

                if (isset($script['General']['H1Text']) && strpos($script['General']['H1Text'], 'N/A') === false) {
                    $title = trim($script['General']['H1Text']);
                }
                if (isset($script['General']['Symbol']) && strpos($script['General']['Symbol'], 'N/A') === false) {
                    $symbol = $script['General']['Symbol'];
                }
                if (isset($script['ICO']['WhitePaperLink']) && strpos($script['ICO']['WhitePaperLink'], 'N/A') === false) {
                    $whitepaper = trim($script['ICO']['WhitePaperLink']);
                    $url_id_whitepaper = $this->urls_handler->process($whitepaper, false);
                }

                if (isset($script['ICO']['FundsRaisedUSD'])) {
                    $script['ICO']['FundsRaisedUSD'] = preg_replace('/\.\d+/', '', $script['ICO']['FundsRaisedUSD']);
                    $converted = $this->convertMoney($script['ICO']['FundsRaisedUSD']);
                    if ($converted) {
                        $fundingRaisedListUSD = $converted[0];
                    }
                }

                if (isset($script['ICO']['FundsRaisedList'])) {
                    $script['ICO']['FundsRaisedList'] = preg_replace('/\.\d+/', '', $script['ICO']['FundsRaisedList']);

                    $converted = $this->convertMoney($script['ICO']['FundsRaisedList']);
                    if ($converted) {
                        $fundingRaisedList = $converted[0];
                        $fundingRaisedListCurrency = $converted[1];
                        if (strlen($fundingRaisedListCurrency) > 8) {
                            $fundingRaisedListCurrency = null;
                        }
                    }
                }

                if (empty($fundingRaisedListUSD) && !empty($fundingRaisedList) && $fundingRaisedListCurrency === 'USD') {
                    $fundingRaisedListUSD = $fundingRaisedList;
                }

                if (isset($script['ICO']['FundingTarget'])) {
                    $converted = $this->convertMoney($script['ICO']['FundingTarget']);
                    if ($converted) {
                        $softcap = $converted[0];
                        $softcap_currency = $converted[1];
                        if (strlen($softcap_currency) > 8) {
                            $softcap_currency = null;
                        }
                    }
                }

                if (isset($script['ICO']['FundingCap'])) {
                    $converted = $this->convertMoney($script['ICO']['FundingCap']);
                    if ($converted) {
                        $hardcap = $converted[0];
                        $hardcap_currency = $converted[1];
                        if (strlen($hardcap_currency) > 8) {
                            $hardcap_currency = null;
                        }
                    }
                }

                if ($script['ICO']['Date']) {
                    $sdate = Tools::convert_any_date(trim($script['ICO']['Date']));
                }

                if ($script['ICO']['EndDate']) {
                    $edate = Tools::convert_any_date(trim($script['ICO']['EndDate']));
                }


                $ico_id = $this->ico_handler->upsert($title, $website, $symbol);
                $url_id_link_to = $this->urls_handler->process($ico_page, false);

                $this->upsert($ico_id, $src_id, $sdate, $edate, null, null, $hardcap, $softcap,
                    null, null, $url_id_whitepaper, null, $url_id_link_to, null, $fundingRaisedList,
                    $fundingRaisedListUSD, $fundingRaisedListCurrency, $hardcap_currency, $softcap_currency);

            }

        }
        $src->incrSuc();
    }

    //TODO: need rework
    private function icomarks()
    {
        $host = 'https://icomarks.com';

        $statuses = [
            'upcoming',
            'presale_active',
            'presale_ended',
            'active'
        ];

        $site = 'https://icomarks.com/icos/ajax_more';

        $src_id = $this->source_handler->process($site);
        $src = $this->source_handler->findFirst("src_id = $src_id");

        foreach ($statuses as $status) {
            $offset = 0;

            while (1) {
                $this->logger->debug('Handling', ['site' => $site, 'status' => $status, 'offset' => $offset]);
                if ($offset === false) {
                    break;
                }

                $postData = [
                    'offset' => $offset,
                    'status' => $status
                ];

                try {
                    $response = $this->guzzle->request('POST', $site, ['form_params' => $postData]);
                } catch (\GuzzleHttp\Exception\RequestException $e) {
                    $this->logger->error('request error',
                        ['error' => $e->getRequest()->getBody(), 'response' => $e->getResponse()->getBody(), "code"
                        => $e->getCode()]);
                    $src->incrFail();
                    $this->logger->error('exit', ["code_line" => __LINE__]);
                    exit;
                }

                $decoded = json_decode($response->getBody(), true);
                if (empty($decoded)) {
                    $this->logger->debug('decode fail. Continue');
                    $src->incrFail();
                    continue;

                }
                if (!isset($decoded['content']) || !isset($decoded['offset'])) {
                    $this->logger->debug('Empty content or offset not set. Continue', ['decoded' => $decoded]);
                    $src->incrFail();
                    continue;
                }
                $html = $decoded['content'];
                if (empty($html)) {
                    $this->logger->debug('Empty content. Continue', ['decoded' => $decoded]);
                    $src->incrFail();
                    continue;
                }
                if ($decoded['offset'] > $offset) {
                    $offset = $decoded['offset'];
                } else {
                    $offset = false;
                }

                $links_to_icos = null;
                try {
                    $links_to_icos = $this->getListofIcosFromHtml($html, '/(href=")(\/ico\/[\w-]+)/', 2);
                } catch (\Exception $e) {
                    $this->logger->debug('', ['e' => $e->getMessage()]);
                }

                if (empty($links_to_icos)) {
                    $this->logger->debug('Empty links to icos');
                    $src->incrFail();
                    continue;
                }

                foreach ($links_to_icos as $ico_page) {
                    list($ico_id, $title, $edate, $sdate, $website, $whitepaper, $url_id_whitepaper, $teleg, $url_id_telegram, $psdate, $pedate, $symbol, $rate) = [
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null
                    ];
                    $kyc = false;
                    $ico_page = $host . $ico_page;

                    $this->logger->debug('Handle link', [$ico_page]);
                    try {
                        $response = $this->guzzle->request('GET', $ico_page);
                    } catch (\GuzzleHttp\Exception\RequestException $e) {
                        $this->logger->error('request error',
                            ['error' => $e->getRequest(), 'response' => $e->getResponse()]);
                        $src->incrFail();
                        continue;
                    }

                    $html = $response->getBody()->getContents();

                    if (empty($html)) {
                        $this->logger->debug('Fail to get html. Continue');
                        $src->incrFail();
                        continue;
                    }

                    $dom = HtmlDomParser::str_get_html($html);

                    if (empty($dom)) {
                        $this->logger->debug('Fail to parse html. Continue');
                        $src->incrFail();
                        continue;
                    }

                    $rate = $dom->find('div[class=ico-rating-overall]', 0);
                    if (!empty($rate)) {
                        $rate = floatval($rate);
                    }

                    $teleg = $dom->find('a[class=banner-telegram]', 0);
                    if (!empty($teleg)) {
                        $teleg = $teleg->href;
                        if (!empty($teleg)) {
                            $url_id_telegram = $this->urls_handler->process($teleg, false);
                        }
                    }

                    //TODO: too hard to find data without ids.

                    $website = $dom->find('span[text=Website]', 0);
                    //get parent, get a->href
                }
            }
        }

    }

    private function cointelligence()
    {
        $site = 'https://www.cointelligence.com/content/ico_list/page/';
        $page = 1;

        $this->logger->debug('Get list of all pools from page', ['src' => $site]);

        $src_id = $this->source_handler->process($site);
        $src = $this->source_handler->findFirst("src_id = $src_id");

        while (1) {
            $this->logger->debug('Handling ', ['url' => $site.$page]);
            $links_to_icos = null;

            try {
                $links_to_icos = $this->getListofIcosFromSite($site.$page++, '/(a href=")(https:\/\/www\.cointelligence\.com\/content\/ico_list\/[\w-]+)/',
                    $src, 2);
            } catch (\Exception $e) {
                $this->logger->debug('', ['e' => $e->getMessage()]);
            }

            if (empty($links_to_icos)) {
                $this->logger->debug('Empty links to icos');
                break;
            }

            foreach ($links_to_icos as $ico_page) {
                list($ico_id, $title, $edate, $sdate, $website, $whitepaper, $url_id_whitepaper, $teleg,
                    $url_id_telegram, $fundingRaisedListUSD, $funding, $rate, $symbol, $token_url, $capital,
                    $currency, $url_id_link_to, $hardcap, $softcap, $softcap_currency, $hardcap_currency, $tat_id) = [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                ];
                $kyc = false;

                $this->logger->debug('Handle link', [$ico_page]);
                try {
                    $response = $this->guzzle->request('GET', $ico_page);
                } catch (\GuzzleHttp\Exception\RequestException $e) {
                    $this->logger->error('request error',
                        ['error' => $e->getRequest(), 'response' => $e->getResponse()]);
                    $src->incrFail();
                    continue;
                }

                $html = $response->getBody()->getContents();

                if (empty($html)) {
                    $this->logger->debug('Fail to get html. Continue');
                    $src->incrFail();
                    continue;
                }

                $dom = HtmlDomParser::str_get_html($html);

                if (empty($dom)) {
                    $this->logger->debug('Fail to parse html. Continue');
                    $src->incrFail();
                    continue;
                }


                //main part
                $h1 = $dom->find('h1[class=entry-title]', 0);
                if (!empty($h1)) {
                    if (isset($h1->nodes[0])) {
                        $title = trim($h1->nodes[0]->text());
                    }
                    if (isset($h1->nodes[1])) {
                        $symbol = trim($h1->nodes[1]->text());
                        if (strlen($symbol) > 10) {
                            $symbol = null;
                        }
                    }
                }

                $info_section = $dom->find('li > span');

                foreach ($info_section as $section) {
                    $parent = null;
                     if (strpos($section->text(), 'Whitepaper') !== false) {
                         $parent = $section->parent;
                         $a = $parent->find('a', 0);
                         if (!empty($a)) {
                             $whitepaper = $a->href;
                         }
                    } elseif (strpos($section->text(), 'Social') !== false) {
                         $telega_icon = $section->parent->find("i[class='fa-telegram']", 0);
                         if (!empty($telega_icon)) {
                             $teleg = $telega_icon->parent->href;
                         }
                    } elseif (strpos($section->text(), 'Block Explorer') !== false) {
                         $parent = $section->parent;
                         $a = $parent->find('a', 0);
                         if (!empty($a)) {
                             $token_url = $a->href;
                         }
                    } elseif (strpos($section->text(), 'Website') !== false) {
                        $parent = $section->parent;
                        $a = $parent->find('a', 0);
                        if (!empty($a)) {
                            $website = $a->href;
                        }
                        break;
                    }
                }

                if (empty($website)) {
                    $this->logger->debug('Empty website. Continue');
                    continue;
                }

                if (!empty($whitepaper)) {
                    $url_id_whitepaper = $this->urls_handler->process($whitepaper, false);
                }

                if (!empty($teleg)) {
                    if (strpos($teleg, '@') !== false) {
                        try {
                            $tat_id = $this->telegram_at->process(null, $teleg);
                            $tat_id = empty($tat_id) ? null : reset($tat_id);
                        } catch (\RuntimeException $e) {
                            $this->logger->debug('', $e->getMessage());
                        }
                    } else {
                        $url_id_telegram = $this->urls_handler->process($teleg, false);
                    }
                }

                $sdate = $dom->find('span[class="ico-time ico-start"]', 0);
                if (!empty($sdate) && isset($sdate->attr['data-start'])) {
                    $sdate = Tools::convert_any_date(strtotime($sdate->attr['data-start']));
                }

                $edate = $dom->find('span[class="ico-time ico-end"]', 0);
                if (!empty($edate) && isset($edate->attr['data-end'])) {
                    $edate = Tools::convert_any_date(strtotime($edate->attr['data-end']));
                }

                $rate = $dom->find('div[class=analysis-block-1] p[class=row-score]', 0);
                if (!empty($rate)) {
                    $rate = floatval($rate->text());
                }

                $capital = $dom->find('div[class]d-item-scale] div[class=numbers]', 0);
                if (!empty($capital)) {
                    $capital = $capital->text();
                    if (strpos($capital, 'N/A') === false) {
                        $capital = preg_replace('/\(.+?\)/', '', $capital);
                        $capital = trim($capital);
                        $parts = explode(' ', $capital);
                        if (!empty($parts) && !empty($parts[0]) && !empty($parts[1])) {
                            $capital = $parts[0];
                            $currency = $parts[1];
                            $capital = preg_replace('/[\.,\s]/', '', $capital);
                            $funding = intval($capital);
                            if (strpos($currency, 'USD') !== false) {
                                $fundingRaisedListUSD = $capital;
                            }
                        }
                    }
                }

                $soft_info = $dom->find('div[class=soft-cap] span[class=cap-number]', 0);
                if (!empty($soft_info)) {
                    $converted = $this->convertMoney($soft_info->text());
                    if ($converted) {
                        $softcap = $converted[0];
                        $softcap_currency = $converted[1];
                    }
                }

                $hard_info = $dom->find('div[class=max-cap] span[class=cap-number]', 0);
                if (!empty($hard_info)) {
                    $converted = $this->convertMoney($hard_info->text());
                    if ($converted) {
                        $hardcap = $converted[0];
                        $hardcap_currency = $converted[1];
                    }
                }

                $ico_id = $this->ico_handler->upsert($title, $website, $symbol, $token_url);
                $url_id_link_to = $this->urls_handler->process($ico_page, false);

                $this->upsert($ico_id, $src_id, $sdate, $edate, null, null, $hardcap, $softcap,
                    null, $rate, $url_id_whitepaper, $url_id_telegram, $url_id_link_to, null, $funding,
                    $fundingRaisedListUSD, $currency, $hardcap_currency, $softcap_currency, $tat_id);
                $dom->clear();
            }
        }
        $src->incrSuc();
    }

    private function getListofIcosFromSite($site, $regex, $src, $indexToReturn = 0)
    {
        if (empty($site)) {
            throw new \RuntimeException('Empty site ');
        }

        if (empty($regex)) {
            throw new \RuntimeException('Empty regex ');
        }

        if (empty($src)) {
            throw new \RuntimeException('Empty src ');
        }

        try {
            $response = $this->guzzle->request('GET', $site);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $this->logger->error('request error',
                ['error' => $e->getRequest(), 'response' => $e->getResponse()]);
            $src->incrFail();
            throw new \RuntimeException('Request error');
        }

        $html = $response->getBody()->getContents();

        if (empty($html)) {
            throw new \RuntimeException('Fail to get html from site url ' . $site);
        }

        $links_to_icos = [];
        preg_match_all($regex, $html, $links_to_icos);

        if ($indexToReturn === -1) {
            return $links_to_icos;
        }

        $links_to_icos = $links_to_icos[$indexToReturn];
        $links_to_icos = array_unique($links_to_icos);
        return $links_to_icos;
    }

    private function getListofIcosFromHtml($html, $regex, $indexToReturn = 0)
    {
        if (empty($html)) {
            throw new \RuntimeException('Empty html');
        }

        if (empty($html)) {
            throw new \RuntimeException('Empty regex');
        }

        $links_to_icos = [];
        preg_match_all($regex, $html, $links_to_icos);

        if ($indexToReturn === -1) {
            return $links_to_icos;
        }

        $links_to_icos = $links_to_icos[$indexToReturn];
        $links_to_icos = array_unique($links_to_icos);
        return $links_to_icos;
    }

    private function upsert(
        $ico_id,
        $src_id,
        $ics_sdate = null,
        $ics_edate = null,
        $ics_presale_sdate = null,
        $ics_presale_edate = null,
        $ics_hardcap = null,
        $ics_softcap = null,
        $ics_verified = null,
        $ics_rate = null,
        $url_id_whitepaper = null,
        $url_id_telegram = null,
        $url_id_link_to = null,
        $ics_kyc = false,
        $ics_raised_fund = null,
        $ics_raised_fund_usd = null,
        $ics_raised_fund_currency = null,
        $ics_hardcap_currency = null,
        $ics_softcap_currency = null,
        $tat_id = null
    )
    {
        if (empty($ico_id)) {
            throw new \InvalidArgumentException('Empty ico_id');
        }
        $ico_id = intval($ico_id);
        if (empty($ico_id)) {
            throw new \InvalidArgumentException('Invalid ico_id');
        }

        if (empty($src_id)) {
            throw new \InvalidArgumentException('Empty src_id');
        }
        $src_id = intval($src_id);
        if (empty($src_id)) {
            throw new \InvalidArgumentException('Invalid src_id');
        }

        $data = ['ico_id' => $ico_id, 'src_id' => $src_id];

        $data['ics_last_update'] = 'now()';
        $data['ics_sdate'] = $ics_sdate;
        $data['ics_edate'] = $ics_edate;
        $data['ics_presale_sdate'] = $ics_presale_sdate;
        $data['ics_presale_edate'] = $ics_presale_edate;
        $data['ics_hardcap'] = intval($ics_hardcap);
        $data['ics_softcap'] = intval($ics_softcap);
        $data['ics_verified'] = $ics_verified === null ? false : $ics_verified;
        $data['ics_rate'] = empty($ics_rate) ? null : strval($ics_rate);
        $data['url_id_telegram'] = $url_id_telegram;
        $data['url_id_whitepaper'] = $url_id_whitepaper;
        $data['url_id_link_to'] = $url_id_link_to;
        $data['tat_id'] = $tat_id;
        $data['ics_kyc'] = $ics_kyc;
        $data['ics_raised_fund'] = intval($ics_raised_fund);
        $data['ics_raised_fund_usd'] = $ics_raised_fund_usd;
        $data['ics_raised_fund_currency'] = $ics_raised_fund_currency;
        $data['ics_hardcap_currency'] = $ics_hardcap_currency;
        $data['ics_softcap_currency'] = $ics_softcap_currency;

        $rec = self::findFirst([
                'conditions' => 'ico_id = :ico_id: AND src_id = :src_id:',
                'bind' => ['ico_id' => $data['ico_id'], 'src_id' => $data['src_id']]
            ]
        );

        if (empty($rec->ics_id)) {
            $this->ldebug('', ['data' => $data]);
            $rec = new self;
            $rec->save($data);
            if ($rec->getMessages()) {
                foreach ($rec->getMessages() as $err) {
                    trigger_error($err, E_USER_ERROR);
                }
                throw new \RuntimeException("Failed to insert new icos_sources {$data}");
            }
            $this->ldebug('New icos_sources',
                ['ics_id' => $rec->ics_id, 'ico_id' => $rec->ico_id, 'src_id' => $rec->src_id]);
        } else {
            $diff = [];
            foreach ($data as $field => $value) {
                if ($rec->$field !== $value) {
                    $diff[$field] = $value;
                }
            }
            if (!empty($diff)) {
                $rec->update($diff);
                if (count($diff) > 1) {
                    $this->ldebug('Updating existing icos_sources',
                        ['ics_id' => $rec->ics_id, 'ico_id' => $rec->ico_id, 'src_id' => $rec->src_id] + $diff);
                }
            }
        }

        return $rec->ics_id;
    }

    //not finished
    public function convertMoney($data)
    {
        if (empty($data)) {
            return null;
        }

        $data = trim($data);

        $matches = [];

        preg_match_all('/[\d,.\s]+/', $data, $matches);
        $matches = reset($matches);
        $matches = reset($matches);

        $lastdot = strrpos($matches, '.');
        $lastcome = strrpos($matches, ',');

        if ($lastcome && $lastdot) {
            if ($lastdot > $lastcome) {
                $matches = substr($matches, 0, $lastdot);
            } else {
                $matches = substr($matches, 0, $lastcome);
            }
        }
        $num = preg_replace('/[\.,\s]/', '', $matches);

        if (empty($num)) {
            return null;
        }
        $other = preg_replace('/[\.,\d]/', '', $data);

        $cur = null;
        $sign = null;
        $to_usd = null;
        $nother = 0;
        if (!empty($other)) {
            $parts = explode(' ', $other);
            foreach ($parts as $part) {
                if (empty($part)) {
                    continue;
                }
                if ($part === 'M' || $part === 'million') {
                    $num .= '000000';
                } elseif ($part === '$') {
                    $sign = "USD";
                } elseif ($part === '€') {
                    $sign = 'EUR';
                } else {
                    $nother++;
                    $cur = $part;
                }
            }
        }

        if (empty($cur) && !empty($sign)) {
            $cur = $sign;
        }

        if ($nother > 1) {
            $cur = null;
        }

        if ($cur === null) {
            return [intval($num), $cur, $to_usd];
        }
        else {
            if ($cur === 'USD') {
                $to_usd = intval($num);
            }
            else {

            }
        }

        return [intval($num), $cur, $to_usd];
    }
}
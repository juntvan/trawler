<?php

namespace App\Controllers;

use App\Models\Telegram\Dialogs;
use App\Models\Telegram\Messages;

class DialogsController extends ControllerBase
{
    public function indexAction()
    {
        $this->assets->addJs('js/vue.js');

        $dialogs = Dialogs::find();

        $this->view->params = $this->view->getActionName();

        $this->view->dialogs = json_encode($dialogs->toArray(), JSON_NUMERIC_CHECK);
    }

    public function messagesAction($dlg_id)
    {
        $this->assets->addJs('js/vue.js');

        if (!is_numeric($dlg_id)) {
            $this->response->redirect('404.html');
        }

        $messages = Messages::find([
            'conditions' => 'dlg_id = :dlg_id:',
            'bind' => ['dlg_id' => $dlg_id]
        ]);

        if (!empty($messages[0]->dlg->dlg_title)) {

            $this->view->messages = json_encode($messages->toArray(), JSON_NUMERIC_CHECK);
            $this->view->dlg_title = $messages[0]->dlg->dlg_title;
        }
        else {
            $this->view->messages = json_encode(['msg_title' => 'No msg for dialog'], JSON_NUMERIC_CHECK);
        }
    }
}
<?php

namespace App\Controllers;

use App\Models\Coins\Icos;
use App\Models\Coins\Icos\Transactions;

class IcosController extends ControllerBase
{
    public function indexAction()
    {
        $icos_transaction_model = new Transactions();

        $icos = \App\Models\Coins\Icos::find([
            'conditions' => 'ico_title <> ?0',
            'bind' => ['test']
        ]);

        foreach ($icos as $ico) {

            $root = $icos_transaction_model->findFirst([
               'conditions' => 'ico_id = :ico_id:',
                'bind' => ['ico_id' => $ico->ico_id],
                'order' => 'ict_timestamp ASC'
            ]);

            $tokens = $icos_transaction_model->query()
                ->columns('*')
                ->where('ico_id = ?0')
                ->bind([$ico->ico_id])
                ->groupBy('ict_id')
                ->orderBy('ict_timestamp DESC, adr_from')
                ->execute();


            $res = [];


            foreach ($tokens as $token) {
                $res[$token->adrfrom->adr_title][] = $token->adrto->adr_title;
            }
            print_r('1');
        }



//        echo '<table>';
//       foreach ($tokens as $token) {
//
//           echo '<tr>';
//           echo '<td>' . $token->adrfrom->adr_title . '</td>';
//           echo '<td> -> </td>';
//           echo '<td>' . $token->adrto->adr_title . '</td>';
//           echo '</tr>';
//       }
//        echo '</table>';
        return $this->view;
    }
}
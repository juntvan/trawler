<?php

namespace App\Models\Telegram\Users;

use App\Models\ModelBase;
use attics\Lib\Llog\Logger;

class History extends ModelBase
{
    use Logger;

    public function initialize()
    {
        $this->setSchema('telegram');
        $this->setSource('users_history');
    }

    public static function upsert($user) {
        $data = [];

        if (empty($user->usr_id))
            throw new \InvalidArgumentException('Empty user id');
        $data['usr_id'] = intval($user->usr_id);

        if (!empty($user->usr_name)) {
            $data['uhs_name'] = $user->usr_name;
        }

        if (!empty($user->first_name)) {
            $data['uhs_first_name'] = $user->first_name;
        }

        if (!empty($user->last_name)) {
            $data['uhs_last_name'] = $user->last_name;
        }

        if (!empty($user->verified)) {
            $data['uhs_verified'] = 1;
        }

        if (!empty($user->phone)) {
            $data['uhs_phone'] = $user->phone;
        }

        if (!empty($user->restricted)) {
            $data['uhs_restricted'] = 1;
        }

        if (!empty($user->access_hash)) {
            $data['uhs_access_hash'] = intval($user->access_hash);
        }
    }
}
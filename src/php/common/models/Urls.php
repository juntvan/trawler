<?php

namespace App\Models;

use App\Lib\Tools;
use App\Models\Coins\Addresses;
use attics\Lib\Llog\Logger;
use GuzzleHttp\Client;
use GuzzleHttp\TransferStats;
use SparQL\Exception;
use Symfony\Component\Console\Exception\RuntimeException;

class Urls extends \App\Models\Base
{
    use Logger;
    private $addresses_handler;
    private $message_addresses_handler;


    public function onConstruct()
    {
        $this->belongsTo('hst_id','App\Models\Hosts','hst_id',['alias'=>'hst']);
        $this->hasManyToMany('url_id', 'App\Models\Telegram\Messages\Urls', 'url_id', 'msg_id', 'App\Models\Telegram\Messages', 'msg_id', ['alias'=>'msgurl']);
    }

    public static function isValid($url)
	{
		if (empty($url)) {
			return false;
		}

		$hostname = parse_url($url, PHP_URL_HOST);

		$ip = gethostbyname($hostname);
		if ($ip === $hostname)
		{
			return false;
		}
		return true;
	}

	public function process($url, $preValidation = true)
    {
        if (empty($url)) {
            throw new \InvalidArgumentException('Empty url');
        }

        if (strpos($url, 'bit.ly') !== false) {
            $guzzle = new Client();
            try {
                $response = $guzzle->request('GET', $url, [
                    'on_stats' => function (TransferStats $stats) use (&$new_url){
                        $new_url = $stats->getEffectiveUri();
                        $new_url = $new_url->getScheme() . '://' . $new_url->getHost() . $new_url->getPath();
                    }
                ]);
            } catch (\GuzzleHttp\Exception\RequestException $e) {
                $this->logger->error('request error',
                    ['error' => $e->getRequest(), 'response' => $e->getResponse()]);
            }
            if (!empty($new_url)) {
                $url = $new_url;
            }
        }

        $url = trim($url);
        $url = trim($url,'.');

        $url_with_scheme = $this->addScheme($url);

        if ($preValidation) {
            if (!(self::isValid($url_with_scheme))) {
                throw new \InvalidArgumentException("Invalid url " . $url);
            }
        }

        $url_hash = $this->hash($url_with_scheme);
        if (empty($url_hash)) {
            throw new RuntimeException("Invalid hash for " . $url);
        }

        $host = new Hosts();
        try {
            $hst_id = $host->upsert($url_with_scheme);
        } catch (\Exception $e) {
            throw new RuntimeException("Invalid hst_id " . $url);
        }

        $data = [
            'url_title' => $url,
            'hst_id' => $hst_id,
            'url_hash' => $url_hash
        ];

        $rec = self::findFirst([
            'conditions' => 'url_hash = :url_hash:',
            'bind' => ['url_hash' => $url_hash]
        ]);

        if (empty($rec->url_id)) {
            $rec = new self();
            $rec->save($data);

            if ($rec->getMessages()) {
                foreach ($rec->getMessages() as $message) {
                    trigger_error($message);
                }
                throw new \RuntimeException("Failed to insert url");
            }
            $this->ldebug("Added new url", ['url' => $rec->url_title]);
        }

        return $rec->url_id;
    }

    /**
     * @param string $message
     * @param int $msg_id
     * @return null
     */
    public function handle_telega_msg($message, $msg_id)
    {
    	$urls = [];

		$urls = Tools::getUrlFromMessage($message);
		if (empty($urls))
		{
			throw new \InvalidArgumentException("Url not found for msg_id {$msg_id}");
		}

		foreach ($urls as $url)
		{
			if (empty($url))
			{
				$this->ldebug('Empty url');
				continue;
			}

			$url = trim($url);
			$url = trim($url,'.');

			$url_with_scheme = $this->addScheme($url);

			if (!(self::isValid($url_with_scheme)))
			{
				$this->ldebug("Invalid url ", [$url]);
				continue;
			}

			$url_hash = $this->hash($url_with_scheme);
			if (empty($url_hash))
			{
				$this->ldebug("Invalid hash for ", [$url]);
				continue;
			}

			$host = new Hosts();
			try {
                $hst_id = $host->upsert($url_with_scheme);
            } catch (\Exception $e)
            {
                $this->ldebug("Invalid hst_id ", [$url]);
                continue;
            }

			$data = [
				'url_title' => $url,
				'hst_id' => $hst_id,
				'url_hash' => $url_hash
			];

            $rec = self::findFirst([
                'conditions' => 'url_hash = :url_hash:',
                'bind' => ['url_hash' => $url_hash]
            ]);

			if (empty($rec->url_id))
			{
				$rec = new self();
				$rec->save($data);
			}
			if ($rec->getMessages())
			{
				foreach ($rec->getMessages() as $message)
				{
					trigger_error($message);
				}
				throw new \RuntimeException("Failed to insert url");
			}

			$this->ldebug("Added new url {$rec->url_title}");

			try {
			    //fill message_urls table
                \App\Models\Telegram\Messages\Urls::process($msg_id, $rec->url_id);
            } catch (\Exception $e) {
                //$this->ldebug("Fail to add messages_url (url_id = {$rec->url_id})");
                throw new \RuntimeException('Faild to insert to messages_urls');
            }

            //get eth from url
            $addrs_from_msg = Tools::getEthFromUrl($rec->url_title);
			if (empty($addrs_from_msg)) {
			    continue;
            }

            $this->addresses_handler = new Addresses();
            $this->message_addresses_handler = new \App\Models\Telegram\Urls\Addresses();

            foreach ($addrs_from_msg as $adr) {
                //fill eth address
                $adr_id = $this->addresses_handler->process($adr);

                //fill urls_addresses
                $this->message_addresses_handler->upsert($rec->url_id, $adr_id);

            }


		}

        return null;
    }

    public function hstidByUrlid($url_id) {
        if (empty($url_id)) {
            throw new \InvalidArgumentException('Empty url_id');
        }
        $url_id = intval($url_id);
        if (empty($url_id)) {
            throw new \InvalidArgumentException('Invalid url_id');
        }

        $url = self::findFirst([
            'conditions' => 'url_id = :url_id:',
            'bind' => ['url_id' => $url_id]
        ]);

        if (!empty($url->hst_id)) {
            return $url->hst_id;
        } else {
            return null;
        }
    }

    private function hash($url)
	{
		$host = null;
		$path = null;
		$query = null;

		if (empty($url)) {
            throw new \InvalidArgumentException('Empty url');
        }

    	$parts = parse_url($url);
    	if (empty($parts['host'])) {
    		return null;
		}
		$host = $parts['host'];
    	$host = preg_replace("/^www\./", '', $host);
    	$host = strtolower($host);
    	if (empty($host)) {
			throw new \InvalidArgumentException('Invalid host in url');
		}
		if (!empty($parts['path'])) {
			$path = $parts['path'];
		}
		if (!empty($parts['query'])) {
			$query = $parts['query'];
		}

		return md5($host.$path.$query);
	}

	private function addScheme($url)
	{
		if (preg_match("/^https?:\/\//", $url)) {
			return $url;
		} else {
			return 'http://' . $url;
		}
	}

}
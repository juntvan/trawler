<?php

namespace App\Models\Coins;

use coinmarketcap\api\CoinMarketCap;
use GuzzleHttp\Client;

class Coins extends Base
{
    const ETH = 1;

    public function initialize()
    {
        parent::initialize();
        $this->belongsTo('adr_id_contract', 'App\Models\Coins\Addresses', 'adr_id', ['alias'=> 'adr']);
    }

    public static function process($uid = 'ethereum', $name = 'Ethereum', $symbol = 'ETH', $source = 'api.coinmarketcap.com', $adr_id_contract = null)
    {
        if (empty($name)) {
            var_dump($name);
            throw new \InvalidArgumentException('Empty name');
        }

        if (empty($symbol)) {
            throw new \InvalidArgumentException('Empty symbol');
        }

        $symbol = trim($symbol, '() *[]');

        if ($symbol > 10) {
            throw new \InvalidArgumentException('Invalid symbol');
        }

        $uid = strtolower($uid);

        $rec = self::findFirst([
            'conditions' => 'con_code = :con_code:',
            'bind' => ['con_code' => $symbol]
        ]);

        $data = [
            'con_uid' => $uid,
            'con_title' => $name,
            'con_source' => $source,
            'con_code' => $symbol,
            'adr_id_contract' => $adr_id_contract
        ];

        if (empty($rec->con_id)) {
            $rec = new self;
            $rec->save($data);
            if ($rec->getMessages()) {
                print_r($data);
                foreach ($rec->getMessages() as $message) {
                    trigger_error($message);
                }
                throw new \RuntimeException("Fail to insert coin {$uid}");
            }
        } elseif (!empty($adr_id_contract)) {
            $rec->update(['adr_id_contract' => $adr_id_contract]);
            if ($rec->getMessages()) {
                foreach ($rec->getMessages() as $message) {
                    trigger_error($message);
                }
                throw new \RuntimeException("Fail to update adr_id_contract for con_id " . $rec->con_id);
            }
        }

        return $rec->con_id;
    }

    public function getTicketsCoinmarket()
    {
        $this->ldebug('Handling tickets from coinmarketcap');
        try {
            $tikets = \coinmarketcap\api\CoinMarketCap::getTicker(10000);
        } catch (\Exception $e) {
            $this->lerror('Get error', ['e' => $e]);
            exit;
        }

        foreach ($tikets as $tiket) {
            if (empty($tiket['id'])) {
                throw new \InvalidArgumentException('Empty ticket id');
            }
            if (empty($tiket['name'])) {
                throw new \InvalidArgumentException('Empty ticket name');
            }
            if (empty($tiket['symbol'])) {
                throw new \InvalidArgumentException('Empty ticket symbol');
            }
            $this->ldebug('ticket', ['id' => $tiket['id'], 'name' => $tiket['name'], 'symbol' => $tiket['symbol']]);
            Coins::process($tiket['id'], $tiket['name'], $tiket['symbol'], 'api.coinmarketcap.com');
        }
    }

    public function setCoinContract($contract_address)
    {
        if (empty($contract_address)) {
            throw new \InvalidArgumentException('Empty contract_address');
        }

        $adr_model = new Addresses();
        $adr_model->init($this->getLogger());
        $adr_id = $adr_model->process($contract_address);
        $guzzle = new Client([
            'timeout' => 30
        ]);

        $url = "https://api.ethplorer.io/getTokenInfo/{$contract_address}?apiKey=freekey";

        try {
            $response = $guzzle->request('GET', $url);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $this->logger->error('request error',
                ['error' => $e->getRequest(), 'response' => $e->getResponse()]);
        }

        $response = json_decode($response->getBody(), true);

        if (isset($response['error'])) {
            throw new \RuntimeException('Get error ' . $response['error']['code'] . ' message ' . $response['error']['message']);
        }

        if (empty($response['symbol'])) {
            throw new \InvalidArgumentException('Token symbol not found');
        }

        $con = self::findFirst([
            'conditions' => 'con_code = :con_code:',
            'bind' => ['con_code' => $response['symbol']]
        ]);

        if (empty($con->con_id)) {
            throw new \RuntimeException('Coin not found with symbol ' . $response['symbol']);
        }

        if (!empty($con->adr_id_contract)) {
            $this->ldebug('adr_id_contract already exist for', ['con_id' => $con->con_id, 'adr_id_contract' => $con->adr_id_contract]);
            return $con->con_id;
        }

        $con->update(['adr_id_contract' => $adr_id]);
        if ($con->getMessages()) {
            foreach ($con->getMessages() as $message) {
                trigger_error($message);
            }
            throw new \RuntimeException("Fail to update adr_id_contract for con_id " . $con->con_id);
        }
        $this->ldebug('Update Coin', ['con_id' => $con->con_id, 'adr_id_contract' => $con->adr_id_contract]);
        return $con->con_id;
    }

    public static function getCoinRegex()
    {
        $con_model = new self;

        $coins = $con_model->find();

        $str = '/\b(';
        $f = 0;
        foreach ($coins as $coin) {
            if ($f) {
                $str .= '|';
            }
            $str .= $coin->con_code;
            $f = 1;
        }
        $str .= ')\b/';

        return $str;
    }

    public function getCoinId($symbol)
    {
        if (empty($symbol)) {
            throw new \InvalidArgumentException('Empty symbol');
        }
        $con = $this->findFirst([
            'conditions' => 'con_code = :con_code:',
            'bind' => ['con_code' => $symbol]
        ]);

        if (empty($con->con_id)) {
            return null;
        }
        return $con->con_id;
    }

    public static function getEthCoinId()
    {
        return self::ETH;
    }
}
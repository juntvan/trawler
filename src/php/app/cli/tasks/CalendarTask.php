<?php

class CalendarTask extends TaskBase
{
    public function mainAction()
    {
        // Get the API client and construct the service object.
        $client = $this->getClient();
        $service = new Google_Service_Calendar($client);

        // Print the next 10 events on the user's calendar.
        $calendarId = 'primary';
        $optParams = array(
            'maxResults' => 10,
            'orderBy' => 'startTime',
            'singleEvents' => true,
            'timeMin' => date('c'),
        );

        $results = $service->events->listEvents($calendarId, $optParams);

        if (empty($results->getItems())) {
            print "No upcoming events found.\n";
        } else {
            print "Upcoming events:\n";
            foreach ($results->getItems() as $event) {
                $start = $event->start->dateTime;
                if (empty($start)) {
                    $start = $event->start->date;
                }
                printf("%s (%s)\n", $event->getSummary(), $start);
            }
        }
    }

    public function testAction()
    {
        $this->logger = $this->di->get('logger');

        $client = $this->getClient();
        $calendarId = 'rengen12@gmail.com';
        $service = new Google_Service_Calendar($client);
        $postBody = new Google_Service_Calendar_FreeBusyRequest();

        $ico = App\Models\Icos\Icos\Sources::findFirst();

        $postBody->setTimeMin($this->prepareDate($ico->ics_sdate));
        $postBody->setTimeMax($this->prepareDate($ico->ics_edate));
        $postBody->setItems(['id' => $calendarId]);
        $busy = $service->freebusy->query($postBody);
        echo 1;
    }

    public function addIcosSeparatedAction()
    {
        $this->logger = $this->di->get('logger');

        $client = $this->getClient();
        $calendarId = 'kkuanv1c5eu6dnf8o8ttjmpoco@group.calendar.google.com';
        $service = new Google_Service_Calendar($client);

        $month_ago = 2592000;
        $icos = \App\Models\Icos\Icos::find([
            'conditions' => 'ico_last_check > :monthago:',
            'bind' => ['monthago' => date('Y-m-d H:i:s', $month_ago)]
        ]);

        foreach ($icos as $ico) {

            $this->logger->debug('Handling ico', ['ico_id' => $ico->ico_id, 'ico_name' => $ico->ico_name]);

            $description = 'Website: ' . $ico->hst->hst_title . PHP_EOL;
            $psdate = null;
            $pedate = null;
            $sdate = null;
            $edate = null;
            $whitepaper = null;
            $kyc = null;
            $hardcap = null;
            $softcap = null;
            foreach ($ico->ics as $ics) {

                if ($ics->ics_presale_sdate) {
                    $psdate[] = $ics->ics_presale_sdate;
                }
                if ($ics->ics_presale_edate) {
                    $pedate[] = $ics->ics_presale_edate;
                }
                if ($ics->ics_sdate) {
                    $sdate[] = $ics->ics_sdate;
                }
                if ($ics->ics_edate) {
                    $edate[] = $ics->ics_edate;
                }

                if (!empty($ics->whitepaper)) {
                    $whitepaper = $ics->whitepaper->url_title;
                }

                if ($ics->linkto) {
                    $description .= 'From: ' . $ics->linkto->url_title;
                } else {
                    $description .= 'From: ' . $ics->src->hst->hst_title;
                }

                if (!empty($ics->ics_raised_fund)) {
                    $description .= ' Collected: ' . $ics->ics_raised_fund . ' ' . $ics->ics_raised_fund_currency;
                }

                if ($ics->ics_rate) {
                    $description .= ' Rate: ' . $ics->ics_rate;
                }

                if (!empty($ics->ics_hardcap)) {
                    $hardcap[] = $ics->ics_hardcap . ' ' . $ics->ics_hardcap_currency;
                }

                if (!empty($ics->ics_softcap)) {
                    $softcap[] = $ics->ics_softcap . ' ' . $ics->ics_softcap_currency;
                }

                if ($ics->ics_kyc) {
                    $kyc = true;
                }

                $description .= PHP_EOL;
            }

            $psdate = $this->getDateFromArr($psdate);
            $pedate = $this->getDateFromArr($pedate);
            $sdate = $this->getDateFromArr($sdate);
            $edate = $this->getDateFromArr($edate);
            $hardcap = $this->getDateFromArr($hardcap);
            $softcap = $this->getDateFromArr($softcap);

            if (!empty($whitepaper)) {
                $description .= "Whitepaper: " . $whitepaper;
                $description .= PHP_EOL;
            }

            if (!empty($hardcap)) {
                $description .= "Hardcap: " . $hardcap;
                $description .= PHP_EOL;
            }

            if (!empty($softcap)) {
                $description .= "Softcap: " . $softcap;
                $description .= PHP_EOL;
            }

            if ($kyc) {
                $description .= "KYC +";
                $description .= PHP_EOL;
            }


            $description .= 'Last update: ' . \App\Lib\Tools::convert_any_date(time());

            if (!empty($psdate)) {
                $event = new Google_Service_Calendar_Event(array(
                    'summary' => $ico->ico_name . ' (PreIco Start)',
                    'description' => $description,
                    'start' => array(
                        'dateTime' => $this->prepareDate($psdate)
                    ),
                    'end' => array(
                        'dateTime' => $this->prepareDate($psdate)
                    ),
                    'colorId' => '1'
                ));

                if (empty($ico->ico_calendar_id_ps)) {
                    try {
                        $event = $service->events->insert($calendarId, $event);
                    } catch (Google_Service_Exception $e) {
                        $this->logger->error("", ['e' => $e->getMessage()]);
                        continue;
                    }
                    $ico->updCalId($event->id, 'ico_calendar_id_ps');
                    $this->logger->debug("Event created (PreIco Start)",
                        ['ico_id' => $ico->ico_id, 'ico_name' => $ico->ico_name]);
                } else {
                    try {
                        $event = $service->events->patch($calendarId, $ico->ico_calendar_id_ps, $event);
                    } catch (Google_Service_Exception $e) {
                        $this->logger->error("", ['e' => $e->getMessage()]);
                        continue;
                    }
                    $this->logger->debug("Event updated",
                        [
                            'ico_id' => $ico->ico_id,
                            'ico_name' => $ico->ico_name,
                            'ico_calendar_id_ps' => $ico->ico_calendar_id_ps
                        ]);
                }
            }

            if (!empty($pedate) && $pedate !== $psdate) {
                $event = new Google_Service_Calendar_Event(array(
                    'summary' => $ico->ico_name . ' (PreIco End)',
                    'description' => $description,
                    'start' => array(
                        'dateTime' => $this->prepareDate($pedate)
                    ),
                    'end' => array(
                        'dateTime' => $this->prepareDate($pedate)
                    ),
                    'colorId' => '2'
                ));

                if (empty($ico->ico_calendar_id_pe)) {
                    try {
                        $event = $service->events->insert($calendarId, $event);
                    } catch (Google_Service_Exception $e) {
                        $this->logger->error("", ['e' => $e->getMessage()]);
                        continue;
                    }
                    $ico->updCalId($event->id, 'ico_calendar_id_pe');
                    $this->logger->debug("Event created (PreIco End)",
                        ['ico_id' => $ico->ico_id, 'ico_name' => $ico->ico_name]);
                } else {
                    try {
                        $event = $service->events->patch($calendarId, $ico->ico_calendar_id_pe, $event);
                    } catch (Google_Service_Exception $e) {
                        $this->logger->error("", ['e' => $e->getMessage()]);
                        continue;
                    }
                    $this->logger->debug("Event updated",
                        [
                            'ico_id' => $ico->ico_id,
                            'ico_name' => $ico->ico_name,
                            'ico_calendar_id_pe' => $ico->ico_calendar_id_pe
                        ]);
                }
            }

            if (!empty($sdate)) {
                $event = new Google_Service_Calendar_Event(array(
                    'summary' => $ico->ico_name . ' (Ico Start)',
                    'description' => $description,
                    'start' => array(
                        'dateTime' => $this->prepareDate($sdate)
                    ),
                    'end' => array(
                        'dateTime' => $this->prepareDate($sdate)
                    ),
                    'colorId' => '3'
                ));

                if (empty($ico->ico_calendar_id_s)) {
                    try {
                        $event = $service->events->insert($calendarId, $event);
                    } catch (Google_Service_Exception $e) {
                        $this->logger->error("", ['e' => $e->getMessage()]);
                        continue;
                    }
                    $ico->updCalId($event->id, 'ico_calendar_id_s');
                    $this->logger->debug("Event created (Ico Start)",
                        ['ico_id' => $ico->ico_id, 'ico_name' => $ico->ico_name]);
                } else {
                    try {
                        $event = $service->events->patch($calendarId, $ico->ico_calendar_id_s, $event);
                    } catch (Google_Service_Exception $e) {
                        $this->logger->error("", ['e' => $e->getMessage()]);
                        continue;
                    }
                    $this->logger->debug("Event updated",
                        [
                            'ico_id' => $ico->ico_id,
                            'ico_name' => $ico->ico_name,
                            'ico_calendar_id_s' => $ico->ico_calendar_id_s
                        ]);
                }
            }

            if (!empty($edate) && $edate !== $sdate) {
                $event = new Google_Service_Calendar_Event(array(
                    'summary' => $ico->ico_name . ' (Ico End)',
                    'description' => $description,
                    'start' => array(
                        'dateTime' => $this->prepareDate($edate)
                    ),
                    'end' => array(
                        'dateTime' => $this->prepareDate($edate)
                    ),
                    'colorId' => '4'
                ));

                if (empty($ico->ico_calendar_id_e)) {
                    try {
                        $event = $service->events->insert($calendarId, $event);
                    } catch (Google_Service_Exception $e) {
                        $this->logger->error("", ['e' => $e->getMessage()]);
                        continue;
                    }
                    $ico->updCalId($event->id, 'ico_calendar_id_e');
                    $this->logger->debug("Event created (Ico End)",
                        ['ico_id' => $ico->ico_id, 'ico_name' => $ico->ico_name]);
                } else {
                    try {
                        $event = $service->events->patch($calendarId, $ico->ico_calendar_id_e, $event);
                    } catch (Google_Service_Exception $e) {
                        $this->logger->error("", ['e' => $e->getMessage()]);
                        continue;
                    }
                    $this->logger->debug("Event updated",
                        [
                            'ico_id' => $ico->ico_id,
                            'ico_name' => $ico->ico_name,
                            'ico_calendar_id_e' => $ico->ico_calendar_id_e
                        ]);
                }
            }
        }
    }

    public function addIcosAction()
    {
        $this->logger = $this->di->get('logger');

        $client = $this->getClient();
        $calendarId = 'velgcq5snke8ttr39m46l79e1k@group.calendar.google.com';
        $service = new Google_Service_Calendar($client);

        $icos = \App\Models\Icos\Icos::find();

        foreach ($icos as $ico) {

            $this->logger->debug('Handling ico', ['ico_id' => $ico->ico_id, 'ico_name' => $ico->ico_name]);

            $description = 'Website: ' . $ico->hst->hst_title . PHP_EOL;
            $psdate = null;
            $pedate = null;
            $sdate = null;
            $edate = null;
            $whitepaper = null;
            foreach ($ico->ics as $ics) {

                if ($ics->ics_presale_sdate) {
                    $psdate[] = $ics->ics_presale_sdate;
                }
                if ($ics->ics_presale_edate) {
                    $pedate[] = $ics->ics_presale_edate;
                }
                if ($ics->ics_sdate) {
                    $sdate[] = $ics->ics_sdate;
                }
                if ($ics->ics_edate) {
                    $edate[] = $ics->ics_edate;
                }

                if (!empty($ics->whitepaper)) {
                    $whitepaper = $ics->whitepaper->url_title;
                }

                if ($ics->linkto) {
                    $description .= 'From: ' . $ics->linkto->url_title;
                } else {
                    $description .= 'From: ' . $ics->src->hst->hst_title;
                }

                if ($ics->ics_rate) {
                    $description .= ' Rate: ' . $ics->ics_rate;
                }
                $description .= PHP_EOL;
            }

            $psdate = $this->getDateFromArr($psdate);
            $pedate = $this->getDateFromArr($pedate);
            $sdate = $this->getDateFromArr($sdate);
            $edate = $this->getDateFromArr($edate);

            if (!empty($whitepaper)) {
                $description .= "Whitepaper: " . $whitepaper;
            }

            if (!empty($psdate)) {
                $event = new Google_Service_Calendar_Event(array(
                    'summary' => $ico->ico_name . ' (PreIco)',
                    'description' => $description,
                    'start' => array(
                        'dateTime' => $this->prepareDate($psdate)
                    ),
                    'end' => array(
                        'dateTime' => empty($pedate) ? $this->prepareDate($psdate) : $this->prepareDate($pedate)
                    ),
                    'colorId' => '2'
                ));

                if (empty($ico->ico_calendar_id_preico)) {
                    try {
                        $event = $service->events->insert($calendarId, $event);
                    } catch (Google_Service_Exception $e) {
                        $this->logger->error("", ['e' => $e->getMessage()]);
                        continue;
                    }
                    $ico->updCalId($event->id, 'ico_calendar_id_preico');
                    $this->logger->debug("Event created (PreIco)",
                        ['ico_id' => $ico->ico_id, 'ico_name' => $ico->ico_name]);

                } else {
                    try {
                        $event = $service->events->patch($calendarId, $ico->ico_calendar_id_preico, $event);
                    } catch (Google_Service_Exception $e) {
                        $this->logger->error("", ['e' => $e->getMessage()]);
                        continue;
                    }
                    $this->logger->debug("Event updated",
                        [
                            'ico_id' => $ico->ico_id,
                            'ico_name' => $ico->ico_name,
                            'ico_calendar_id_preico' => $ico->ico_calendar_id_preico
                        ]);
                }
            }

            if (!empty($sdate)) {
                $event = new Google_Service_Calendar_Event(array(
                    'summary' => $ico->ico_name . ' (Ico)',
                    'description' => $description,
                    'start' => array(
                        'dateTime' => $this->prepareDate($sdate)
                    ),
                    'end' => array(
                        'dateTime' => empty($edate) ? $this->prepareDate($sdate) : $this->prepareDate($edate)
                    ),
                    'colorId' => '1'
                ));

                if (empty($ico->ico_calendar_id)) {
                    try {
                        $event = $service->events->insert($calendarId, $event);
                    } catch (Google_Service_Exception $e) {
                        $this->logger->error("", ['e' => $e->getMessage()]);
                        continue;
                    }
                    $ico->updCalId($event->id, 'ico_calendar_id');
                    $this->logger->debug("Event created (Ico)",
                        ['ico_id' => $ico->ico_id, 'ico_name' => $ico->ico_name]);
                } else {
                    try {
                        $event = $service->events->patch($calendarId, $ico->ico_calendar_id, $event);
                    } catch (Google_Service_Exception $e) {
                        $this->logger->error("", ['e' => $e->getMessage()]);
                        continue;
                    }
                    $this->logger->debug("Event updated",
                        [
                            'ico_id' => $ico->ico_id,
                            'ico_name' => $ico->ico_name,
                            'ico_calendar_id' => $ico->ico_calendar_id
                        ]);
                }
            }

        }

    }

    public function clearCalendarAction()
    {
        $this->logger = $this->di->get('logger');

        $client = $this->getClient();
        $calendarId = 'kkuanv1c5eu6dnf8o8ttjmpoco@group.calendar.google.com';
        $service = new Google_Service_Calendar($client);

        while (1) {
            $res = $service->events->listEvents($calendarId);
            if (empty($res['items'])) {
                break;
            }
            foreach ($res['items'] as $item) {
                $res = $service->events->delete($calendarId, $item['id']);
                print_r($res);
            }
        }
        echo 1;
    }

    private function prepareDate($timestamp)
    {
        if (empty($timestamp)) {
            throw new \InvalidArgumentException('Empty timestamp');
        }

        $one = 1;
        $timestamp = str_replace(' ', 'T', $timestamp, $one);
        $timestamp .= 'Z';
        return $timestamp;
    }

    private function getDateFromArr($dates)
    {
        if (empty($dates)) {
            return null;
        }

        if (count($dates) < 2) {
            return $dates[0];
        }

        $counting = array_count_values($dates);

        $greater = null;
        $freq = 0;
        foreach ($counting as $key => $value) {
            if (empty($greater)) {
                $greater = $key;
                $freq = $value;
            } else {
                if ($value > $freq) {
                    $freq = $value;
                    $greater = $key;
                }
            }
        }
        return $greater;
    }

    private function getClient()
    {
        $KEY_FILE_LOCATION = __DIR__ . '/client_secret.json';



        $client = new Google_Client();
        $client->setApplicationName('Google Calendar API');
        $client->setScopes(Google_Service_Calendar::CALENDAR);
        $client->setAuthConfig($KEY_FILE_LOCATION);
        $client->setAccessType('offline');

        // Load previously authorized credentials from a file.
        $credentialsPath = __DIR__ . '/' . $this->expandHomeDirectory('credentials.json');
        if (file_exists($credentialsPath)) {
            $accessToken = json_decode(file_get_contents($credentialsPath), true);
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

            // Store the credentials to disk.
            if (!file_exists(dirname($credentialsPath))) {
                mkdir(dirname($credentialsPath), 0700, true);
            }
            file_put_contents($credentialsPath, json_encode($accessToken));
            printf("Credentials saved to %s\n", $credentialsPath);
        }
        $client->setAccessToken($accessToken);

        // Refresh the token if it's expired.
        if ($client->isAccessTokenExpired()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }

    /**
     * Expands the home directory alias '~' to the full path.
     * @param string $path the path to expand.
     * @return string the expanded path.
     */
    private function expandHomeDirectory($path)
    {
        $homeDirectory = getenv('HOME');
        if (empty($homeDirectory)) {
            $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
        }
        return str_replace('~', realpath($homeDirectory), $path);
    }
}
<?php

namespace App\Lib;

class Tools
{
    /**
     * @param integer $timestamp
     * @return string
     */
    public static function date_convert($timestamp)
    {
        $timestamp = intval($timestamp);
        if(empty($timestamp)) {
            return null;
        }

        $ten_year_ago = time() - 311040000;
        $one_day_after = time() + 86400;

        if($timestamp<$ten_year_ago){
            return null;
        }

        if($timestamp > $one_day_after){
            return null;
        }

        return date('Y-m-d H:i:s',$timestamp);
    }

    public static function convert_any_date($timestamp)
    {
        if ($timestamp === null) {
            return null;
        }
        $timestamp = intval($timestamp);
        if(empty($timestamp) ||  $timestamp > 2147483647 || $timestamp < 1) {
            return null;
        }

        return date('Y-m-d H:i:s', $timestamp);
    }

    public static function readMore($string, $limit = 300, $more_text = '...')
    {
        if (empty($string)) {
            return false;
        }

        if (strlen($string) > $limit) {
            return substr($string, 0, $limit) . '...';
        }

        return $string;
    }

    public static function sdir($str, $rootPath = null, $depth = 2)
    {
        if (!empty($rootPath)) {
            $rootPath = rtrim($rootPath, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
        }

        $pattern = '/[^a-z0-9]/';

        $s = preg_replace($pattern, '', strtolower($str));
        $path = '';

        if (strlen($s) >= $depth) {
            for ($i = 1; $i <= $depth; $i++) {
                $path .= substr($s, 0, $i) . '/';
            }
        }

        if (!empty($rootPath) && !is_dir($rootPath . $path)) {
            mkdir($rootPath . $path, 0775, true);
        }

        return $rootPath . $path;
    }

    public static function getUrlFromMessage($str)
    {
        $reg = "/(https?:\/\/)?(www\.)?([\w-_]+)\.(?!https?|!www)([a-z]{2,6})[\w\.\@\?\^\=\%\&\:\/\~\+\#\-]*\/?/";//add ! before www
        $matches = [];

        preg_match_all($reg, $str,$matches);
        return reset($matches);
    }

	public static function getEthFromUrl($str)
    {
		$reg = "/0x[a-fA-F0-9]{40}/";
		$matches = [];

		preg_match_all($reg, $str,$matches);
		return reset($matches);
	}

    public static function getEthFromEtherscan($str)
    {
        $reg = '/opcode-tool\?a=(0x[a-fA-F0-9]{40})/';
        $matches = [];

        preg_match_all($reg, $str,$matches);
        return $matches[1];
    }

    public static function getEthFromEtherscanTokenTransaction($str)
    {
        $reg = "/class='address-tag'>(0x[a-fA-F0-9]{40})<\/a><br>/";
        $matches = [];

        preg_match_all($reg, $str,$matches);
        return $matches[1];
    }
}
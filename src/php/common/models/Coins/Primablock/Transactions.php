<?php

namespace App\Models\Coins\Primablock;

use App\Lib\Tools;
use App\Models\Coins\Addresses;

class Transactions extends \App\Models\Coins\Base
{
    private $adr_handler;

    public function initialize()
    {
        parent::initialize();
        $this->setSource('primablock_transactions');
        $this->belongsTo('prm_id','App\Models\Coins\Primablock','prm_id',['alias'=>'prm']);
        $this->belongsTo('adr_id','App\Models\Coins\Addresses','adr_id',['alias'=>'adr']);
        $this->belongsTo('adr_from','App\Models\Coins\Addresses','adr_id',['alias'=>'adrfrom']);
        $this->belongsTo('adr_to','App\Models\Coins\Addresses','adr_id',['alias'=>'adrto']);
        $this->belongsTo('con_id','App\Models\Coins\Coins','con_id',['alias'=>'con']);
    }

    public function process($transaction, $adr_id, $prm_id, $con_id = null, $is_token = false)
    {
        $data = [];

        if (empty($transaction)) {
            throw new \InvalidArgumentException('Empty transaction arg');
        }

        if (empty($adr_id)) {
            throw new \InvalidArgumentException('Empty adr_id');
        }
        $data['adr_id'] = $adr_id;

        if (empty($prm_id)) {
            throw new \InvalidArgumentException('Empty prm_id');
        }
        $data['prm_id'] = $prm_id;

        $data['con_id'] = $con_id;

        if (empty($transaction['hash'])) {
            throw new \InvalidArgumentException("Empty hash for prm_id {$prm_id}");
        }
        $data['pbt_hash'] = trim($transaction['hash']);

        if (empty($transaction['timeStamp'])) {
            throw new \InvalidArgumentException("Empty timeStamp for prm_id {$prm_id}");
        }
        $data['pbt_timestamp'] = intval($transaction['timeStamp']);
        if (empty($data['pbt_timestamp'])) {
            throw new \InvalidArgumentException("Invalid timeStamp for prm_id {$prm_id}");
        }
        $data['pbt_timestamp'] = Tools::date_convert($data['pbt_timestamp']);
        if (empty($data['pbt_timestamp'])) {
            throw new \InvalidArgumentException("Time convertion error for prm_id {$prm_id}");
        }

        if (empty($transaction['blockNumber'])) {
            throw new \InvalidArgumentException("Empty blockNumber for prm_id {$prm_id}");
        }
        $data['pbt_block'] = intval($transaction['blockNumber']);
        if (empty($data['pbt_block'])) {
            throw new \InvalidArgumentException("Invalid blockNumber for prm_id {$prm_id}");
        }

        if (isset($transaction['value'])) {

            $right_op = '1000000000000000000'; //for eth
            if (!empty($transaction['tokenDecimal'])) {
                $right_op = bcpow('10', $transaction['tokenDecimal']);
            }

            $data['pbt_value'] = bcdiv($transaction['value'], $right_op, 5);
        }

        $data['pbt_success'] = 1;
        if (isset($transaction['isError'])) {
            if (intval($transaction['isError']) > 0) {
                $data['pbt_success'] = 0;
            }
        }

        if (isset($transaction['input'])) {
            //10 chars from input
            $data['pbt_method'] = substr($transaction['input'], 0, 10);
        }

        $this->adr_handler = new Addresses();
        $this->adr_handler->setLogger($this->getLogger());

        if (!empty($transaction['to'])) {
            $data['adr_to'] = $this->adr_handler->process($transaction['to']);
        }

        if (!empty($transaction['from'])) {
            $data['adr_from'] = $this->adr_handler->process($transaction['from']);
        }

        if ($is_token) {
            $data['pbt_is_token'] = true;
        }

        $rec = self::findFirst([
                'conditions' => 'pbt_hash = :pbt_hash:',
                'bind' => ['pbt_hash' => $data['pbt_hash']]
            ]
        );

        if (empty($rec->adr_id)) {
            $rec = new self();
            $rec->save($data);
            if ($rec->getMessages()) {
                foreach ($rec->getMessages() as $err) {
                    trigger_error($err, E_USER_ERROR);
                }
                throw new \RuntimeException('Failed to insert new transaction', $data);
            }
            $this->ldebug('New transaction', ['id' => $rec->pbt_id]);
        }
        else {
            $this->ldebug('Transaction already exist', ['id' => $rec->pbt_id]);
        }

        return $rec->pbt_id;
    }
}
# My project's README


install migration: 
   cd /src/php -> ./vendor/bin/migrate install -v --path=./migrations

up migration
   cd /src/php -> ./vendor/bin/migrate up

down migration
   cd /src/php -> ./vendor/bin/migrate down
   

create new migration:
   cd /src/php -> ./vendor/bin/migrate create migrations


recover base struct
    vendor/bin/migrate reset -vvv
    
certain version
    vendor/bin/migrate down --up-to=1 -vvv
    vendor/bin/migrate up --up-to=1 -vvv
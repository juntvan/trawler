<?php

namespace App\Models\Telegram\Urls;

use App\Models\Telegram\Base;

class Addresses extends Base
{
    public function initialize()
    {
        parent::initialize();
        $this->setSource('urls_addresses');
        $this->belongsTo('url_id', 'App\Models\Urls', 'url_id', ['alias' => 'url']);
        $this->belongsTo('adr_id', 'App\Models\Coins\Addresses', 'adr_id', ['alias' => 'adr']);
    }

    public function upsert($url_id, $adr_id)
    {
        if (empty($url_id)) {
            throw new \InvalidArgumentException('Empty url_id');
        }

        if (empty($adr_id)) {
            throw new \InvalidArgumentException('Empty adr_id');
        }

        $data = [
            'url_id' => $url_id,
            'adr_id' => $adr_id
        ];

        $rec = self::findFirst([
            'conditions' => 'url_id = :url_id: AND adr_id = :adr_id:',
            'bind' => ['url_id' => $url_id, 'adr_id' => $adr_id]
        ]);

        if (empty($rec->adr_id)) {
            $rec = new self;
            $rec->save($data);
            if ($rec->getMessages()) {
                foreach ($rec->getMessages() as $message) {
                    trigger_error($message);
                }
                throw new \RuntimeException('Fail to insert into urls_addresses');
            }
            $this->ldebug('Added new row in urls_addresses', ['adr_id' => $rec->adr_id]);
            echo "\n\n";
        }
        return true;
    }
}
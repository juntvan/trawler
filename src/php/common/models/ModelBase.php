<?php
namespace App\Models;

use Phalcon\Di;

class ModelBase extends \Phalcon\Mvc\Model
{
    public function defaultValue()
    {
        return new \Phalcon\Db\RawValue('default');
    }
}
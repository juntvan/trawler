<?php

class StatTask extends TaskBase
{
    public function updateCoinsHypeAction()
    {
        $this->logger = $this->di->get('logger');

        $coins_hype_model = new \App\Models\Stat\Coins\Hype();
        $coins_hype_model->init($this->logger);

        $coins_hype_model->upsert();
    }

    public function updateDialogsAction()
    {
        $this->logger = $this->di->get('logger');

        $dialog_model = new \App\Models\Stat\Dialogs();
        $dialog_model->init($this->logger);

        $dialog_model->upsert();
    }

    public function updateUsersAction()
    {
        $this->logger = $this->di->get('logger');

        $users_model = new \App\Models\Stat\Users();
        $users_model->init($this->logger);

        $users_model->upsert();
    }
}

<?php

namespace App\Models\Coins\Tokentransfers;

use App\Models\Coins\Icos\Tokentransfers;

class Transactions extends \App\Models\Coins\Base
{
    public function initialize()
    {
        parent::initialize();
        $this->setSource('tokentransfers_transactions');
        $this->belongsTo('tx_id','App\Models\Coins\Transactions','tx_id',['alias'=>'tx']);
        $this->belongsTo('ict_id','App\Models\Coins\Icos\Tokentransfers\Transactions','ict_id',['alias'=>'ict']);
    }

    public function process()
    {
        $this->ldebug('Start handling tokentransfers_transactions');

        $sql = 'SELECT tk.ict_id, t.tx_id, tk_adr_from.adr_title as token_from, tk_adr_to.adr_title as token_to, tk.ict_value as token_value, con.con_code as token,
                  t.tx_value as trans_value, tk.ict_hash as token_hash,
                  t_adr_from.adr_title as trans_from, t_adr_to.adr_title as trans_to, t.tx_hash as trans_hash
                FROM coins.icos_tokentransfers tk
                  LEFT JOIN coins.icos ico ON tk.ico_id = ico.ico_id
                  LEFT JOIN coins.coins con ON con.con_id = tk.con_id
                  LEFT JOIN coins.transactions t ON t.adr_from = tk.adr_to
                  JOIN coins.addresses tk_adr_from ON tk.adr_from = tk_adr_from.adr_id
                  JOIN coins.addresses tk_adr_to ON tk.adr_to = tk_adr_to.adr_id
                  JOIN coins.addresses t_adr_from ON t.adr_from = t_adr_from.adr_id
                  JOIN coins.addresses t_adr_to ON t.adr_to = t_adr_to.adr_id
                  LEFT JOIN coins.tokentransfers_transactions ti ON tk.ict_id = ti.ict_id AND t.tx_id = ti.tx_id
                WHERE ico_title <> \'test\' AND ico.ico_id = tk.ico_id
                      AND t.adr_to = tk.adr_from
                      AND t.tx_value > 0
                      AND tk.adr_to NOT IN (SELECT adr_id FROM coins.transaction_ignore)
                      AND tk.adr_from NOT IN (SELECT adr_id FROM coins.transaction_ignore)
                      AND t.adr_to NOT IN (SELECT adr_id FROM coins.transaction_ignore)
                      AND t.adr_from NOT IN (SELECT adr_id FROM coins.transaction_ignore)
                      AND ti.tx_id ISNULL AND ti.ict_id ISNULL';


        $tokentransfer_handler = new Tokentransfers();

        $tokens = new \Phalcon\Mvc\Model\Resultset\Simple(
            null,
            $tokentransfer_handler,
            $tokentransfer_handler->getReadConnection()->query($sql));


        foreach ($tokens as $token) {
            $data = [];
            if (empty($token->ict_id)) {
                throw new \InvalidArgumentException('Empty ict_id');
            }
            $data['ict_id'] = intval($token->ict_id);
            if (empty($data['ict_id'])) {
                throw new \InvalidArgumentException('Invalid ict_id');
            }

            if (empty($token->tx_id)) {
                throw new \InvalidArgumentException('Empty tx_id');
            }
            $data['tx_id'] = intval($token->tx_id);
            if (empty($token->tx_id)) {
                throw new \InvalidArgumentException('Invalid tx_id');
            }

            $rec = self::findFirst([
                'conditions' => 'ict_id = :ict_id: AND tx_id = :tx_id:',
                'bind' => ['ict_id' => $data['ict_id'], 'tx_id' => $data['tx_id']]
            ]);

            if (empty($rec->ict_id)) {
                $rec = new self();
                $rec->save($data);
                if ($rec->getMessages()) {
                    foreach ($rec->getMessages() as $message) {
                        trigger_error($message);
                    }
                    throw new \RuntimeException("Fail to insert tokentransfer_transaction" . 'ict_id ' . $rec->ict_id . ' tx_id ' . $rec->tx_id);
                }
                $this->ldebug('Added new tokentransfer_transaction', ['ict_id' => $rec->ict_id, 'tx_id' => $rec->tx_id]);
            }
        }
    }
}
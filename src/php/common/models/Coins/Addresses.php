<?php

namespace App\Models\Coins;

class Addresses extends Base
{
    public function initialize()
    {
        parent::initialize();
        $this->belongsTo('con_id','App\Models\Coins\Coins','con_id', ['alias'=>'con']);
        $this->hasManyToMany('adr_id', 'App\Models\Telegram\Urls\Addresses', 'adr_id', 'url_id', 'App\Models\Urls', 'url_id', ['alias'=>'urladr']);
        $this->hasManyToMany('adr_id', 'App\Models\Telegram\Messages\Addresses', 'adr_id', 'msg_id', 'App\Models\Telegram\Messages', 'msg_id', ['alias'=>'msgadr']);
    }

    public function process($adr_title, $currency = 'ethereum')
    {
        static $tickets;

        if (empty($adr_title)) {
            throw new \InvalidArgumentException('Empty adt_title');
        }

        $adr_title = trim($adr_title);

        $data = [
            'adr_title' => $adr_title
        ];

        if (!empty($tickets[$currency])) {
            $ticket = $tickets[$currency];
        }
        else {
            $ticket = \coinmarketcap\api\CoinMarketCap::getCurrencyTicker($currency);
            if (empty($ticket[0])) {
                throw new \InvalidArgumentException('Fail to get ticket');
            }
            $tickets[$currency] = $ticket;
        }

        $con_id = Coins::process($ticket[0]['id'], $ticket[0]['name'], $ticket[0]['symbol'], 'api.coinmarketcap.com');

        $data['con_id'] = $con_id;

        $rec = self::findFirst([
            'conditions' => 'adr_title = :adr_title:',
            'bind' => ['adr_title' => $adr_title]
        ]);

        if (empty($rec->adr_id)) {
            $rec = new self();
            $rec->save($data);
            if ($rec->getMessages()) {
                foreach ($rec->getMessages() as $message) {
                    trigger_error($message);
                }
                throw new \RuntimeException("Fail to insert adr_title {$adr_title}");
            }
            $this->ldebug('Added new address', ['adr_id' => $rec->adr_id]);
        }

        return $rec->adr_id;
    }
}
CREATE SCHEMA coins;

CREATE SCHEMA telegram;


CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;




COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;





CREATE TABLE coins.addresses (
    adr_id integer NOT NULL,
    con_id integer,
    adr_title character varying(64) NOT NULL,
    adr_created timestamp without time zone DEFAULT now() NOT NULL
);






CREATE SEQUENCE coins.addresses_adr_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;






ALTER SEQUENCE coins.addresses_adr_id_seq OWNED BY coins.addresses.adr_id;






CREATE TABLE coins.coins (
    con_id integer NOT NULL,
    con_uid character varying(24) NOT NULL,
    con_title character varying(24) NOT NULL,
    con_code character varying(5) NOT NULL,
    con_source character varying(21),
    con_created timestamp without time zone DEFAULT now() NOT NULL
);






CREATE TABLE coins.icos (
    ico_id integer NOT NULL,
    ico_title character varying(64),
    ico_created timestamp without time zone DEFAULT now() NOT NULL
);






CREATE TABLE coins.primablock (
    prm_id integer NOT NULL,
    ico_id integer,
    prm_title character varying(128),
    prm_fee numeric(4,2),
    prm_capacity numeric(6,2),
    prm_collected numeric(6,2),
    prm_contribution_min numeric(6,2),
    prm_contribution_max numeric(6,2),
    prm_auto boolean,
    prm_created timestamp without time zone DEFAULT now()
);






COMMENT ON COLUMN coins.primablock.prm_capacity IS 'Maximum investment value';






CREATE TABLE coins.primablock_transactions (
    pbt_id integer NOT NULL,
    prm_id integer NOT NULL,
    adr_id integer NOT NULL,
    pbt_hash character(64) NOT NULL,
    pbt_value numeric(12,5),
    pbt_block integer,
    pbt_timestamp timestamp without time zone NOT NULL,
    pbt_success boolean DEFAULT false NOT NULL,
    pbt_method character varying(12)
);






COMMENT ON COLUMN coins.primablock_transactions.prm_id IS 'Primablock smartcontract address';






COMMENT ON COLUMN coins.primablock_transactions.pbt_hash IS 'tx hash';






COMMENT ON COLUMN coins.primablock_transactions.pbt_value IS 'Amount of investment in ETH';






COMMENT ON COLUMN coins.primablock_transactions.pbt_timestamp IS 'Tx timestamp';






COMMENT ON COLUMN coins.primablock_transactions.pbt_method IS 'Smart contract method';






CREATE SEQUENCE coins.primablock_transactions_pbt_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;






ALTER SEQUENCE coins.primablock_transactions_pbt_id_seq OWNED BY coins.primablock_transactions.pbt_id;






CREATE TABLE public.hosts (
    hst_id integer NOT NULL,
    hst_title character varying(64) NOT NULL,
    hst_created timestamp without time zone DEFAULT now() NOT NULL
);






CREATE SEQUENCE public.hosts_hst_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;






ALTER SEQUENCE public.hosts_hst_id_seq OWNED BY public.hosts.hst_id;






CREATE TABLE public.urls (
    url_id integer DEFAULT nextval(('public.urls_url_id_seq' :: text) :: regclass) NOT NULL,
    url_hash character(32) NOT NULL,
    url_title character varying(256) NOT NULL,
    url_created timestamp without time zone DEFAULT now() NOT NULL,
    hst_id integer NOT NULL
);






COMMENT ON COLUMN public.urls.url_hash IS 'md5 hash from lower host + params without scheme and www.';






CREATE SEQUENCE public.urls_url_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;






CREATE SEQUENCE telegram.dialog_types_dtp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 32767
    CACHE 1;






CREATE TABLE telegram.dialogs (
    dlg_id bigint NOT NULL,
    tps_id smallint NOT NULL,
    nic_id bigint,
    dlg_title character varying(512),
    dlg_created timestamp without time zone DEFAULT now() NOT NULL,
    dlg_democracy boolean,
    dlg_restricted boolean,
    dlg_access_hash bigint,
    dlg_photo boolean,
    dlg_about text,
    dlg_bot_api_id bigint NOT NULL,
    dlg_last_update bigint NOT NULL,
    dlg_participants_count integer,
    dlg_date timestamp without time zone NOT NULL,
    dlg_migrated_to bigint,
    dlg_bot boolean
);






COMMENT ON COLUMN telegram.dialogs.tps_id IS 'Dialog type';






COMMENT ON COLUMN telegram.dialogs.nic_id IS 'current dialog nick, id could be changed';






COMMENT ON COLUMN telegram.dialogs.dlg_title IS 'Can be empty in case of P2P user chat';






COMMENT ON COLUMN telegram.dialogs.dlg_photo IS 'Temporary flag indicator, must me refactored to photo record';






COMMENT ON COLUMN telegram.dialogs.dlg_bot_api_id IS '??';






COMMENT ON COLUMN telegram.dialogs.dlg_migrated_to IS '?? foreign key';






CREATE TABLE telegram.messages (
    msg_id bigint NOT NULL,
    mtp_id smallint NOT NULL,
    mac_id smallint,
    msg_id_reply bigint,
    msg_date timestamp without time zone NOT NULL,
    msg_title text,
    msg_created timestamp without time zone DEFAULT now() NOT NULL,
    dlg_id bigint NOT NULL,
    usr_id_from bigint,
    usr_id_to bigint,
    msg_via_bot_id bigint
);






COMMENT ON COLUMN telegram.messages.msg_id_reply IS 'Set FK after debug completed';






CREATE TABLE telegram.messages_actions (
    mac_id smallint NOT NULL,
    mac_title character varying(64) NOT NULL,
    mac_created timestamp without time zone DEFAULT now() NOT NULL
);






CREATE SEQUENCE telegram.messages_actions_mac_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;






ALTER SEQUENCE telegram.messages_actions_mac_id_seq OWNED BY telegram.messages_actions.mac_id;






CREATE TABLE telegram.messages_addresses (
    msg_id bigint NOT NULL,
    adr_id integer NOT NULL
);






CREATE TABLE telegram.messages_types (
    mtp_id bigint DEFAULT nextval(('telegram.messages_types_mtp_id_seq' :: text) :: regclass) NOT NULL,
    mtp_title character varying(64) NOT NULL,
    mtp_created timestamp without time zone DEFAULT now() NOT NULL
);






CREATE SEQUENCE telegram.messages_types_mtp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 32767
    CACHE 1;






ALTER SEQUENCE telegram.messages_types_mtp_id_seq OWNED BY telegram.messages_types.mtp_id;






CREATE TABLE telegram.messages_urls (
    msg_id bigint NOT NULL,
    url_id integer NOT NULL
);






CREATE TABLE telegram.nicknames (
    nic_id integer NOT NULL,
    nic_title character varying(128) NOT NULL,
    nic_created timestamp without time zone DEFAULT now() NOT NULL
);






COMMENT ON TABLE telegram.nicknames IS 'Contain uniq names for all users/groups/channel';






CREATE SEQUENCE telegram.nicknames_nic_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;






ALTER SEQUENCE telegram.nicknames_nic_id_seq OWNED BY telegram.nicknames.nic_id;






CREATE TABLE telegram.types (
    tps_id bigint DEFAULT nextval(('telegram.types_tps_id_seq' :: text) :: regclass) NOT NULL,
    tps_title character varying(64) NOT NULL
);






CREATE SEQUENCE telegram.types_tps_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 32767
    CACHE 1;






CREATE TABLE telegram.urls_addresses (
    url_id integer NOT NULL,
    adr_id integer NOT NULL
);






CREATE TABLE telegram.users (
    usr_id bigint NOT NULL,
    tps_id smallint,
    usr_first_name character varying(256),
    usr_last_name character varying(256),
    usr_verified boolean,
    usr_restricted boolean,
    usr_access_hash bigint,
    usr_created timestamp without time zone DEFAULT now() NOT NULL,
    usr_name character varying(32),
    usr_phone character varying(24),
    usr_last_online timestamp without time zone,
    ust_id smallint,
    usr_last_update timestamp without time zone,
    usr_status_expires timestamp without time zone
);






COMMENT ON COLUMN telegram.users.usr_name IS 'Current Username';






COMMENT ON COLUMN telegram.users.usr_phone IS 'Current User Phone';






COMMENT ON COLUMN telegram.users.ust_id IS 'User online status';






COMMENT ON COLUMN telegram.users.usr_last_update IS 'Timestamp of user last update';






COMMENT ON COLUMN telegram.users.usr_status_expires IS 'Timestamp of user status (ust_id) expiration, techically must me set to null';






CREATE TABLE telegram.users_history (
    uhs_id bigint NOT NULL,
    uhs_name character varying(32),
    uhs_first_name character varying(256),
    uhs_last_name character varying(256),
    uhs_verified boolean,
    uhs_phone character varying(24),
    uhs_restricted boolean,
    uhs_access_hash bigint,
    usr_id bigint NOT NULL
);






CREATE SEQUENCE telegram.users_history_uhs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;






ALTER SEQUENCE telegram.users_history_uhs_id_seq OWNED BY telegram.users_history.uhs_id;






CREATE TABLE telegram.users_statuses (
    ust_id bigint DEFAULT nextval(('telegram.users_statuses_ust_id_seq' :: text) :: regclass) NOT NULL,
    ust_title character varying(64) NOT NULL,
    ust_created timestamp without time zone DEFAULT now() NOT NULL
);






CREATE SEQUENCE telegram.users_statuses_ust_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 32767
    CACHE 1;






ALTER TABLE ONLY coins.addresses ALTER COLUMN adr_id SET DEFAULT nextval('coins.addresses_adr_id_seq' :: regclass);






ALTER TABLE ONLY coins.primablock_transactions ALTER COLUMN pbt_id SET DEFAULT nextval('coins.primablock_transactions_pbt_id_seq' :: regclass);






ALTER TABLE ONLY public.hosts ALTER COLUMN hst_id SET DEFAULT nextval('public.hosts_hst_id_seq' :: regclass);






ALTER TABLE ONLY telegram.messages_actions ALTER COLUMN mac_id SET DEFAULT nextval('telegram.messages_actions_mac_id_seq' :: regclass);






ALTER TABLE ONLY telegram.messages_types ALTER COLUMN mtp_id SET DEFAULT nextval('telegram.messages_types_mtp_id_seq' :: regclass);






ALTER TABLE ONLY telegram.nicknames ALTER COLUMN nic_id SET DEFAULT nextval('telegram.nicknames_nic_id_seq' :: regclass);






ALTER TABLE ONLY telegram.users_history ALTER COLUMN uhs_id SET DEFAULT nextval('telegram.users_history_uhs_id_seq' :: regclass);






ALTER TABLE ONLY coins.addresses
    ADD CONSTRAINT addresses_adr_title_key UNIQUE (adr_title);






ALTER TABLE ONLY coins.addresses
    ADD CONSTRAINT addresses_pkey PRIMARY KEY (adr_id);






ALTER TABLE ONLY coins.coins
    ADD CONSTRAINT coins_con_code_key UNIQUE (con_code);






ALTER TABLE ONLY coins.coins
    ADD CONSTRAINT coins_con_uid_key UNIQUE (con_uid);






ALTER TABLE ONLY coins.coins
    ADD CONSTRAINT coins_pkey PRIMARY KEY (con_id);






ALTER TABLE ONLY coins.icos
    ADD CONSTRAINT icos_pkey PRIMARY KEY (ico_id);






ALTER TABLE ONLY coins.primablock
    ADD CONSTRAINT primablock_pkey PRIMARY KEY (prm_id);






ALTER TABLE ONLY coins.primablock_transactions
    ADD CONSTRAINT primablock_transactions_pkey PRIMARY KEY (pbt_id);






ALTER TABLE ONLY public.hosts
    ADD CONSTRAINT hosts_hst_title_key UNIQUE (hst_title);






ALTER TABLE ONLY public.hosts
    ADD CONSTRAINT hosts_pkey PRIMARY KEY (hst_id);






ALTER TABLE ONLY public.urls
    ADD CONSTRAINT table_url_hash_key UNIQUE (url_hash);






ALTER TABLE ONLY public.urls
    ADD CONSTRAINT urls_pkey PRIMARY KEY (url_id);






ALTER TABLE ONLY telegram.dialogs
    ADD CONSTRAINT dialogs_pkey PRIMARY KEY (dlg_id);






ALTER TABLE ONLY telegram.messages_actions
    ADD CONSTRAINT messages_actions_mac_title_key UNIQUE (mac_title);






ALTER TABLE ONLY telegram.messages_actions
    ADD CONSTRAINT messages_actions_pkey PRIMARY KEY (mac_id);






ALTER TABLE ONLY telegram.messages_addresses
    ADD CONSTRAINT messages_addresses_pkey PRIMARY KEY (msg_id, adr_id);






ALTER TABLE ONLY telegram.messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (msg_id);






ALTER TABLE ONLY telegram.messages_types
    ADD CONSTRAINT messages_types_mtp_title_key UNIQUE (mtp_title);






ALTER TABLE ONLY telegram.messages_types
    ADD CONSTRAINT messages_types_pkey PRIMARY KEY (mtp_id);






ALTER TABLE ONLY telegram.messages_urls
    ADD CONSTRAINT messages_urls_pkey PRIMARY KEY (msg_id, url_id);






ALTER TABLE ONLY telegram.nicknames
    ADD CONSTRAINT nicknames_nic_name_key UNIQUE (nic_title);






ALTER TABLE ONLY telegram.nicknames
    ADD CONSTRAINT nicknames_pkey PRIMARY KEY (nic_id);






ALTER TABLE ONLY telegram.types
    ADD CONSTRAINT types_pkey PRIMARY KEY (tps_id);






ALTER TABLE ONLY telegram.types
    ADD CONSTRAINT types_tps_title_key UNIQUE (tps_title);






ALTER TABLE ONLY telegram.urls_addresses
    ADD CONSTRAINT urls_addresses_pkey PRIMARY KEY (url_id, adr_id);






ALTER TABLE ONLY telegram.users_history
    ADD CONSTRAINT users_history_pkey PRIMARY KEY (uhs_id);






ALTER TABLE ONLY telegram.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (usr_id);






ALTER TABLE ONLY telegram.users_statuses
    ADD CONSTRAINT users_statuses_pkey PRIMARY KEY (ust_id);






ALTER TABLE ONLY telegram.users_statuses
    ADD CONSTRAINT users_statuses_ust_title_key UNIQUE (ust_title);






CREATE INDEX primablock_transactions_idx ON coins.primablock_transactions USING btree (adr_id);






CREATE INDEX messages_addresses_idx_adr_id ON telegram.messages_addresses USING btree (adr_id);






CREATE INDEX messages_addresses_idx_msg_id ON telegram.messages_addresses USING btree (msg_id);






CREATE INDEX messages_idx_mac_id ON telegram.messages USING btree (mac_id);






CREATE INDEX messages_idx_mtp_id ON telegram.messages USING btree (mtp_id);






CREATE INDEX messages_idx_usr_id_from ON telegram.messages USING btree (usr_id_from);






CREATE INDEX messages_idx_usr_id_to ON telegram.messages USING btree (usr_id_to);






CREATE INDEX messages_urls_idx_msg_id ON telegram.messages_urls USING btree (msg_id);






CREATE INDEX messages_urls_idx_url_id ON telegram.messages_urls USING btree (url_id);






CREATE INDEX urls_addresses_idx_adr_id ON telegram.urls_addresses USING btree (adr_id);






CREATE INDEX urls_addresses_idx_url_id ON telegram.urls_addresses USING btree (url_id);






CREATE INDEX users_history_idx ON telegram.users_history USING btree (usr_id);






ALTER TABLE ONLY coins.addresses
    ADD CONSTRAINT addresses_fk_adr_id FOREIGN KEY (con_id) REFERENCES coins.coins(con_id) ON UPDATE CASCADE ON DELETE RESTRICT DEFERRABLE INITIALLY DEFERRED;






ALTER TABLE ONLY coins.primablock
    ADD CONSTRAINT primablock_fk FOREIGN KEY (prm_id) REFERENCES coins.addresses(adr_id) ON UPDATE CASCADE ON DELETE RESTRICT DEFERRABLE INITIALLY DEFERRED;






ALTER TABLE ONLY coins.primablock
    ADD CONSTRAINT primablock_fk_ico_id FOREIGN KEY (ico_id) REFERENCES coins.icos(ico_id) ON UPDATE CASCADE ON DELETE RESTRICT DEFERRABLE INITIALLY DEFERRED;






ALTER TABLE ONLY coins.primablock_transactions
    ADD CONSTRAINT primablock_transactions_fk FOREIGN KEY (prm_id) REFERENCES coins.primablock(prm_id) ON UPDATE CASCADE ON DELETE RESTRICT DEFERRABLE INITIALLY DEFERRED;






ALTER TABLE ONLY coins.primablock_transactions
    ADD CONSTRAINT primablock_transactions_fk1 FOREIGN KEY (adr_id) REFERENCES coins.addresses(adr_id) ON UPDATE CASCADE ON DELETE RESTRICT DEFERRABLE INITIALLY DEFERRED;






ALTER TABLE ONLY public.urls
    ADD CONSTRAINT urls_fk FOREIGN KEY (hst_id) REFERENCES public.hosts(hst_id) ON UPDATE CASCADE ON DELETE RESTRICT DEFERRABLE INITIALLY DEFERRED;






ALTER TABLE ONLY telegram.dialogs
    ADD CONSTRAINT dialogs_fk FOREIGN KEY (tps_id) REFERENCES telegram.types(tps_id) ON UPDATE CASCADE ON DELETE RESTRICT DEFERRABLE INITIALLY DEFERRED;






ALTER TABLE ONLY telegram.dialogs
    ADD CONSTRAINT dialogs_fk1 FOREIGN KEY (nic_id) REFERENCES telegram.nicknames(nic_id) ON UPDATE CASCADE ON DELETE RESTRICT;






ALTER TABLE ONLY telegram.messages_addresses
    ADD CONSTRAINT messages_addresses_fk_adr_id FOREIGN KEY (adr_id) REFERENCES coins.addresses(adr_id) ON UPDATE CASCADE ON DELETE RESTRICT DEFERRABLE INITIALLY DEFERRED;






ALTER TABLE ONLY telegram.messages_addresses
    ADD CONSTRAINT messages_addresses_fk_msg_id FOREIGN KEY (msg_id) REFERENCES telegram.messages(msg_id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;






ALTER TABLE ONLY telegram.messages
    ADD CONSTRAINT messages_fk_dlg_id FOREIGN KEY (dlg_id) REFERENCES telegram.dialogs(dlg_id) ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED;






ALTER TABLE ONLY telegram.messages
    ADD CONSTRAINT messages_fk_mac_id FOREIGN KEY (mac_id) REFERENCES telegram.messages_actions(mac_id) ON UPDATE CASCADE ON DELETE RESTRICT DEFERRABLE INITIALLY DEFERRED;






ALTER TABLE ONLY telegram.messages
    ADD CONSTRAINT messages_fk_mtp_id FOREIGN KEY (mtp_id) REFERENCES telegram.messages_types(mtp_id) ON UPDATE CASCADE ON DELETE RESTRICT DEFERRABLE INITIALLY DEFERRED;






ALTER TABLE ONLY telegram.messages
    ADD CONSTRAINT messages_fk_usr_id_from FOREIGN KEY (usr_id_from) REFERENCES telegram.users(usr_id) ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED;






ALTER TABLE ONLY telegram.messages
    ADD CONSTRAINT messages_fk_usr_id_to FOREIGN KEY (usr_id_to) REFERENCES telegram.users(usr_id) ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED;






ALTER TABLE ONLY telegram.messages_urls
    ADD CONSTRAINT messages_urls_fk FOREIGN KEY (url_id) REFERENCES public.urls(url_id) ON UPDATE CASCADE ON DELETE RESTRICT DEFERRABLE INITIALLY DEFERRED;






ALTER TABLE ONLY telegram.messages_urls
    ADD CONSTRAINT messages_urls_fk_msg_id FOREIGN KEY (msg_id) REFERENCES telegram.messages(msg_id) ON UPDATE CASCADE ON DELETE RESTRICT DEFERRABLE INITIALLY DEFERRED;






ALTER TABLE ONLY telegram.urls_addresses
    ADD CONSTRAINT urls_addresses_fk_adr_id FOREIGN KEY (adr_id) REFERENCES coins.addresses(adr_id) ON UPDATE CASCADE ON DELETE RESTRICT DEFERRABLE INITIALLY DEFERRED;






ALTER TABLE ONLY telegram.urls_addresses
    ADD CONSTRAINT urls_addresses_fk_url_id FOREIGN KEY (url_id) REFERENCES public.urls(url_id) ON UPDATE CASCADE ON DELETE RESTRICT DEFERRABLE INITIALLY DEFERRED;






ALTER TABLE ONLY telegram.users_history
    ADD CONSTRAINT users_history_fk_usr_id FOREIGN KEY (usr_id) REFERENCES telegram.users(usr_id) ON UPDATE CASCADE ON DELETE RESTRICT DEFERRABLE;







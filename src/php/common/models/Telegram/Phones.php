<?php
namespace App\Models\Telegram;

class Phones extends \App\Models\Telegram\Base
{
    public static function upsert($phn_number)
    {
        $phn_number = preg_replace("/[^0-9]/", "", $phn_number);
        if (empty($phn_number)) {
            throw new \InvalidArgumentException('Empty phone number');
        }

        // Check if dialog is already exists
        $phn = self::findFirst([
                'conditions' => 'phn_number = :phn_number:',
                'bind' => ['phn_number' => $phn_number]
            ]
        );

        if (!empty($phn->phn_id)) {
            return $phn->phn_id;
        }

        $phn = new self();

        if (!$phn->save(['phn_number' => $phn_number])) {
            foreach ($phn->getMessages() as $message) {
                trigger_error($message);
            }
            throw new \RuntimeException('Failed to insert new phone');
        }

        return $phn->phn_id;
    }
}
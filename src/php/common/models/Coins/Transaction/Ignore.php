<?php

namespace App\Models\Coins\Transaction;

use App\Models\Coins\Addresses;

class Ignore  extends \App\Models\Coins\Base
{
    public function initialize()
    {
        parent::initialize();
        $this->setSource('transaction_ignore');
        $this->belongsTo('adr_id','App\Models\Coins\Addresses','adr_id',['alias'=>'adr']);
    }

    public function process($adr_title, $title = null)
    {
        if (empty($adr_title)) {
            throw new \InvalidArgumentException('Empty adr_title');
        }

        $adr_model = new Addresses();
        $adr_model->init($this->getLogger());

        $adr_id = $adr_model->process($adr_title);

        if (empty($adr_id)) {
            throw new \RuntimeException('Empty adr_id');
        }

        $data = [
            'adr_id' => $adr_id,
            'tig_title' => $title
        ];

        $rec = self::findFirst([
            'conditions' => 'adr_id = :adr_id:',
            'bind' => ['adr_id' => $adr_id]
        ]);

        if (empty($rec->tig_id)) {
            $rec = new self;
            $rec->save($data);
            if ($rec->getMessages()) {
                foreach ($rec->getMessages() as $message) {
                    trigger_error($message);
                }
                throw new \RuntimeException("Fail to insert ignore_adr_id {$adr_id}");
            }
            $this->ldebug('Insert new address for ignoring', ['tig_id' => $rec->tig_id]);
        }

        return $rec->tig_id;
    }
}
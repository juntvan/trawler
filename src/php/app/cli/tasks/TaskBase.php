<?php

class TaskBase extends Phalcon\CLI\Task
{
    private $proto;

    protected function telegramLogin($session = 'session.madeline')
    {
        if (!empty($this->proto)) {
            return $this->proto;
        }

        $acc_data = null;
        $address = '';
        $port = 0;

        if ($GLOBALS['argc'] < 4 || empty($GLOBALS['argv'][3])) {
            throw new \InvalidArgumentException('Pass the name of account or acc_id');
        }

        $acc = $GLOBALS['argv'][3];
        $num = intval($acc);
        if ($num > 0) {
            $acc_data = \App\Models\Telegram\Accounts::findFirst([
                'conditions' => 'acc_id = :acc_id:',
                'bind' => ['acc_id' => $num]
            ]);
        } else {
            $acc_data = \App\Models\Telegram\Accounts::findFirst([
                'conditions' => 'acc_name = :acc_name:',
                'bind' => ['acc_name' => $acc]
            ]);
        }

        if ($acc_data) {
            $GLOBALS['acc_id'] = $acc_data->acc_id;
            $address = $acc_data->acc_proxy_host;
            $port = $acc_data->acc_proxy_port;
            $dir = $acc_data->acc_tel;
            if (!is_dir($dir)) {
                mkdir($dir, 0664);
            }
            $session = $dir . '/' . $session;
        }


        $settings = [
            'app_info' => [
                'api_id' => $this->config->telegram->api->id,
                'api_hash' => $this->config->telegram->api->hash
            ],
            'updates' => ['handle_updates' => false],
            'connection_settings'=> [
                'all' => [
                    'timeout' => 10,
                    'proxy' => '\SocksProxy',
                    'proxy_extra' => [
                        'address' => $address,
                        'port' => $port
                    ]
                ]
            ],
            'logger' => [
                    'logger_level' => 4
                ],
            'peer' => [
                'full_fetch' => false,
                'cache_all_peers_on_startup' => false,
                'full_info_cache_time' => 10000
            ],
            'flood_timeout' => [
                'wait_if_lt' => 120
            ]
        ];

        if (strpos($session, 'event') !== false) {
            $settings['updates']['handle_updates'] = true;
            $settings['updates']['handle_old_updates'] = true;
        }

        $this->proto = new \danog\MadelineProto\API($session, $settings);

        $this->proto->start();

        return $this->proto;
    }
}

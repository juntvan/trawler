<?php

namespace App\Models;

use attics\Lib\Llog\Logger;

class Base extends ModelBase
{
    use Logger;
    
    public function initialize()
    {
        $this->setSchema('public');
    }

    public function init($logger) {
        $this->setLogger($logger);
    }
}
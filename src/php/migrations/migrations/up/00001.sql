ALTER TABLE coins.primablock ADD prm_scraped TIMESTAMP NULL;
ALTER TABLE coins.primablock RENAME COLUMN prm_capacity TO prm_allocation;
ALTER TABLE coins.primablock RENAME COLUMN prm_collected TO prm_pool_value;
ALTER TABLE coins.primablock ADD prm_status VARCHAR(24) NULL;
ALTER TABLE coins.primablock_transactions ALTER COLUMN pbt_hash TYPE CHAR(128) USING pbt_hash :: CHAR(128);
ALTER TABLE coins.primablock_transactions ADD pbt_from CHAR(64) NULL;
ALTER TABLE coins.primablock_transactions ADD pbt_to CHAR(64) NULL;
ALTER TABLE coins.coins ADD adr_id_contract INT NULL;


ALTER TABLE coins.coins
ADD CONSTRAINT coins_fk_con_id
FOREIGN KEY (adr_id_contract) REFERENCES addresses (adr_id) ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED;


CREATE SEQUENCE coins.coins_con_id_seq
MAXVALUE 2147483647;

ALTER TABLE coins.coins
  ALTER COLUMN con_id TYPE INTEGER;

ALTER TABLE coins.coins
  ALTER COLUMN con_id SET DEFAULT nextval('coins.coins_id_seq' :: text);

ALTER TABLE coins.primablock_transactions ALTER COLUMN pbt_from TYPE INTEGER USING pbt_from :: INTEGER;
ALTER TABLE coins.primablock_transactions ALTER COLUMN pbt_to TYPE INTEGER USING pbt_to :: INTEGER;

CREATE INDEX coins_idx_adr_id_contract ON coins.coins
  USING btree (adr_id_contract);

CREATE INDEX addresses_idx_con_id ON coins.addresses
  USING btree (con_id);

ALTER TABLE coins.addresses
  RENAME CONSTRAINT addresses_fk_adr_id TO addresses_fk_con_id;

ALTER TABLE coins.coins
  RENAME CONSTRAINT coins_fk TO coins_fk_adr_id;


ALTER TABLE coins.primablock
  RENAME CONSTRAINT primablock_fk TO primablock_fk_adr_id;

CREATE INDEX primablock_idx_ico_id ON coins.primablock
  USING btree (ico_id);

ALTER TABLE coins.primablock_transactions
  RENAME CONSTRAINT primablock_transactions_fk TO primablock_transactions_fk_prm_id;

ALTER TABLE coins.primablock_transactions
  RENAME CONSTRAINT primablock_transactions_fk1 TO primablock_transactions_fk_adr_id;

ALTER TABLE coins.primablock_transactions
  ADD COLUMN con_id INTEGER;

ALTER TABLE coins.primablock_transactions
  RENAME COLUMN pbt_from TO adr_from;

ALTER TABLE coins.primablock_transactions
  RENAME COLUMN pbt_to TO adr_to;

ALTER INDEX coins.primablock_transactions_idx
  RENAME TO primablock_transactions_idx_adr_id;

ALTER TABLE coins.primablock_transactions
  ADD CONSTRAINT primablock_transactions_fk_adr_id_from FOREIGN KEY (adr_from)
    REFERENCES coins.addresses(adr_id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
    DEFERRABLE
    INITIALLY DEFERRED;

ALTER TABLE coins.primablock_transactions
  ADD CONSTRAINT primablock_transactions_fk_adr_id_to FOREIGN KEY (adr_to)
    REFERENCES coins.addresses(adr_id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
    DEFERRABLE
    INITIALLY DEFERRED;

ALTER TABLE coins.primablock_transactions
  ADD CONSTRAINT primablock_transactions_fk_con_id FOREIGN KEY (con_id)
    REFERENCES coins.coins(con_id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
    DEFERRABLE
    INITIALLY DEFERRED;

CREATE INDEX primablock_transactions_idx_prm_id ON coins.primablock_transactions
  USING btree (prm_id);

CREATE INDEX primablock_transactions_idx_adr_id_from ON coins.primablock_transactions
  USING btree (adr_from);

CREATE INDEX primablock_transactions_idx_adr_id_to ON coins.primablock_transactions
  USING btree (adr_to);

CREATE INDEX primablock_transactions_idx_con_id ON coins.primablock_transactions
  USING btree (con_id);

ALTER TABLE public.urls
  RENAME CONSTRAINT urls_fk TO urls_fk_hst_id;

CREATE INDEX urls_idx_hst_id ON public.urls
  USING btree (hst_id);

ALTER TABLE telegram.dialogs
  RENAME CONSTRAINT dialogs_fk TO dialogs_fk_tps_id;

ALTER TABLE telegram.dialogs
  RENAME CONSTRAINT dialogs_fk1 TO dialogs_fk_nic_id;

CREATE INDEX dialogs_idx_tps_id ON telegram.dialogs
  USING btree (tps_id);

CREATE INDEX dialogs_idx_nic_id ON telegram.dialogs
  USING btree (nic_id);

ALTER TABLE telegram.messages_urls
  RENAME CONSTRAINT messages_urls_fk TO messages_urls_fk_url_id;

ALTER TABLE coins.primablock ADD prm_invalid BOOLEAN NULL;

ALTER TABLE coins.primablock ALTER COLUMN prm_allocation TYPE NUMERIC(12,2) USING prm_allocation :: NUMERIC(12,2);
ALTER TABLE coins.primablock ALTER COLUMN prm_pool_value TYPE NUMERIC(12,2) USING prm_pool_value :: NUMERIC(12,2);
ALTER TABLE coins.primablock ALTER COLUMN prm_contribution_max TYPE NUMERIC(12,2) USING prm_contribution_max :: NUMERIC(12,2);
<?php

namespace App\Models\Coins;

class Base extends \App\Models\ModelBase
{
    use \attics\Lib\Llog\Logger;

    public function initialize()
    {
        $this->setSchema('coins');
    }

    public function init($logger)
    {
        $this->setLogger($logger);
    }
}
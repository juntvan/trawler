<?php

namespace App\Models\Coins\Icos;

use App\Lib\Tools;
use App\Models\Coins\Addresses;
use App\Models\Coins\Coins;

class Tokentransfers extends \App\Models\Coins\Base
{
    public function initialize()
    {
        parent::initialize();
        $this->setSource('icos_tokentransfers');
        $this->belongsTo('ico_id','App\Models\Coins\Icos','ico_id',['alias'=>'ico']);
        $this->belongsTo('adr_from','App\Models\Coins\Addresses','adr_id',['alias'=>'adrfrom']);
        $this->belongsTo('adr_to','App\Models\Coins\Addresses','adr_id',['alias'=>'adrto']);
        $this->belongsTo('con_id','App\Models\Coins\Coins','con_id',['alias'=>'con']);
    }

    public function process($transaction, $ico_id)
    {
        $data = [];

        if (empty($transaction)) {
            throw new \InvalidArgumentException('Empty transaction arg');
        }

        if (empty($ico_id)) {
            throw new \InvalidArgumentException('Empty ico_id');
        }
        $ico_id = intval($ico_id);
        $data['ico_id'] = $ico_id;
        if (empty($data['ico_id'])) {
            throw new \InvalidArgumentException('Invalid ico_id');
        }

        if (!empty($transaction['tokenSymbol'])) {
            if (empty($transaction['tokenName'])) {
                throw new \InvalidArgumentException('Empty tokenName ' . $transaction);
            }
            $data['con_id'] = Coins::process($transaction['tokenName'], $transaction['tokenName'],
                $transaction['tokenSymbol'], 'api.etherscan.io');
        }
        else{
            $data['con_id'] = Coins::process();
        }

        if (empty($transaction['hash'])) {
            throw new \InvalidArgumentException("Empty hash for ico_id {$ico_id}");
        }
        $data['ict_hash'] = trim($transaction['hash']);

        if (empty($transaction['timeStamp'])) {
            throw new \InvalidArgumentException("Empty timeStamp for ico_id {$ico_id}");
        }
        $data['ict_timestamp'] = intval($transaction['timeStamp']);
        if (empty($data['ict_timestamp'])) {
            throw new \InvalidArgumentException("Invalid timeStamp for ico_id {$ico_id}");
        }
        $data['ict_timestamp'] = Tools::date_convert($data['ict_timestamp']);
        if (empty($data['ict_timestamp'])) {
            throw new \InvalidArgumentException("Time convertion error for ico_id {$ico_id}");
        }

        if (empty($transaction['blockNumber'])) {
            throw new \InvalidArgumentException("Empty blockNumber for ico_id {$ico_id}");
        }
        $data['ict_block'] = intval($transaction['blockNumber']);
        if (empty($data['ict_block'])) {
            throw new \InvalidArgumentException("Invalid blockNumber for ico_id {$ico_id}");
        }

        if (isset($transaction['value'])) {

            $right_op = '1000000000000000000'; //for eth
            $scale = 18;
            if (!empty($transaction['tokenDecimal'])) {
                $right_op = bcpow('10', intval($transaction['tokenDecimal']));
                $scale = intval($transaction['tokenDecimal']);
            }

            $data['ict_value'] = bcdiv($transaction['value'], $right_op, $scale);
        }

        $data['ict_success'] = 1;
        if (isset($transaction['isError'])) {
            if (intval($transaction['isError']) > 0) {
                $data['ict_success'] = 0;
            }
        }

        $adr_handler = new Addresses();
        $adr_handler->setLogger($this->getLogger());


        $data['adr_to'] = null;
        $data['adr_from'] = null;
        if (!empty($transaction['to'])) {
            $data['adr_to'] = $adr_handler->process($transaction['to']);
        }

        if (!empty($transaction['from'])) {
            $data['adr_from'] = $adr_handler->process($transaction['from']);
        }

        $rec = self::findFirst([
                'conditions' => 'ict_hash = :ict_hash: AND adr_to = :adr_to: AND adr_from = :adr_from:',
                'bind' => [
                    'ict_hash' => trim($data['ict_hash']),
                    'adr_to' => $data['adr_to'],
                    'adr_from' => $data['adr_from']
                ]
            ]
        );

        if (empty($rec->ict_created)) {
            $rec = new self();
            $rec->save($data);
            if ($rec->getMessages()) {
                foreach ($rec->getMessages() as $err) {
                    trigger_error($err, E_USER_ERROR);
                }
                throw new \RuntimeException('Failed to insert new token transfer ', $data);
            }
            $this->ldebug('New token transfer', ['id' => $rec->ict_id]);
        }
        else {
            $this->ldebug('Token transfer already exist', ['id' => $rec->ict_id]);
        }

        return $rec->ict_id;
    }


}
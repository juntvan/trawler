<?php

namespace App\Models;

use App\Lib\Tools;

class Hosts extends \App\Models\Base
{
    public static function prepareHst ($url)
    {
        if (empty($url)) {
            return null;
        }

        $url = self::addScheme($url);
        $parsed_url = parse_url($url);
        if (empty($parsed_url['host'])) {
            throw new \RuntimeException('Empty host in URL '.$url);
        }

        $hst_title = strtolower($parsed_url['host']);
        $hst_title = preg_replace("/^www\./", '', $hst_title);
        return $hst_title;
    }

    public function upsert($url)
    {
        if (empty($url)) {
            throw new \InvalidArgumentException('Empty url');
        }

        try {
            $hst_title = self::prepareHst($url);
        } catch (\RuntimeException $e) {
            throw new \RuntimeException('Empty host in URL '.$url);
        }

        $data = [
            'hst_title' => $hst_title
        ];

        $rec = self::findFirst([
            'conditions' => 'hst_title = :hst_title:',
            'bind' => ['hst_title' => $hst_title]
        ]);

        if (empty($rec->hst_id)) {
            $rec = new self();
            $rec->save($data);
        } else {
            // Compare new and old records
            $diff = [];
            foreach ($data as $field => $value) {
                if (empty($rec->$field) or $rec->$field != $value) {
                    $diff[$field] = $value;
                }
            }
            if (!empty($diff)) {
                $rec->update($diff);
                $this->ldebug('Updating existing host', [$rec->hst_id, $rec->hst_title] + $diff);
            }
        }

        if ($rec->getMessages()) {
            foreach ($rec->getMessages() as $message) {
                trigger_error($message);
            }
            throw new \RuntimeException("Failed to insert new host for $url");
        }

        $this->ldebug("Added new host with hst_id: $rec->hst_id; hst_title: $rec->hst_title");
        return $rec->hst_id;
    }

    private static function addScheme($url)
    {
        if (preg_match("/^https?:\/\//", $url)) {
            return $url;
        } else {
            return 'http://' . $url;
        }
    }

}
<?php
namespace App\Models\Telegram;

class Types extends \App\Models\Telegram\Base
{
    const USER = 'user';
    const CHANNEL = 'channel';
    const CHAT = 'chat';
    const SUPERGROUP = 'supergroup';
    const BOT = 'bot';
    const FILLTEMP = 'will_be_filled_in_future';

    /**
     * Validate type title
     * @param $tps_title
     * @return bool|string
     */
    public static function isValid($tps_title)
    {
        $tps_title = trim(strtolower($tps_title));
        if(!in_array($tps_title,[self::USER,self::CHANNEL,self::CHAT,self::SUPERGROUP,self::BOT,self::FILLTEMP])){
            return false;
        }

        return $tps_title;
    }

    /**
     * Create new title if not exists
     * @param string $tps_title
     * @return integer
     */
    public static function upsert($tps_title)
    {
        $tps_title = self::isValid($tps_title);
        if(empty($tps_title)){
            throw new \InvalidArgumentException('Invalid type');
        }

        static $cache;
        if(!empty($cache[$tps_title])){
            return $cache[$tps_title];
        }

        $tps = self::findFirst([
                'conditions' => 'tps_title = :tps_title:',
                'bind' => ['tps_title' => $tps_title]
            ]
        );
        
        if(empty($tps->tps_id)){
            // Insert new
            $tps = new self();
            $tps->save(['tps_title' => $tps_title]);
        }

        if($tps->getMessages()){
            foreach($tps->getMessages() as $message){
                trigger_error($message);
            }
            throw new \RuntimeException('Failed to insert new type');
        }

        if(empty($tps->tps_id)){
            throw new \RuntimeException('Failed to insert new type');
        }

        $cache[$tps_title] = $tps->tps_id;

        return $cache[$tps_title];
    }
}
<?php
namespace App\Models\Telegram\Users;

class Statuses extends \App\Models\Telegram\Base
{

    public function initialize()
    {
        parent::initialize();
        $this->setSource('users_statuses');
    }


    /**
     * Create new status if not exists
     * @param string $ust_title
     * @return integer
     */
    public static function upsert($ust_title)
    {
        $ust_title = trim($ust_title);
        if(empty($ust_title)){
            throw new \InvalidArgumentException('Invalid status');
        }

        static $cache;
        if(!empty($cache[$ust_title])){
            return $cache[$ust_title];
        }

        $ust = self::findFirst([
                'conditions' => 'ust_title = :ust_title:',
                'bind' => ['ust_title' => $ust_title]
            ]
        );

        if(empty($ust->ust_id)){
            // Insert new
            $ust = new self();
            $ust->save(['ust_title' => $ust_title]);
        }

        if($ust->getMessages()){
            foreach($ust->getMessages() as $message){
                trigger_error($message);
            }
            throw new \RuntimeException('Failed to insert new status');
        }

        if(empty($ust->ust_id)){
            throw new \RuntimeException('Failed to insert new status');
        }

        $cache[$ust_title] = $ust->ust_id;

        return $cache[$ust_title];
    }
}
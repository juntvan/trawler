<?php

$db_name = getenv('DB_NAME');
$db_pass = getenv('DB_PASSWORD');
$db_usrname = getenv('DB_USER');
$db_host = getenv('DB_HOST');
$db_port = getenv('DB_PORT');

try {
    $dbconn = new PDO("pgsql:dbname={$db_name};host={$db_host}", $db_usrname, $db_pass);

    if($dbconn){
        echo "Connected to the {$db_name} database successfully!" . PHP_EOL;
    }
} catch (PDOException $e) {
    echo $e->getMessage();
    exit;
}

$sql = 'SELECT update_prm_number()';


$prepared_sql = $dbconn->prepare($sql);

try {
    $res = $prepared_sql->execute();

} catch (PDOException $e) {
    echo $e->getMessage();
    exit;
}

print_r($res);

echo PHP_EOL;
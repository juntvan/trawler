<?php
namespace App\Controllers;

use App\Lib\Response;
use App\Models\Telegram\Dialogs;
use function MongoDB\BSON\toJSON;

class IndexController extends ControllerBase
{
    public function indexAction()
    {
       echo 'index';
        $this->response->setContent('<div>index</div>');
        return $this->view;
    }
}
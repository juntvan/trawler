<?php

class TelegaTask extends TaskBase
{
    private $dialog_handler;

    public function eventsAction()
    {
        ini_set('memory_limit', '1512M');
        ini_set('xdebug.max_nesting_level', 400);
        $this->logger = $this->di->get('logger');

        $this->logger->debug('Start listening events');

        $proto = $this->telegramLogin('events.madeline');
        $proto->start();
        //$eventHandler = new EventHandler($MadelineProto);
        //$eventHandler->init($this->logger);
        $proto->setEventHandler('EventHandler');

        while (1) {
            try {
                $proto->loop();
            } catch (\danog\MadelineProto\Exception $e) {
                $this->logger->warning(__LINE__ . ' Fail in loop', [$e]);
                sleep(5);
            } catch (\Error $e) {
                if ($e->getCode() !== 0) {
                    break;
                }
                $this->logger->warning($e->getMessage() . ' Restart');
            } catch (Exception $e) {
                $this->logger->warning(__LINE__ . ' Fail in loop', ['err' => $e, 'code' => $e->getCode()]);
                if ($e->getCode() === -500) {
                    continue;
                }
                if ($e->getCode() === 0) {
                    continue;
                }
                break;
            }
        }
    }

    //just grab dialogs from acc
    public function getDialogsAction()
    {
        ini_set('memory_limit', '1512M');
        $this->logger = $this->di->get('logger');

        // Create dialog haldler
        $dialog_handler = new \App\Models\Telegram\Dialogs();
        $dialog_handler->init($this->logger);

        $proto = $this->telegramLogin();
        // Get list of available dialogs
        try {
            $dialogs = $proto->get_dialogs();
        } catch (\danog\MadelineProto\RPCErrorException $e) {
            if ($e->rpc === 'BOT_METHOD_INVALID') {
                \danog\MadelineProto\Logger::log("Bots can't execute this method!");
                return;
            } else {
                $this->logger->error($e->getMessage());
                return;
            }
        }

        foreach ($dialogs as $dialog) {

            $full = null;
            $attepts = 0;
            do {
                $attepts++;
                try {
                    $full = $proto->get_full_info($dialog);
                    $attepts = 3;
                } catch (\Exception $e) {
                    switch ($e->getCode()) {
                        case 420:
                            $this->logger->debug('Flood error, wait a little bit', [$e->getCode(), $e->getMessage()]);
                            sleep(10);
                            break;

                        default:
                            throw new \RuntimeException("Unexpected error {$e->getCode()} {$e->getMessage()}");
                    }
                }
            } while ($attepts < 3);

            if (!empty($full)) {
                //print_r($full);
                $dlg_id = $dialog_handler->process($full);
            } else {
                $this->logger->notice('Failed to get dialog full info');
                continue;
            }
            sleep(5);
        }
    }

    public function getParticipantsAction()
    {
        ini_set('memory_limit', '2548M');
        $this->logger = $this->di->get('logger');

        $user_handler = new \App\Models\Telegram\Users();
        $user_handler->init($this->logger);
        $dialog_handler = new \App\Models\Telegram\Dialogs();
        $dialog_handler->init($this->logger);
        $participants_handler = new \App\Models\Telegram\Participants();
        $participants_handler->init($this->logger);

        $proto = $this->telegramLogin();

        $this->logger->debug('Start getting participants');

        try {
            $dialogs = $proto->get_dialogs();
        } catch (\danog\MadelineProto\RPCErrorException $e) {
            if ($e->rpc === 'BOT_METHOD_INVALID') {
                \danog\MadelineProto\Logger::log("Bots can't execute this method!");
                return;
            } else {
                $this->logger->error($e->getMessage());
                return;
            }
        }

        foreach ($dialogs as $dialog) {
            try {
                $Chat = $proto->get_pwr_chat($dialog);
            } catch (Exception $e) {
                $this->logger->error('Fail to get_pwr_chat', ['e' => $e]);
                continue;
            }
            if (empty($Chat) || empty($Chat['participants'])) {
                sleep(10);
                continue;
            }

            foreach ($Chat['participants'] as $participant) {

                if (empty($participant['user'])) {
                    $this->logger->warning('Empty participant[user]');
                }

                $usr_id = $user_handler->upsert($participant['user']);

                $dlg_id = null;
                if (!empty($dialog['channel_id'])) {
                    $dlg_id = intval($dialog['channel_id']);
                } elseif (!empty($dialog['chat_id'])) {
                    $dlg_id = intval($dialog['chat_id']);
                } elseif (!empty($dialog['user_id'])) {
                    $dlg_id = intval($dialog['user_id']);
                }

                $dlg_id = $dialog_handler->getsetDlgId($dlg_id);

                if (empty($dlg_id)) {
                    $this->logger->debug('Empty dlg_id');
                    continue;
                }

                $date = null;
                if (!empty($participant['date'])) {
                    $date = $participant['date'];
                }

                $participants_handler->upsert($dlg_id, $usr_id, $date, false);
            }
            sleep(6);
        }
    }

    public function dialogsAction()
    {
        // this is memory hungry operation
        ini_set('memory_limit', '2012M');
        ini_set('xdebug.max_nesting_level', 400);
        $this->logger = $this->di->get('logger');

        //get all participants before searching msg
//        $this->getParticipantsAction();

        // Create dialog haldler
        $dialog_handler = new \App\Models\Telegram\Dialogs();
        $dialog_handler->init($this->logger);

        //create messages handler
        $messages_handler = new \App\Models\Telegram\Messages();
        $messages_handler->init($this->logger);

        $participant_handler = new \App\Models\Telegram\Participants();
        $participant_handler->init($this->logger);

        // Create telegram handler
        $this->logger->debug('Start telegram session');

        $proto = $this->telegramLogin('session.madeline');
        // Get list of available dialogs
        try {
            $dialogs = $proto->get_dialogs();
        } catch (\danog\MadelineProto\RPCErrorException $e) {
            if ($e->rpc === 'BOT_METHOD_INVALID') {
                \danog\MadelineProto\Logger::log("Bots can't execute this method!");
                return;
            } else {
                $this->logger->error($e->getMessage());
            }
        }

        foreach ($dialogs as $dialog) {

            $full = null;
            $attepts = 0;
            do {
                $attepts++;
                $this->logger->debug('in while');
                try {
                    $full = $proto->get_full_info($dialog);
                    $attepts = 3;
                } catch (\Exception $e) {
                    switch ($e->getCode()) {
                        case 420:
                            $this->logger->debug('Flood error, wait a little bit', [$e->getCode(), $e->getMessage()]);
                            sleep(10);
                            break;

                        default:
                            $this->logger->debug('Unexpected error', [$e->getCode(), $e->getMessage()]);
                            sleep(1);
                            break;
                    }
                }
            } while ($attepts < 3);

            if (!empty($full)) {
                $dlg_id = $dialog_handler->process($full);
            } else {
                $this->logger->notice('Failed to get dialog full info');
                continue;
            }

            // Read & process messages
            $add_offset = 0;
            $floodwaitrate = 0;
            $getHistErr = 0;
            while (1) {
                if ($getHistErr > 5) {
                    break;
                }
                $settings = array(
                    'peer' => $dialog,
                    'offset_id' => 0,
                    'offset_date' => 0,
                    'add_offset' => $add_offset,
                    'limit' => 100,
                    'max_id' => 0,
                    'min_id' => 0,
                    'hash' => 0
                );
                try {
                    $messages = $proto->messages->getHistory($settings);
                } catch (Exception $e) {
                    $getHistErr++;
                    $this->logger->warning('getHistory fail', ['settings' => $settings, 'e' => $e->getMessage()]);
                    if ($e->getMessage() === 'You have not joined this chat') {
                        break;
                    }
                    sleep(6);
                    continue;
                }
                $getHistErr = 0;
                $add_offset += 100;
                if (empty($messages['messages'])) {
                    print_r($messages);
                    break;
                }
                $this->logger->debug('handle users from messages');
                foreach ($messages['users'] as $user) {
                    $newusr = new \App\Models\Telegram\Users();
                    $usr_id = $newusr->upsert($user);
                    $participant_handler->upsert($dlg_id, $usr_id, null, false);
                }
                $this->logger->debug('start handle message');
                foreach ($messages['messages'] as $msg) {
                    $messages_handler->process($msg);
                }
                $floodwaitrate = $add_offset / 100;
                if ($floodwaitrate > 22) {
                    $floodwaitrate = 7;
                } elseif ($floodwaitrate > 12) {
                    $floodwaitrate = 5;
                }
                $this->logger->debug('wait ' . $floodwaitrate);
                sleep($floodwaitrate);
            }

        }
    }

    public function joinIcoDialogsAction()
    {
        ini_set('memory_limit', '1512M');

        $this->logger = $this->di->get('logger');

        $this->dialog_handler = new \App\Models\Telegram\Dialogs();
        $this->dialog_handler->init($this->logger);

        //at
        $ics_model = new \App\Models\Icos\Icos\Sources();
        $ics_model->init($this->logger);

        $ics = \App\Models\Icos\Icos\Sources::find([
            'conditions' => 'tat_id IS NOT NULL'
        ]);
        $wait = 1;
        foreach ($ics as $ic) {
            if ($ic->at && !$ic->at->tat_processed) {
                if ($wait > 14) {
                    $wait = 6;
                }
                $this->joinDialogAT($ic->at);
                sleep($wait++);
            }
        }
        //$this->getDialogsAction();


        //link
        $ics = \App\Models\Icos\Icos\Sources::find([
            'conditions' => 'url_id_telegram IS NOT NULL'
        ]);

        $channeljoin_handler = new \App\Models\Telegram\Channeljoin();
        $channeljoin_handler->setLogger($this->logger);
        foreach ($ics as $ic) {
            $channeljoin_handler->process($ic->telegram->url_title);
        }

        $urls = $channeljoin_handler->find([
            'conditions' => 'chj_processed IS NULL'
        ]);

        $wait = 2;

        foreach ($urls as $value) {
            if ($wait > 32) {
                $wait = 17;
            }
            $this->joinDialogUrl($value);
            sleep($wait++);
        }
        $this->getDialogsAction();
        $this->logger->debug('Finish');

    }

    public function joinAction()
    {
        $this->joinDialogsATAction();
        $this->joinDialogsUrlAction();
    }

    public function joinDialogsUrlAction()
    {
        ini_set('memory_limit', '1512M');

        $this->logger = $this->di->get('logger');

        $this->dialog_handler = new \App\Models\Telegram\Dialogs();
        $this->dialog_handler->init($this->logger);

        //join via link
        $hsts = [
            'https://t.me'
        ];

        $host_model = new \App\Models\Hosts();
        $alldhst_id = [];

        foreach ($hsts as $hst_title) {
            $alldhst_id[] = $host_model->upsert($hst_title);
        }

        $urls = \App\Models\Urls::query()
                                ->inWhere('hst_id', $alldhst_id)
                                ->execute();


        $channeljoin_handler = new \App\Models\Telegram\Channeljoin();
        $channeljoin_handler->setLogger($this->logger);
        foreach ($urls as $url) {
            $channeljoin_handler->process($url->url_title);
        }

        $urls = $channeljoin_handler->find([
            'conditions' => 'chj_processed IS NULL'
        ]);

        $wait = 1;

        foreach ($urls as $value) {
            if ($wait > 14) {
                $wait = 6;
            }
            $this->joinDialogUrl($value);
            sleep($wait++);
        }
        $this->getDialogsAction();
        $this->logger->debug('Finish');
    }

    public function joinDialogsATAction()
    {
        ini_set('memory_limit', '1512M');

        $this->logger = $this->di->get('logger');

        $proto = $this->telegramLogin();

        $this->dialog_handler = new \App\Models\Telegram\Dialogs();
        $this->dialog_handler->init($this->logger);

        //join via at
        $ats = \App\Models\Telegram\Telegramat::find([
            'conditions' => 'tat_processed IS NULL',
            'order' => 'tat_id DESC'
        ]);

        $wait = 1;

        foreach ($ats as $at) {
            if ($wait > 14) {
                $wait = 6;
            }
            $this->joinDialogAT($at);
            sleep($wait++);
        }
        $this->getDialogsAction();
        $this->logger->debug('Finish');
    }

    public function leaveDialogsAction()
    {
        $this->logger = $this->di->get('logger');
        ini_set('memory_limit', '1512M');
        $proto = $this->telegramLogin();

        try {
            $dialogs = $proto->get_dialogs();
        } catch (\danog\MadelineProto\RPCErrorException $e) {
            if ($e->rpc === 'BOT_METHOD_INVALID') {
                \danog\MadelineProto\Logger::log("Bots can't execute this method!");
                return;
            } else {
                $this->logger->error($e->getMessage());
                exit;
            }
        }


        foreach ($dialogs as $dialog) {
            if (!empty($dialog['channel_id'])) {
                try {
                    $updates = $proto->channels->leaveChannel(['channel' => $dialog]);
                    print_r($updates);
                } catch (\danog\MadelineProto\RPCErrorException $e) {
                    $this->logger->debug('', [$e->getMessage()]);

                    if ($e->getMessage() === 'CHANNEL_PRIVATE') {
                        continue;
                    }
                }
            } elseif (!empty($dialog['chat_id'])) {
                try {
//                    $info = $proto->get_info($dialog);
                    $updates = $proto->messages->deleteChatUser(['chat_id' => $dialog, 'user_id' => '@me']);
                    print_r($updates);
                } catch (\danog\MadelineProto\RPCErrorException $e) {
                    $this->logger->debug('', [$e->getMessage()]);

                }
            }

            sleep(2);
        }
    }

    public function testAction()
    {
        $this->logger = $this->di->get('logger');

        ini_set('memory_limit', '1512M');

        $coins = new \App\Models\Coins\Coins();
        $coins->init($this->logger);

        $coins->getTicketsCoinmarket();

        echo 1;
    }

    private function joinDialogUrl($value)
    {
        if (empty($value)) {
            throw new \InvalidArgumentException('Empty channel link');
        }

        $dlg_id = null;
        $this->logger->debug('Handling', ['id' => $value->chj_id, 'link' => $value->chj_link]);

        $value->setLogger($this->logger);

        if (preg_match('/bot$/i', $value->chj_link)) {
            $this->logger->debug('Find link to bot. Continue', ['url' => $value->chj_link]);
            $value->linkProcessed();
            return;
        }

        $proto = $this->telegramLogin("join.madeline");
        $updates = null;
        $dialog = null;

        try {
            $dialog = $proto->get_full_info($value->chj_link);
        } catch (\danog\MadelineProto\RPCErrorException $e) {

            $this->logger->debug('get error', [$e]);

            if ($e->rpc === 'USERNAME_INVALID') {
                $value->linkProcessed();
                return;
            }
            if ($e->rpc === 'INVITE_HASH_EXPIRED') {
                $value->linkProcessed();
                return;
            }
            if ($e->rpc === 'CHAT_FORBIDDEN') {
                $value->linkProcessed();
                return;
            }

            if ($e->getCode() == 420) {
                //                    $this->logger->debug('Flood error, wait a little bit', [$e->getCode(), $e->getMessage()]);
                //                    $pos = strrpos($e->getMessage(), '_');
                //                    $wait = intval(substr($e->getMessage(), $pos + 1));
                //                    $this->logger->debug('wait ' . $wait);
                //                    sleep($wait);
                $this->logger->debug('', ['e' => $e, 'link' => $value->chj_link]);
                return;
            } elseif ($e->getMessage() === 'INVITE_HASH_INVALID') {
                $this->logger('', [$e->getMessage()]);
                $value->linkProcessed();
                return;
            }

            $this->logger->debug(__LINE__ . 'Unknown Error ');
            var_dump($e);
            $this->logger->debug('exit');
            exit;
        } catch (\danog\MadelineProto\Exception $e) {
            if ($e->getMessage() === 'This peer is not present in the internal peer database') {
                $value->linkProcessed();
                return;
            }
            if ($e->getMessage() === 'You have not joined this chat') {
                $this->logger->debug('', ['error' => $e->getMessage(), 'url' => $value->chj_link]);
            } else {
                $this->logger->debug(__LINE__ .' Fail to get info from url. Unknown error', ['error' => $e, 'url'
                => $value->chj_link]);
                $this->logger->debug('exit');
                exit;
            }
        } catch (\Exception $e) {
            throw new \RuntimeException(__LINE__ ." Unknown error url = {$value->url_title}, {$e}");
        }

        if (!empty($dialog)) {
            // check if already joined
            if (!empty($dialog['channel_id'])) {
                $dlg_id = intval($dialog['channel_id']);
            }
            if (!empty($dialog['chat_id'])) {
                $dlg_id = intval($dialog['chat_id']);
            }
            if (!empty($dialog['user_id'])) {
                $value->linkProcessed();
                return;
            }
            if (empty($dlg_id)) {
                $this->logger->debug('Empty dlg_id');
                return;
            }

            $exist = \App\Models\Telegram\Dialogs::findFirst([
                'conditions' => 'dlg_id = :dlg_id:',
                'bind' => ['dlg_id' => $dlg_id]
            ]);

            if (!empty($exist->dlg_id)) {
                $this->logger->debug('Already joined', ['dlg_id' => $exist->dlg_id]);
                $value->linkProcessed();
                return;
            }
        }

        while (1) {

            $this->logger->debug('wait 10');
            sleep(10);
            try {
                if (strpos($value->chj_link, 'joinchat') !== false) {
                    $updates = $proto->messages->importChatInvite(['hash' => $value->chj_link]);
                } else {
                    $updates = $proto->channels->joinChannel(['channel' => $value->chj_link]);
                }
            } catch (\danog\MadelineProto\RPCErrorException $e) {
                $this->logger->debug('get error', [$e]);

                if ($e->getMessage() === 'The user is already in the group') {
                    $value->linkProcessed();
                    return;
                }

                if ($e->getCode() == 420) {
                    $this->logger->debug('Flood error, wait a little bit', [$e->getCode(), $e->getMessage()]);
                    $pos = strrpos($e->getMessage(), '_');
                    $wait = intval(substr($e->getMessage(), $pos + 1));
                    $this->logger->debug('wait ' . $wait);
                    sleep($wait);
                    //                    $this->logger->debug('Flood error (links). COntinue ', ['link' => $value->chj_link]);
                    continue;
                } elseif ($e->getMessage() === 'CHANNELS_TOO_MUCH') {
                    $this->logger('', [$e]);
                    exit;
                } elseif ($e->getMessage() === 'INVITE_HASH_INVALID') {
                    $this->logger('', [$e]);
                    $value->linkProcessed();
                    return;
                }

                $this->logger->debug('exit');
                exit;
            } catch (\danog\MadelineProto\Exception $e) {
                $this->logger->debug('get error', [$e]);

                $this->logger->warning('Fail to join channel', ['url' => $value->chj_link]);
                exit;
            } catch (\Exception $e) {
                $this->logger->warning('Undefined error ', [$e]);
                exit;
            }
            break;
        }

        $this->logger->debug('Joined to dialog', ['dlg_id' => $dlg_id, 'link' => $value->chj_link]);
        $value->linkProcessed();
        if (empty($dialog)) {
            try {
                $dialog = $proto->get_full_info($value->chj_link);
            } catch (\danog\MadelineProto\RPCErrorException $e) {
                $this->logger->debug('Fail to get dialog after joining', [$e]);
                return;
            } catch (\danog\MadelineProto\Exception $e) {
                $this->logger->debug('Fail to get dialog after joining', [$e]);
                return;
            } catch (\Exception $e) {
                $this->logger->debug('Fail to get dialog after joining', [$e]);
                return;
            }
        }
        $dlg_id = $this->dialog_handler->process($dialog);
        $this->logger->debug('Processed link to channel in urls', ['dlg_id' => $dlg_id]);
    }

    private function joinDialogAT($at)
    {
        if (empty($at)) {
            throw new \InvalidArgumentException('Empty at');
        }

        $this->logger->debug('Handling', ['id' => $at->tat_id, 'at' => $at->tat_title]);

        //ignore bots
        if (preg_match('/bot$/i', $at->tat_title)) {
            $this->logger->debug('Find link to bot. Continue', ['url' => $at->tat_title]);
            $at->tatProcessed();
            return;
        }

        $proto = $this->telegramLogin("join.madeline");
        $at->init($this->logger);
        $err = 0;
        try {
            $full = $proto->get_full_info($at->tat_title);
        } catch (\danog\MadelineProto\RPCErrorException $e) {

            $this->logger->debug('get error', [$e]);

            if ($e->rpc === 'USERNAME_INVALID') {
                $at->tatProcessed();
                return;
            }
            if ($e->rpc === 'INVITE_HASH_EXPIRED') {
                $at->tatProcessed();
                return;
            }
            if ($e->getCode() == 420) {
                $this->logger->debug('Flood error', [$at->tat_title]);

                //                    $this->logger->debug('Flood error, wait a little bit', [$e->getCode(), $e->getMessage()]);
                //                    $pos = strrpos($e->getMessage(), '_');
                //                    $wait = intval(substr($e->getMessage(), $pos + 1));
                //                    $this->logger->debug('wait ' . $wait);
                //                    sleep($wait);
                return;
            }

            var_dump($e);
            $this->logger->debug('exit');
            exit;
        } catch (\danog\MadelineProto\Exception $e) {

            $this->logger->debug('get error', [$e]);

            if ($e->getMessage() === 'This peer is not present in the internal peer database') {
                $at->tatProcessed();
                return;
            }
            if (strpos($e->getMessage(), 'contacts.resolveUsername')) {
                $at->tatProcessed();
                return;
            }

            $this->logger->debug('exit' . __LINE__);
            var_dump($e);
            exit;
        }

        if (!empty($full['user_id'])) {
            $this->logger->debug('find user_id. Continue');
            $at->tatProcessed();
            return;
        }

        $dlg_id = null;
        if (!empty($full['channel_id'])) {
            $dlg_id = intval($full['channel_id']);
        } elseif (!empty($full['chat_id'])) {
            $dlg_id = intval($full['chat_id']);
        } else {
            throw new \RuntimeException(__LINE__ . 'Unknown dialogue type');
        }

        if (empty($dlg_id)) {
            throw new \InvalidArgumentException('Invalid dlg_id');
        }

        $exist = \App\Models\Telegram\Dialogs::findFirst([
            'conditions' => 'dlg_id = :dlg_id:',
            'bind' => ['dlg_id' => $dlg_id]
        ]);

        if (!empty($exist->dlg_id)) {
            $this->logger->debug('dialog exist', ['dlg_id' => $exist->dlg_id]);
            $at->tatProcessed();
            return;
        }


        $dlg_id = null;
        if (!empty($full['channel_id'])) {
            $this->logger->debug('wait 10');
            sleep(10);

            $dlg_id = $full['channel_id'];

            try {
                $updates = $proto->channels->joinChannel(['channel' => $at->tat_title]);
            } catch (\danog\MadelineProto\RPCErrorException $e) {
                $this->logger->debug('get error', [$e]);

                if ($e->getCode() == 420) {
                    $this->logger->debug('Flood error, wait a little bit', [$e->getCode(), $e->getMessage()]);
                    $pos = strrpos($e->getMessage(), '_');
                    $wait = intval(substr($e->getMessage(), $pos + 1));
                    $this->logger->debug('wait ' . $wait);
                    sleep($wait);
                    //                        $this->logger->debug('Flood error. Continue', [$at->tat_title]);
                    return;
                } elseif ($e->getMessage() === 'CHANNELS_TOO_MUCH') {
                    $this->logger('', [$e]);
                    exit;
                }

                $this->logger->warning('RPCErrorException', [$e]);
                $this->logger->debug('exit');
                exit;
            } catch (\danog\MadelineProto\Exception $e) {
                $this->logger->debug('get error', [$e]);

                $this->logger->warning('Fail to join channel', ['url' => $at->tat_title]);
                $this->logger->debug('exit');
                exit;
            } catch (\Exception $e) {
                $this->logger->warning('Undefined error ', [$e]);
                exit;
            }
        } elseif (!empty($full['chat_id'])) {
            $this->logger->debug('found chat', ['full' => $full]);

            $dlg_id = $full['chat_id'];

            $this->logger->debug('exit');
            exit;
        }

        if ($err) {
            $this->logger->debug('continue');
            return;
        }

        $this->logger->debug('Join to channel', ['dlg_id' => $dlg_id, 'link' => $at->tat_title]);
        $this->dialog_handler->process($full);
        $at->tatProcessed();
    }

}

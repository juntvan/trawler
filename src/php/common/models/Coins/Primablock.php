<?php

namespace App\Models\Coins;

class Primablock extends Base
{
    public function initialize()
    {
        parent::initialize();
        $this->belongsTo('prm_id','App\Models\Coins\Addresses','adr_id',['alias'=>'adr']);
        $this->hasMany('prm_id','App\Models\Coins\Primablock\Transactions','prm_id',['alias'=>'pbt']);
    }

   public function upsert($adr_id, $balance) {

       $data = [];

       if (empty($adr_id)) {
           throw new \InvalidArgumentException('Empty adr_id');
       }
       $data['prm_id'] = intval($adr_id);
       if (empty($data['prm_id'])) {
           throw new \InvalidArgumentException('Invalid adr_id');
       }

       $data['ico_id'] = Icos::process('test');

       $rec = self::findFirst([
           'conditions' => 'prm_id = :prm_id:',
           'bind' => ['prm_id' => $data['prm_id']]
       ]);

       $right_op = '1000000000000000000'; //for eth

       $data['prm_balance'] = bcdiv($balance, $right_op, 10);

       if (empty($rec->prm_id)) {
           $rec = new self;
           $rec->save($data);
           if ($rec->getMessages()) {
               foreach ($rec->getMessages() as $message) {
                    trigger_error($message);
               }
               throw new \InvalidArgumentException('Failed to insert primablock');
           }
           $this->ldebug('Insert new row in primablock ', ['data' => $data]);
       }
       else {
           if ($rec->prm_balance !== $data['prm_balance']) {
               $rec->update($data);
               $this->ldebug('update ', ['prm_id' => $data['prm_id']]);
           }
       }

       return $rec->prm_id;
   }

   public function getSetId($adr_title) {
        if (empty($adr_title)) {
            throw new \InvalidArgumentException('Empty adr_title');
        }

        $adr_handler = new Addresses();
        $adr_id = $adr_handler->process($adr_title);

       $data['prm_id'] = intval($adr_id);
       $data['ico_id'] = Icos::process('test');

       $rec = self::findFirst([
           'conditions' => 'prm_id = :prm_id:',
           'bind' => ['prm_id' => $data['prm_id']]
       ]);

       if (empty($rec->prm_id)) {
           $rec = new self;
           $rec->save($data);
           if ($rec->getMessages()) {
               foreach ($rec->getMessages() as $message) {
                   trigger_error($message);
               }
               throw new \InvalidArgumentException('Failed to insert primablock');
           }
           $this->ldebug('Insert new row in primablock ', ['data' => $data]);
       }

       return $rec->prm_id;
   }
}
<?php

namespace App\Controllers;

use App\Lib\Tools;
use App\Models\Coins\Primablock;
use App\Models\Icos\Icos;
use App\Models\Stat\Coins\Hype;
use App\Models\Stat\Users;
use App\Models\Telegram\Dialogs;
use App\Models\Telegram\Messages;
use App\Models\Telegram\Participants;

class ApiController extends ControllerBase
{
    public function indexAction()
    {
        return $this->response->setContent('<div>index</div>');
    }

    public function dialogsAction()
    {
        //TODO: количество примаблоков в диалогах
        $data = [];
        $dialogs = Dialogs::find([
            'order' => 'dlg_date DESC',
            'conditions' => 'dlg_date IS NOT NULL'
        ]);
        $dateFrom = Tools::date_convert(intval($this->request->get('dateFrom')));
        $dateTo = Tools::date_convert(intval($this->request->get('dateTo')));

        foreach ($dialogs as $dialog) {

            $prts = $dialog->getPrt([
                'conditions' => 'prt_joined > :dateFrom: AND prt_joined < :dateTo:',
                'bind' => ['dateFrom' => $dateFrom, 'dateTo' => $dateTo]
            ]);

            $data[] = [
                'id' => $dialog->dlg_id,
                'title' => $dialog->dlg_title,
                'date' => $dialog->dlg_date,
                'prt_count' => $dialog->dlg_participants_count,
                'prt_added' => count($prts),
                'prm_number' => $dialog->dlg_prm_number
            ];
        }
        return $this->response->setJsonContent($data);
    }

    public function messagesAction()
    {
        $dlg_id = intval($this->request->get('dialog'));
        $page = intval($this->request->get('page'));
        $per_page = intval($this->request->get('per_page'));

        if ($page < 1 || $page > 100000) {
            $page = 1;
        }

        if ($per_page < 1 || $per_page > 1000) {
            $per_page = 100;
        }

        $error = [
            'error' => 1,
            'id' => 0,
            'title' => '',
            'messages' => []
        ];

        if (empty($dlg_id)) {
            return $this->response->setJsonContent($error);
        }

        $messages = [];

        $dlg = Dialogs::findFirst([
            'conditions' => 'dlg_id = :dlg_id:',
            'bind' => ['dlg_id' => $dlg_id]
        ]);

        if (empty($dlg->dlg_id)) {
            return $this->response->setJsonContent($error);
        }

        $msgs = Messages::find([
            'conditions' => 'dlg_id = :dlg_id:',
            'bind' => ['dlg_id' => $dlg_id],
            'limit' => $per_page,
            'offset' => $per_page * ($page - 1)
        ]);

        foreach ($msgs as $msg) {
            $messages[] = [
                'id' => $msg->msg_id,
                'title' => $msg->msg_title,
                'from' => [
                    'id' => $msg->usr_id_from,
                    'name' => $msg->usr->usr_first_name . ' ' . $msg->usr->usr_last_name
                ],
                'to' => [
                    'id' => $msg->dlg_id_to,
                    'name' => $msg->dlg->dlg_title
                ],
                'reply_to' => $msg->msg_id_reply,
                'date' => $msg->msg_date
            ];
        }

        $data = [
            'error' => 0,
            'id' => $dlg->dlg_id,
            'title' => $dlg->dlg_title,
            'page' => $page,
            'per_page' => $per_page,
            'messages' => $messages
        ];
        return $this->response->setJsonContent($data);
    }

    public function primablockAction()
    {
        $page = intval($this->request->get('page'));
        $per_page = intval($this->request->get('per_page'));

        if ($page < 1 || $page > 1000) {
            $page = 1;
        }

        if ($per_page < 1 || $per_page > 10000) {
            $per_page = 50;
        }

        $primablocks = Primablock::find([
            'order' => 'prm_created DESC',
            'limit' => $per_page,
            'offset' => $per_page * ($page - 1)
        ]);

        $data = [];
        foreach ($primablocks as $primablock) {
            $dialogs = [];
            foreach ($primablock->adr->urladr as $urls) {
                $msgs = [];
                foreach ($urls->msgurl as $msg) {

                    if (isset($dialogs[$msg->dlg->dlg_id]['msgs'])) {
                        $msgs = $dialogs[$msg->dlg->dlg_id]['msgs'];
                    }
                    $msgs[$msg->msg_id] = [
                        'id' => $msg->msg_id,
                        'title' => nl2br($msg->msg_title),
                        'from' => [
                            'id' => $msg->usr->usr_id,
                            'name' => $msg->usr->usr_first_name . ' ' . $msg->usr->usr_last_name
                        ],
                        'date' => $msg->msg_date
                    ];
                    $dialogs[$msg->dlg->dlg_id] = [
                        'id' => $msg->dlg->dlg_id,
                        'title' => $msg->dlg->dlg_title,
                        'msgs' => $msgs
                    ];
                }
            }

            $tokens = [];
            $tokens_value = 0;
            $contract_date = null;
            foreach ($primablock->pbt as $transaction) {
                if ($transaction->pbt_is_token) {
                    $tokens[] = [
                        'coin' => $transaction->con->con_title,
                        'coin_code' => $transaction->con->con_code,
                        'from' => $transaction->adrfrom->adr_title,
                        'to' => $transaction->adrto->adr_title,
                        'value' => floatval($transaction->pbt_value),
                        'date' => $transaction->pbt_timestamp
                    ];
                    $tokens_value += floatval($transaction->pbt_value);
                }
                if ($transaction->adr_to === null && $contract_date === null) {
                    $contract_date = $transaction->pbt_timestamp;
                }
            }

            $last_transation = Primablock\Transactions::maximum([
                'conditions' => 'prm_id = :prm_id:',
                'bind' => ['prm_id' => $primablock->prm_id],
                'column' => 'pbt_timestamp'
            ]);

            $data[] = [
                'fee' => floatval($primablock->prm_fee),
                'balance' => round(floatval($primablock->prm_balance), 4),
                'allocation' => floatval($primablock->prm_allocation),
                'pool_value' => floatval($primablock->prm_pool_value),
                'contribution_max' => floatval($primablock->prm_contribution_max),
                'contribution_min' => floatval($primablock->prm_contribution_min),
                'invalid' => $primablock->prm_invalid == null ? false : $primablock->prm_invalid,
                'status' => $primablock->prm_status === null ? 'Fail' : $primablock->prm_status,
                'address' => $primablock->adr->adr_title,
                'coin' => $primablock->adr->con->con_title,
                'dialogs' => $dialogs,
                'last_transaction' => $last_transation === null ? '0' : $last_transation,
                'date' => $contract_date === null ? '0' : $contract_date,
                'tokens' => [
                    'value' => round($tokens_value, 4),
                    'tokens' => $tokens
                ]
            ];
        }

        $data['page'] = $page;
        $data['per_page'] = $per_page;

        return $this->response->setJsonContent($data);
    }

    public function coinStatAction()
    {
        $data = [];

        $month_ago = date('Y-m-d', time() - 2592000);

        $hypes = Hype::find([
            'conditions' => 'coh_date > :month_ago:',
            'bind' => ['month_ago' => $month_ago],
            'order' => 'con_id ASC, coh_date ASC'
        ]);

        foreach ($hypes as $hype) {
            $data[$hype->con->con_code][] = [
                'date' => $hype->coh_date,
                'freq' => $hype->coh_usagef
            ];
        }

        return $this->response->setJsonContent($data);
    }

    public function icosAction()
    {
        $three_days_ago = date('Y-m-d', time() - 259200);
        $icos = Icos::find([
            'conditions' => 'ico_last_check > :three_days_ago:',
            'bind' => ['three_days_ago' => $three_days_ago],
            'order' => 'ico_id'
        ]);

        $data = [];
        foreach ($icos as $ico) {
            $sources = [];

            foreach ($ico->ics as $ics) {
                $sources[] = [
                    'from' => $ics->linkto ? $ics->linkto->url_title : $ics->src->hst->hst_title,
                    'whitepaper' => $ics->whitepaper ? $ics->whitepaper->url_title : null,
                    'sdate' => $ics->ics_sdate,
                    'edate' => $ics->ics_edate,
                    'psdate' => $ics->ics_presale_sdate,
                    'pedate' => $ics->ics_presale_edate,
                    'hardcap' => $ics->ics_hardcap,
                    'hardcap_cur' => $ics->ics_hardcap_currency,
                    'softcap' => $ics->ics_softcap,
                    'softcap_cur' => $ics->ics_softcap_currency,
                    'raised' => $ics->ics_raised_fund,
                    'raised_cur' => $ics->ics_raised_fund_currency,
                    'kyc' => $ics->ics_kyc
                ];
            }

            $data[$ico->ico_id] = [
                'name' => $ico->ico_name,
                'site' => $ico->hst->hst_title,
                'coin' => $ico->con ? $ico->con->con_code : null,
                'sources' => $sources
            ];
        }
        return $this->response->setJsonContent($data);
    }

    public function icoStatAction()
    {
        $ico_today = 0;
        $preico_today = 0;

        $today = date('Y-m-d', time());

        $ics = Icos\Sources::find([
            'columns' => 'ico_id',
            'conditions' => '(ics_sdate <= :today: AND ics_edate >= :today:) OR (ics_sdate <= :today: AND ics_edate IS NULL)',
            'bind' => ['today' => $today],
            'group' => 'ico_id'
        ]);

        $ico_today = count($ics);

        $ics = Icos\Sources::find([
            'columns' => 'ico_id',
            'conditions' => '(ics_presale_sdate <= :today: AND ics_presale_edate >= :today:) OR (ics_presale_sdate <= :today: AND ics_presale_edate IS NULL)',
            'bind' => ['today' => $today],
            'group' => 'ico_id'
        ]);

        $preico_today = count($ics);

        $data = [
            'ico_today' => $ico_today,
            'preico_today' => $preico_today
        ];
        return $this->response->setJsonContent($data);
    }

    public function dialogsStatAction()
    {
        $dialogs = Dialogs::find([
            'conditions' => 'dlg_participants_count > 3'
        ]);

        $data = [];
        foreach ($dialogs as $dialog) {
            $stat = [];

            $stat_from_table = $dialog->getStatdialogs([
                'order' => 'std_date'
            ]);

            foreach ($stat_from_table as $s) {
                $stat[] = [
                    'date' => $s->std_date,
                    'nmsg' => $s->std_nmsg,
                    'nusr' => $s->std_nusr
                ];
            }

            $data[$dialog->dlg_id] = [
                'name' => isset($dialog->dlg_title) ? $dialog->dlg_title : "No name",
                'participants' => $dialog->dlg_participants_count,
                'stat' => $stat
            ];
        }

        return $this->response->setJsonContent($data);
    }

    public function usersStatAction()
    {
        $statusers = Users::find([
            'conditions' => 'stu_nmsg >= 100',
            'order' => 'stu_nmsg DESC'
        ]);

        $data = [];
        foreach ($statusers as $stu) {
            $stats = [];
            $stat = [
                'nick' => $stu->prt->usr->usr_name ? $stu->prt->usr->usr_name : null,
                'name' => $stu->prt->usr->usr_first_name ? $stu->prt->usr->usr_first_name : null . ' ' .
                $stu->prt->usr->usr_last_name ? $stu->prt->usr->usr_last_name : null,
                'nmsg' => $stu->stu_nmsg
            ];

            if (!$stat['nick']) {
                $stat['nick'] = $stat['name'];
            }

            $stats[] = $stat;

            if (isset($data[$stu->prt->dlg_id]) && isset($data[$stu->prt->dlg_id]['stat'])) {
                $prev = $data[$stu->prt->dlg_id]['stat'];
                array_push($prev, $stat);
                $stats = $prev;
            }

            $data[$stu->prt->dlg_id] = [
                'dialog' => $stu->prt->dlg->dlg_title ? $stu->prt->dlg->dlg_title : "No name",
                'stat' => $stats
            ];


        }

        return $this->response->setJsonContent($data);
    }
}
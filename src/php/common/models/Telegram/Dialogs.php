<?php

namespace App\Models\Telegram;

use App\Lib\Tools;
use App\Models\Telegram\Users;
use App\Models\Telegram\Inputpeer;

class Dialogs extends \App\Models\Telegram\Base
{

    public function initialize()
    {
        parent::initialize();
        //$this->hasManyToMany('dlg_id', 'App\Models\Telegram\Participants', 'dlg_id', 'usr_id', 'App\Models\Telegram\Users', 'usr_id', ['alias'=>'prt']);
        $this->hasMany('dlg_id','App\Models\Telegram\Participants','dlg_id', ['alias'=>'prt']);
        $this->hasMany('dlg_id','App\Models\Stat\Dialogs','dlg_id', ['alias'=>'statdialogs']);
    }

    /**
     * @var Users
     */
    private $user_handler;

    public function init($logger)
    {
        $this->setLogger($logger);

        $this->user_handler = new Users();
        $this->user_handler->setLogger($logger);
    }

    /**
     * @param array $dialog
     * @return integer
     */
    public function process($dialog)
    {
        $data = [];

        if (empty($dialog)) {
            throw new \InvalidArgumentException('Empty dialog');
        }

        if (empty($dialog['full'])) {
            print_r($dialog['full']);
            throw new \InvalidArgumentException('Empty full');
        }

        if (empty($dialog['full']['id']) && empty($dialog['full']['user']['id'])) {
            throw new \InvalidArgumentException('Empty dlg_id');
        }

        if (!empty($dialog['full']['id'])) {
            $data['dlg_id'] = intval($dialog['full']['id']);
        } else {
            $data['dlg_id'] = $dialog['full']['user']['id'];
        }

        if (empty($data['dlg_id'])) {
            throw new \InvalidArgumentException('Invalid dlg_id');
        }

        if (empty($dialog['type'])) {
            throw new \InvalidArgumentException("Empty type for dialog {$data['dlg_id']}");
        }
        $tps_title = Types::isValid($dialog['type']);
        if (empty($tps_title)) {
            throw new \InvalidArgumentException("Invalid dialog type {$dialog['type']} for {$data['dlg_id']}");
        }
        $data['tps_id'] = Types::upsert($dialog['type']);
        unset($dialog['type']);

        if (!empty($dialog['Chat'])) {
            if (empty($dialog['Chat']['title'])) {
                throw new \InvalidArgumentException("Empty title for dialog");
            }
            $data['dlg_title'] = trim(mb_convert_encoding($dialog['Chat']['title'], 'UTF-8'));

            if (empty($dialog['Chat']['participants_count']) && empty($dialog['full']['participants_count'])) {
                throw new \InvalidArgumentException("Empty participants_count for dialog");
            }
            if (!empty($dialog['Chat']['participants_count'])) {
                $data['dlg_participants_count'] = intval($dialog['Chat']['participants_count']);
                unset($dialog['Chat']['participants_count']);
            } elseif (!empty($dialog['full']['participants_count'])) {
                $data['dlg_participants_count'] = intval($dialog['full']['participants_count']);
                unset($dialog['full']['participants_count']);
            }
            if (empty($data['dlg_participants_count'])) {
                throw new \InvalidArgumentException("Invalid participants_count for chat");
            }

            if (empty($dialog['Chat']['date'])) {
                throw new \InvalidArgumentException("Empty date for dlg");
            }
            $data['dlg_date'] = Tools::date_convert($dialog['Chat']['date']);
            unset($dialog['Chat']['date']);

            if (!empty($dialog['Chat']['migrated_to']['channel_id'])) {
                $migrated_to = intval($dialog['Chat']['migrated_to']['channel_id']);
                if (!empty($migrated_to)) {
                    $dlg_model = new self;
                    $data['dlg_migrated_to'] = $dlg_model->getsetDlgId($migrated_to);
                }
                unset($dialog['Chat']['migrated_to']);
            }

            if (!empty($dialog['Chat']['username'])) {
                $username = trim($dialog['Chat']['username']);
                $data['nic_id'] = Nicknames::upsert($username);
                unset($dialog['Chat']['username']);
            }

            if (!empty($dialog['Chat']['access_hash'])) {
                $data['dlg_access_hash'] = intval($dialog['Chat']['access_hash']);
                if (empty($data['dlg_access_hash'])) {
                    throw new \InvalidArgumentException("Invalid access hash for dlg_id {$data['dlg_id']}");
                }
            }

            if (!empty($dialog['Chat']['democracy'])) {
                $data['dlg_democracy'] = 1;
            }

            if (!empty($dialog['Chat']['restricted'])) {
                $data['dlg_restricted'] = 1;
            }

            if (!empty($dialog['Chat']['photo']['_']) && !strcmp($dialog['Chat']['photo']['_'], 'chatPhoto')) {
                $data['dlg_photo'] = 1;
            }

            if (!empty($dialog['Chat']['full']['about'])) {
                $data['dlg_about'] = $dialog['Chat']['full']['about'];
                unset($dialog['about']);
            }

            unset($dialog['Chat']);
        } elseif (!empty($dialog['User'])) {

            if (!empty($dialog['User']['username'])) {
                $username = trim($dialog['User']['username']);
                $data['nic_id'] = Nicknames::upsert($username);
                unset($dialog['username']);
            } else {
                $this->lnotice('Empty dialog username', ['id' => $data['dlg_id'], 'type' => $tps_title]);
            }

            if (!empty($dialog['User']['bot'])) {
                $data['dlg_bot'] = 1;
            }
            $data['dlg_participants_count'] = 2;

            $usr = new Users();
            $usr_id = $usr->upsert($dialog['User']);
            unset($dialog['User']);
        } else {
            throw new \InvalidArgumentException('Empty User of Chat structure');
        }
        unset($dialog['full']);

        if (empty($dialog['bot_api_id'])) {
            throw new \InvalidArgumentException("Empty bot_api_id for dialog {$data['dlg_id']}");
        }
        $data['dlg_bot_api_id'] = intval($dialog['bot_api_id']);
        unset($dialog['bot_api_id']);
        if (empty($data['dlg_bot_api_id'])) {
            throw new \InvalidArgumentException("Invalid bot_api_id for dialog {$data['dlg_id']}");
        }

        if (empty($dialog['last_update'])) {
            throw new \InvalidArgumentException("Empty last_update for dialog {$data['dlg_id']}");
        }
//        $data['dlg_last_update'] = Tools::date_convert(intval($dialog['last_update']));
//        more useful
        $data['dlg_last_update'] = 'now()';

        unset($dialog['last_update']);
        if (empty($data['dlg_last_update'])) {
            throw new \InvalidArgumentException("Invalid last_update for dialog {$data['dlg_id']}");
        }


        unset($dialog['user_id']);
        unset($dialog['chat_id']);
        unset($dialog['channel_id']);
        unset($dialog['InputPeer']);
        unset($dialog['Peer']);
        unset($dialog['InputUser']);
        unset($dialog['InputChannel']);

        if (empty($GLOBALS['acc_id'])) {
            throw new \InvalidArgumentException('acc_id not set globally');
        }
        $data['acc_id'] = $GLOBALS['acc_id'];

        // Check if dialog is already exists
        $rec = self::findFirst([
                'conditions' => 'dlg_id = :dlg_id:',
                'bind' => ['dlg_id' => $data['dlg_id']]
            ]
        );

        // New record
       // print_r($data);
        if (empty($rec->dlg_created)) {
            $rec = new self();
            $rec->save($data);
            if ($rec->getMessages()) {
                $this->lerror('', ['data' => $data]);
                foreach ($rec->getMessages() as $message) {
                    trigger_error($message, E_USER_ERROR);
                }
                throw new \RuntimeException('Failed to insert new dialog', [$dialog]);
            }
            $this->ldebug('New dialog', ['id' => $rec->dlg_id]);
        } else {
            // Compare new and old records
            $diff = [];
            foreach ($data as $field => $value) {
                if (empty($rec->$field) or $rec->$field != $value) {
                    $diff[$field] = $value;
                }
            }
            if (!empty($diff)) {
                $rec->update($diff);
                $this->ldebug('Updating existing dialog', ['id' => $rec->dlg_id] + $diff);
            }
        }

        //set pinned msg
        if (!empty($dialog['full']['pinned_msg_id'])) {
            $msg_model = new Messages();
            $msg_model->init($this->getLogger());
            $msg_model->setPinnedMsg($rec->dlg_id, $dialog['full']['pinned_msg_id']);
        }

        return $rec->dlg_id;
    }

    public function getsetDlgId($id)
    {
        if (empty($id)) {
            throw new \InvalidArgumentException('Empty id');
        }

        $id = intval($id);

        if (empty($id)) {
            throw new \InvalidArgumentException('Invalid id');
        }

        $rec = self::findFirst([
            'conditions' => 'dlg_id = :dlg_id:',
            'bind' => ['dlg_id' => $id]
        ]);

        $tps_id = Types::upsert('will_be_filled_in_future');
        $time = Tools::date_convert(time());

        if (empty($GLOBALS['acc_id'])) {
            throw new \InvalidArgumentException('acc_id not set globally');
        }

        $data = [
            'dlg_id' => $id,
            'tps_id' => $tps_id,
            'dlg_bot_api_id' => 0,
            'dlg_last_update' => $time,
            'dlg_date' => $time,
            'acc_id' => $GLOBALS['acc_id']

        ];

        if (empty($rec->dlg_id)) {
            $rec = new self;

            $rec->save($data);
            if ($rec->getMessages()) {
                foreach ($rec->getMessages() as $message) {
                    trigger_error($message, E_USER_ERROR);
                }
                throw new \RuntimeException('Failed to insert new dialog ' . $id);
            }
            $this->ldebug('New dialog in getsetDlgId', ['id' => $rec->dlg_id]);

        }
        return $rec->dlg_id;
    }

    public function updateDlgPrmNumber($dlg_id, $number)
    {
        if (empty($dlg_id)) {
            throw new \InvalidArgumentException('Empty dlg_id');
        }
        $dlg_id = intval($dlg_id);

        if (empty($dlg_id)) {
            throw new \InvalidArgumentException('Invalid dlg_id');
        }

        if (!isset($number)) {
            throw new \InvalidArgumentException('Empty number of prm in dialog');
        }
        $number = intval($number);

        $rec = self::findFirst([
            'conditions' => 'dlg_id = :dlg_id:',
            'bind' => ['dlg_id' => $dlg_id]
        ]);

        if (!empty($rec->dlg_id)) {
            if ($number === $rec->dlg_prm_number) {
                return;
            }
            $rec->update(['dlg_prm_number' => $number]);
            if ($rec->getMessages()) {
                foreach ($rec->getMessages() as $message) {
                    trigger_error($message, E_USER_ERROR);
                }
                throw new \RuntimeException('Failed to update number of prm in dialog ' . $dlg_id);
            }
            $this->ldebug('Update number of prm in dialog', ['dlg_prm_number' => $number]);
        }
        else {
            throw new \RuntimeException('Dialog not found ' . $dlg_id);
        }
    }

}
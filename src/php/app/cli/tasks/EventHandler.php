<?php

class EventHandler extends \danog\MadelineProto\EventHandler
{
    use \attics\Lib\Llog\Logger;

    public function __construct($MadelineProto)
    {
        parent::__construct($MadelineProto);
    }

    public function init()
    {
        $this->setLogger(Phalcon\DI::getDefault()->getLogger());
    }

    public function __call($method, $arguments)
    {
        if (method_exists($this, $method)) {
            $this->init();
            return call_user_func_array(array($this, $method), $arguments);
        }
        return false;
    }

    public function onAny($update)
    {
        //\danog\MadelineProto\Logger::log("Received an update of type ".$update['_']);
        echo $update['_'] . "\n";
        //file_put_contents('unhandledEvents', $update['_'] . PHP_EOL, FILE_APPEND);
    }

    private function onUpdateNewMessage($update)
    {
        $this->ldebug('onUpdateNewMessage');

        if (empty($update['message'])) {
            $this->ldebug('empty update[message]', [$update]);
            return;
        }
        $message_handler = new \App\Models\Telegram\Messages();
        $message_handler->init($this->getLogger());
        $message_handler->process($update['message']);
    }

    private function onUpdateEditMessage($update)
    {
        $this->ldebug('onUpdateEditMessage');

        if (empty($update['message'])) {
            $this->ldebug('empty update[message]', [$update]);
            return;
        }
        $message_handler = new \App\Models\Telegram\Messages();
        $message_handler->init($this->getLogger());
        $message_handler->process($update['message']);
    }

    private function onUpdateNewChannelMessage($update)
    {

        $this->ldebug('onUpdateNewChannelMessage');

        if (empty($update['message'])) {
            $this->ldebug('empty up date[message]', [$update]);
            return;
        }

        $message_handler = new \App\Models\Telegram\Messages();
        $message_handler->init($this->getLogger());
        $message_handler->process($update['message']);
    }

    private function onUpdateEditChannelMessage($update)
    {
        $this->ldebug('onUpdateEditChannelMessage');

        if (empty($update['message'])) {
            $this->ldebug('empty update[message]', [$update]);
            return;
        }
        $message_handler = new \App\Models\Telegram\Messages();
        $message_handler->init($this->getLogger());
        $message_handler->process($update['message']);
    }

    private function onUpdateChannelPinnedMessage($update)
    {
        $this->ldebug('onUpdateChannelPinnedMessage');

        $message_handler = new \App\Models\Telegram\Messages();
        $message_handler->init($this->getLogger());
        $message_handler->setPinnedMsg($update['channel_id'], $update['id']);
    }

//    private function onUpdateMessageID($update)
//    {
//        $this->ldebug('onUpdateMessageID', [$update]);
//        print_r($update);
//        exit;
//    }

    private function onUpdateChatParticipantAdd($update)
    {
        $this->ldebug('onUpdateChatParticipantAdd');

        if (empty($update) || empty($update['chat_id'])
                || empty($update['user_id']) || empty($update['date'])) {
            print_r($update);
            exit;
        }

        $participantModel = new \App\Models\Telegram\Participants();
        $participantModel->init($this->getLogger());

        $dialogModel = new \App\Models\Telegram\Dialogs();
        $dialogModel->init($this->getLogger());

        $userModel = new \App\Models\Telegram\Users();
        $userModel->init($this->getLogger());

        $dlg_id = $dialogModel->getsetDlgId($update['chat_id']);
        $usr_id = $userModel->getsetUserId($update['user_id']);
        $participantModel->upsert($dlg_id, $usr_id, empty($update['date']), false);
    }

    private function onUpdateChatParticipantDelete($update)
    {
        $this->ldebug('onUpdateChatParticipantDelete');

        $dlg_id = intval($update['chat_id']);
        $usr_id = intval($update['user_id']);
        if (empty($dlg_id) || empty($usr_id)) {
            $this->ldebug('Empty required element', ['dlg_id' =>$dlg_id, 'usr_id' => $usr_id]);
        } else {
            $participant_model = new \App\Models\Telegram\Participants();
            $participant_model->init($this->getLogger());
            $usr_model = new \App\Models\Telegram\Users();
            $usr_model->init($this->getLogger());
            $usr_id = $usr_model->getsetUserId($usr_id);
            $participant_model->upsert($dlg_id, $usr_id, null, true);
        }
    }
    private function onUpdateChatParticipants($update)
    {
        $this->ldebug('onUpdateChatParticipants');

        print_r($update);
        exit;
    }

    /* unuseful
    private function onUpdateChannel($update)
    {
        $this->ldebug('onUpdateChannel');

        print_r($update);

    }*/


}
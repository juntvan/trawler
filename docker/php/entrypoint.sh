#!/bin/bash

touch /logs/php_errors.log
chown www-data:www-data /logs/php_errors.log

# Setup env variables to docker
printenv | perl -pe 's/^(.+?\=)(.*)$/\1"\2"/g' | cat - /crontab_tmp > /crontab
crontab -u www-data /crontab
cron

# Setup app keys for private repos on bitbucket
git config --global url."https://juntvan:Ckywn9rLPTAcNtaGEExb@bitbucket.org/".insteadOf "https://bitbucket.org/"

# Install packages
composer --working-dir=/src/php install

/src/php/app/cli/start.sh

# Start daemon
php-fpm

